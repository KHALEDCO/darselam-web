import React from "react";
import { Route, Redirect } from "react-router-dom";

import { connect } from 'react-redux';
function AppRoute({ component: Component, userInfo, privateArea, userGroup, permissionName, layout: Layout, ...rest }) {
  //const isAuthenticated =  userInfo.id;

  return (
    <Route
    
      {...rest}
      render={(props) =>

        privateArea == true ? // If the route is allowed only for logged users.
        (
          (userGroup =='admin' && userInfo.userPermissions && userInfo.userPermissions.includes(permissionName) ) || 
          (userGroup =='client' && userInfo.id != 0) ||
          (userGroup =='admin' && permissionName == 'AdminHome') ||
          (userInfo.super == 1)) ? ( // check if it's logged.
          <Layout><Component {...props} /></Layout>

        ) : (<Redirect to="/" />)

        : (<Layout><Component {...props} /></Layout>)
      }
    />
  );
}

const mapStateToProps = (state) => {
  return {
      //selectedLanguage: state.LanguageReducer.slctdLng,
    userInfo: state.User.info
  }
}

export default connect(mapStateToProps, null)(AppRoute);

