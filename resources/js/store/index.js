
import { createStore } from 'redux'; // ,applyMiddleware
//import thunk from 'redux-thunk';
//import logger from 'redux-logger';

import { persistStore, persistReducer } from 'redux-persist';

import storage from 'redux-persist/lib/storage';
import rootReducer  from './reducers/index'

const persistConfig = {
    key: 'LanguageReducer',
    storage: storage, 
    whitelist: ['LanguageReducer', 'cartItems', 'User', 'Favourites', 'Currs', 'CompareList'] // which reducer want to store , 'User'
  };

  const pReducer = persistReducer(persistConfig, rootReducer);

  
//const middleware = applyMiddleware(thunk, logger);
const store = createStore(pReducer); //,middleware
const persistor = persistStore(store);


//const store = createStore(rootReducer)
//console.log(store.getState())

//export default store
export { persistor, store };

