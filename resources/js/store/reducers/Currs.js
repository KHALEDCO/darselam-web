
const INITIAL_STATE = {
    currencyInfo: {
        currencyCode: 'USD', 
        title: 'Dollar',
        exchangeRate: 1
    }
};
 
export const Currs  = (state = INITIAL_STATE, action) => {
    switch(action.type) {
        case 'SAVE_CURRS':
            return {
                rows: action.payload,
                currencyInfo: state.currencyInfo
            };

        case 'CHANGE_CURRENCY':
            return {
                currencyInfo: action.payload,
                rows: state.rows
            };
        default:
            return state;
    }
};
 
export default Currs;
