/* const INITIAL_STATE = {
  mainUrl: 'http://localhost:3000/',  
  apiUrl: 'http://localhost:3000/api/'
}; */

const INITIAL_STATE = {
  mainUrl: 'http://derasa.net/',  
  apiUrl: 'http://derasa.net/api/' 
};
  
export const DfltSts  = (state = INITIAL_STATE, action) => {
  switch(action.type) {
    case 'SAVE_SETTINGS':
      return action.payload;
    default:  
     return state; 
  }
};

export default DfltSts;