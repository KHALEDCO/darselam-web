
const INITIAL_STATE = {rows: []};

export const Products  = (state = INITIAL_STATE, action) => {
    switch(action.type) {
        case 'SAVE_PRODUCTS':
            return {
                rows: action.payload
            };
        default:
            return state;
    }
};

export default Products;
