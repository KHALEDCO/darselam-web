import React, { Component, Fragment} from 'react'

import {toast} from 'react-toastify';
import { connect } from 'react-redux';

import CKEditor from 'ckeditor4-react';

import {Helmet} from "react-helmet";
import { Link } from 'react-router-dom';

import postData from '../../../helpers/postData';
import LangCodesSelect from '../../../components/LangCodesSelect';

 class Create extends Component {
    constructor(props) {
        super(props);
        this.state = {
          rowId:this.props.match.params.id,

            title: '',
            description: '',      
            languageCode: '',
            errorMessage: '',
            isDisabled: false,             
            
        };

        
        this.onChangeValue = this.onChangeValue.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
         
    }
   
    componentDidMount() {  
    }

    
    onChangeValue(e) {
      //console.log(e.target.value)
        this.setState({
            [e.target.name]: e.target.value, isDisabled: false
        });
    }

   
    handleSubmit(e){

        e.preventDefault();
        this.setState({isDisabled: true});
        const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

        postData(this.props.defaultSets.apiUrl+'tabs-trans', {
          rowId: this.state.rowId,
          languageCode: this.state.languageCode,
          title: this.state.title,
          description: this.state.description,
            
          langCode: this.props.selectedLanguage.langCode

        }, this.props.userInfo.accessToken)
            .then( (response) => {   

                toast.update(ldToast, {
                    type: toast.TYPE.SUCCESS,
                    render: response.data.msg
                });

                /* setTimeout( () => {
                    this.props.history.push('/tabs');
                }, 3000); */
            })
            .catch( (error) => {
                //console.log(error.response);
                this.setState({  isDisabled: false})

                if(error.response !== undefined && error.response.status === 422){
                    let errorTxt = '';
                    let errorMessage = error.response.data.errors;

                    Object.keys(errorMessage).map((key, i) => {
                        errorTxt = (<span> {errorTxt} {errorMessage[key][0]} <br /></span>);});
                    toast.update(ldToast, {
                        type: toast.TYPE.ERROR,
                        render: errorTxt
                    });
                }
            });

    }

    

    render() {
       
        const {isDisabled } = this.state;

        const lng = this.props.selectedLanguage;

        return (
                           
        
      <div className="row">
          <Helmet><title>{lng.crtNewTxt}</title></Helmet>

                  
      <div className="col-md-12">
        
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item"><Link to="/admin"><i className="fa fa-home"></i></Link></li>
            <li className="breadcrumb-item"><Link to="/admin/tabs">{lng.tabsTxt}</Link></li>
            <li className="breadcrumb-item active" aria-current="page">{lng.crtNewTxt}</li>
          </ol>
        </nav>
      </div>


        <div className="col-md-8">
            
                     
            <div className="card">
                <div className="card-header card-header-primary">
                  <h4 className="card-title">{lng.crtNewTxt}</h4>
                  <p className="card-category"></p>
                </div>
                <div className="card-body">
                  <form>  

                    <div className="row">
                      <div className="col-md-6">
                        <div className="form-group">
                          <label className="bmd-label-floating">{lng.titleTxt}</label>                          
                          <input 
                          className="form-control"
                          type="text"
                          name="title"
                          onChange={this.onChangeValue} />
                        </div>
                      </div>


                      <div className="col-md-6">
                        <div className="form-group">
                          <label className="bmd-label-floating">{lng.languageTxt}</label>                          
                          <LangCodesSelect
                          defaultSets={this.props.defaultSets}
                          selectedLanguage={this.props.selectedLanguage}
                          onChangeValue={this.onChangeValue} />
                        </div>
                    </div>

                      </div>

                    <div className="row">
                      <div className="col-md-12">
                        <div className="form-group">
                          <label></label>
                          <div className="form-group">
                            <label className="bmd-label-floating">{lng.descriptionTxt}</label>
                                             
                          
                            <CKEditor
                          data=""
                                onChange={evt => {
                                  this.setState({description: evt.editor.getData()})

                                }}
                                onKey={evt => {
                                  this.setState({description: evt.editor.getData()})

                                }}                                
        
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                    
                    <button 
                    type="submit" 
                    onClick={this.handleSubmit}
                    disabled={isDisabled}
                    className="btn btn-primary pull-right">{lng.saveTxt}
                    </button>
                    <div className="clearfix"></div>
                  </form>
                </div>
              </div>
        
        
          
        </div>




      </div>
        
        

        )
    }
}


const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        defaultSets: state.DfltSts,
        userInfo: state.User.info
    }
}
export default connect(mapStateToProps, null)(Create);
