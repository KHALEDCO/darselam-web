import React, { Component, Fragment} from 'react'

import {toast} from 'react-toastify';
import { connect } from 'react-redux';

import ImgToBeUploaded from '../../../components/ImageToUpload';


import CKEditor from 'ckeditor4-react';

import {Helmet} from "react-helmet";
import { Link } from 'react-router-dom';
import { checkMimeType } from '../../../helpers/checkMimeType';
import postData from '../../../helpers/postData';
import LangCodesSelect from '../../../components/LangCodesSelect';

 class Create extends Component {
    constructor(props) {
        super(props);
        this.state = {
          rowId:this.props.match.params.id,

            title: '',
            description: '',
            metaTags: '',            
            languageCode: '',
            photo: '',
            theCount: 1,
            errorMessage: '',
            isDisabled: false,
              
            images: []
        };

        
        this.onChangeValue = this.onChangeValue.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

        //the next functions handle images on selected, change and upload.
        
        this.onOtherImagesChange = this.onOtherImagesChange.bind(this);
        this.rmvSlctdOthrImgs = this.rmvSlctdOthrImgs.bind(this);
        
        this.handleUploadOtherPhoto = this.handleUploadOtherPhoto.bind(this);
        
        //.
        /* this.onSizesSelectChange = this.onSizesSelectChange.bind(this); */

        //this.handleSvValues = this.handleSvValues.bind(this);
         
    }
   
    componentDidMount() {  
    }

    onOtherImagesChange(event) {
      if (event.target.files && event.target.files[0]) {

        this.setState({ images: [...this.state.images, ...event.target.files] })

      }
    };

    // The next function is used inside handleSubmit function not used directrly anywhere 
    // so after row have been inserted it uploads its photo.
 
    handleUploadOtherPhoto(transId) { // Id of the row that created in DB to be inserted in files db table.
      
      if (this.state.images.length == 0) {
        return false;
      }
      // then prepare a form to send files.
      const formData = new FormData()
      //console.log('t', transId)

      // The next two lines go to FilesController to specify the row and tbl where inserted.
      formData.append('rowId', transId);
      formData.append('type', 'product');
      formData.append('languageCode', this.state.languageCode);

      //.

      // iterate over multiple additional images and append them to formData.
      this.state.images.forEach((image, i) => { 
        //console.log(image.name)
        formData.append(
          'files[]',
          image,
          image.name
        )
      })
  
      fetch(this.props.defaultSets.apiUrl+'upload-files', {
        method: 'POST',
        body: formData
      })
      .then(response => response.json())
      .then(response => {
        this.setState({          
          images: []
        });
        /* onUploadProgress: progressEvent => {
          console.log(progressEvent.loaded / progressEvent.total)
        } */
      })
    }
    
    
    rmvSlctdOthrImgs(name) {
      this.setState({
        images: this.state.images.filter(image => image.name !== name)
      })
    }


    onChangeValue(e) {
      //console.log(e.target.value)
        this.setState({
            [e.target.name]: e.target.value, isDisabled: false
        });
    }

   
    handleSubmit(e){

        e.preventDefault();
        this.setState({isDisabled: true});
        const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);


        postData(this.props.defaultSets.apiUrl+'products-trans', {
          rowId: this.state.rowId,
          languageCode: this.state.languageCode,
          title: this.state.title,
          description: this.state.description,
          metaTags: this.state.metaTags,      
            
          langCode: this.props.selectedLanguage.langCode

        }, this.props.userInfo.accessToken)
            .then( (response) => {   

              this.handleUploadOtherPhoto(response.data.rowId);

                toast.update(ldToast, {
                    type: toast.TYPE.SUCCESS,
                    render: response.data.msg
                });

                /* setTimeout( () => {
                    this.props.history.push('/products');
                }, 3000); */
            })
            .catch( (error) => {
                //console.log(error.response);
                this.setState({  isDisabled: false})

                if(error.response !== undefined && error.response.status === 422){
                    let errorTxt = '';
                    let errorMessage = error.response.data.errors;

                    Object.keys(errorMessage).map((key, i) => {
                        errorTxt = (<span> {errorTxt} {errorMessage[key][0]} <br /></span>);});
                    toast.update(ldToast, {
                        type: toast.TYPE.ERROR,
                        render: errorTxt
                    });
                }
            });

    }

    

    render() {
       
        const {isDisabled, images, } = this.state;

        const lng = this.props.selectedLanguage;

        return (
                           
        
      <div className="row">
          <Helmet><title>{lng.crtNewTxt}</title></Helmet>

                  
      <div className="col-md-12">
        
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item"><Link to="/admin"><i className="fa fa-home"></i></Link></li>
            <li className="breadcrumb-item"><Link to="/admin/products">{lng.productsTxt}</Link></li>
            <li className="breadcrumb-item active" aria-current="page">{lng.crtNewTxt}</li>
          </ol>
        </nav>
      </div>


        <div className="col-md-8">
            
                     
            <div className="card">
                <div className="card-header card-header-primary">
                  <h4 className="card-title">{lng.crtNewTxt}</h4>
                  <p className="card-category"></p>
                </div>
                <div className="card-body">
                  <form>  

                    <div className="row">
                      <div className="col-md-6">
                        <div className="form-group">
                          <label className="bmd-label-floating">{lng.titleTxt}</label>                          
                          <input 
                          className="form-control"
                          type="text"
                          name="title"
                          onChange={this.onChangeValue} />
                        </div>
                      </div>


                      <div className="col-md-6">
                        <div className="form-group">
                          <label className="bmd-label-floating">{lng.languageTxt}</label>                          
                          <LangCodesSelect
                          defaultSets={this.props.defaultSets}
                          selectedLanguage={this.props.selectedLanguage}
                          onChangeValue={this.onChangeValue} />
                        </div>
                    </div>

                      </div>

                    <div className="row">
                      <div className="col-md-12">
                        <div className="form-group">
                          <label></label>
                          <div className="form-group">
                            <label className="bmd-label-floating">{lng.descriptionTxt}</label>
                                             
                          
                            <CKEditor
                          data=""
                                onChange={evt => {
                                  this.setState({description: evt.editor.getData()})

                                }}
                                onKey={evt => {
                                  this.setState({description: evt.editor.getData()})

                                }}                                
        
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                    
                    <div className="row">
                      <div className="col-md-12">
                        <div className="form-group">
                          <label></label>
                          <div className="form-group">
                            <label className="bmd-label-floating">{lng.metaTagsTxt}</label>
                                             
                          <textarea 
                          className="form-control"
                          name="metaTags"
                          onChange={this.onChangeValue}
                          >
                          </textarea>
                          </div>
                        </div>
                      </div>
                    </div>
                   
                    <button 
                    type="submit" 
                    onClick={this.handleSubmit}
                    disabled={isDisabled}
                    className="btn btn-primary pull-right">{lng.saveTxt}
                    </button>
                    <div className="clearfix"></div>
                  </form>
                </div>
              </div>
        
        
          
        </div>



        <div className="col-md-4">
         
        
          <div className="col-md-12">
              
          <div className="card card-profile">
                <div className="card-avatar">.</div>
                <div className="card-body">
                  <h6 className="card-category text-gray"></h6>
                  <h5 className="card-title mb-3">{lng.otherPhotosTxt}</h5>
                  
                  <div className="card-description">

                    {images? images.map((img, index) => {
                                  return(
                                    <ImgToBeUploaded image={img} key={'img'+index} rmvSlctdOthrImgs={this.rmvSlctdOthrImgs} />
                                  )
                      }) : null
                    }
                  
                    <input type="file" multiple onChange={this.onOtherImagesChange} />
                  </div>
                  
                </div>
              </div>
           
          </div>            
        
        </div>


      </div>
        
        

        )
    }
}


const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        defaultSets: state.DfltSts,
        userInfo: state.User.info
    }
}
export default connect(mapStateToProps, null)(Create);
