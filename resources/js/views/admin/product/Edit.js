import React, { Component, Fragment} from 'react'

import {toast} from 'react-toastify';
import { connect } from 'react-redux';

import {Helmet} from "react-helmet";
import { Link } from 'react-router-dom';

import ColoursSelect from '../../../components/ColoursSelect';
//import SizesSelect from '../../../components/SizesSelect';

import ProductsSelect from '../../../components/ProductsSelect';

import getData from '../../../helpers/getData';
import putData from '../../../helpers/putData';
//import SubcatSelect from '../../../components/SubcatSelect';
import postData from '../../../helpers/postData';
import FeaturesSelect from '../../../components/FeaturesSelect';
import CatsSelect from '../../../components/CatsSelect';

 class Edit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id:0,
            priceBeforeDiscount: 0,
            priceAfterDiscount: 0,
            sku: '',
            status: 1,
            theCount: 1,
            isOffer: 0,
            isGallery: 0,
            isNew: 0,
            isAccessory: 0,
            errorMessage: '',
            isDisabled: false,
            //categoryId: '',
            //accessoryId: 0,
            //subcatId: '',
            selectedColours: [],
            //selectedSizes: [],
            prdctPrps: [],
            //propValueId: 0,
            propId: 0,
            isSvPrpsDisabled: false,
            //isSvVlsDisabled: false,
            selectedFeatures: []
        };
        
        this.onChangeValue = this.onChangeValue.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.fetchData = this.fetchData.bind(this);      
        
        this.onColoursSelectChange = this.onColoursSelectChange.bind(this);
        this.onCatsSelectChange = this.onCatsSelectChange.bind(this);
        this.onFeaturesSelectChange = this.onFeaturesSelectChange.bind(this);
        this.handleSvPrps = this.handleSvPrps.bind(this);
        //this.handleSvValues = this.handleSvValues.bind(this);
    }
  
    onColoursSelectChange(selectedColours) {
      this.setState({selectedColours})
    }
  
    onFeaturesSelectChange(selectedFeatures) {
      this.setState({selectedFeatures})
    }
    onCatsSelectChange(selectedCats) {
      this.setState({selectedCats})
    }
    
    /* onSizesSelectChange(selectedSizes) {
      this.setState({selectedSizes})
    } */
    componentDidMount(){

         this.fetchData(this.props.match.params.id);     

    }

    fetchData(elmntId) {

      getData(this.props.defaultSets.apiUrl+'products/'+elmntId+'/edit', this.props.userInfo.accessToken)

          //.then(response => response.json())
          .then(response => {

            let colours = [];
        {response.data.product_props && response.data.product_props.map((item, i) => {
          colours.push({
            "label": eval("item.title"+this.props.selectedLanguage.langCode), 
            "value": item.id
          })
        })};
        
        let features = [];
        {response.data.features && response.data.features.map((item, i) => {
          //let featureInfo = item.elment_trans.find(trns => trns.languageCode == this.props.selectedLanguage.langCode);
          features.push({
            "label":  eval("item.title"+this.props.selectedLanguage.langCode),  //featureInfo && featureInfo.title, //
            "value": item.id
          })
        })};

        let cats = [];cats
        {response.data.cats && response.data.cats.map((item, i) => {
          //let featureInfo = item.elment_trans.find(trns => trns.languageCode == this.props.selectedLanguage.langCode);
          cats.push({
            "label":  eval("item.title"+this.props.selectedLanguage.langCode),  //featureInfo && featureInfo.title, //
            "value": item.id
          })
        })};

            this.setState({ 
                id: elmntId,
                //categoryId: response.data.categoryId,
                //subcatId: response.data.subcatId,
                colourId: response.data.colourId,
                sizeId: response.data.sizeId,
                titleAR: response.data.titleAR,
                //titleTR: response.data.titleTR,
                titleEN: response.data.titleEN,
                descriptionAR: response.data.descriptionAR,
                //descriptionTR: response.data.descriptionTR,
                descriptionEN: response.data.descriptionEN,
                metaTags: response.data.metaTags,
                priceBeforeDiscount: response.data.priceBeforeDiscount,
                priceAfterDiscount: response.data.priceAfterDiscount,
                theCount: response.data.theCount,
                sku: response.data.sku,
                status: response.data.status,
                isOffer: response.data.isOffer,
                isGallery: response.data.isGallery,
                isNew: response.data.isNew,
                isAccessory: response.data.isAccessory,
                photo: response.data.photo,
                photo2: response.data.photo2,
                otherImages: response.data.files,
                selectedCats: cats,
                selectedFeatures: features,
                selectedColours: colours,
                prdctPrps: response.data.product_props,                
                //features: response.data.features
              });
              //console.log(response.data.files)
          })
          .catch(function (error) {
              console.log(error);
          });
    }

    // The next function is used inside handleSubmit function not used directrly anywhere 
    // so after row have been inserted it uploads its photo.
  

    onChangeValue(e) {

      this.setState({
            [e.target.name]: e.target.value, isDisabled: false
        });
    }
    
    handleSubmit(e){

        e.preventDefault();
        this.setState({isDisabled: true});
        const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

        // prepare selected features to be sent.
      let features = [];
      {this.state.selectedFeatures && this.state.selectedFeatures.map((item, i) => {
        features.push(item.value);
      })}
      //.

      // prepare selected cats to be sent.
      let cats = [];
      {this.state.selectedCats && this.state.selectedCats.map((item, i) => {
        cats.push(item.value);
      })}
      //.
          putData(this.props.defaultSets.apiUrl+'products/'+this.state.id, {
            id: this.state.id,
            //accessoryId: this.state.accessoryId,
            colourId: this.state.colourId,            
            priceBeforeDiscount: this.state.priceBeforeDiscount,
            priceAfterDiscount: this.state.priceAfterDiscount,
            sku: this.state.sku,
            status: this.state.status,
            isGallery: this.state.isGallery,
            isNew: this.state.isNew,
            isOffer: this.state.isOffer,
            isAccessory: this.state.isAccessory,
            theCount: this.state.theCount,
            features: features,
            cats: cats,
            langCode: this.props.selectedLanguage.langCode
        }, this.props.userInfo.accessToken)
            .then( (response) => {

                toast.update(ldToast, {
                    type: toast.TYPE.SUCCESS,
                    render: response.data.msg
                });
                this.setState({isDisabled: false});
 
               /*  setTimeout( () => {
                    this.props.history.push('/products');
                }, 3000); */
            })
            .catch( (error) => {
                
                this.setState({  isDisabled: false})

                if(error.response !== undefined && error.response.status === 422){
                    let errorTxt = '';
                    let errorMessage = error.response.data.errors;

                    Object.keys(errorMessage).map((key, i) => {
                        errorTxt = (<span> {errorTxt} {errorMessage[key][0]} <br /></span>);});
                    toast.update(ldToast, {
                        type: toast.TYPE.ERROR,
                        render: errorTxt
                    });
                }
            });

    }

/* 
    handleSvValues(e){

      
      e.preventDefault();
      this.setState({isSvVlsDisabled: true});
      const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

      // prepare selected roles to be sent.
      let sizes = [];
      {this.state.selectedSizes && this.state.selectedSizes.map((item, i) => {
        sizes.push(item.value);
      })}
      //.
 
      postData(this.props.defaultSets.apiUrl+'sv-prdct-prps-vls', {
        propId: this.state.propId,
        productId: this.state.id,
        propValues: sizes,
        sku: this.state.sku,
        theCount: this.state.theCount,
        langCode: this.props.selectedLanguage.langCode
      }, this.props.userInfo.accessToken)
          .then( (response) => {
            if(response.data.success == true) {
              toast.update(ldToast, {
                type: toast.TYPE.SUCCESS,
                render: response.data.msg
            });
            } else {
              toast.update(ldToast, {
                type: toast.TYPE.ERROR,
                render: response.data.msg
            });
            }
              
              this.setState({ isSvVlsDisabled: false})

          })
          .catch( (error) => {
              console.log(error);
          });

  } */

    handleSvPrps(e){

      e.preventDefault();
      this.setState({isSvPrpsDisabled: true});
      const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

      // prepare selected roles to be sent.
      let colours = [];
      {this.state.selectedColours && this.state.selectedColours.map((item, i) => {
        colours.push(item.value);
      })}
      //.

      postData(this.props.defaultSets.apiUrl+'sv-prdct-prps', {
        productId: this.state.id,
        props: colours,
        langCode: this.props.selectedLanguage.langCode
      }, this.props.userInfo.accessToken)
          .then( (response) => {
            
              if(response.data.success == true) {
                toast.update(ldToast, {
                  type: toast.TYPE.SUCCESS,
                  render: response.data.msg
              });
              } else {
                toast.update(ldToast, {
                  type: toast.TYPE.ERROR,
                  render: response.data.msg
              });
              }
              this.setState({ isSvVlsDisabled: false, isSvPrpsDisabled: false, prdctPrps: response.data.prps})

          })
          .catch( (error) => {
              console.log(error);
          });

  }

    render() {
        
        
        const {isDisabled,isAccessory
          , priceBeforeDiscount, priceAfterDiscount, sku, status, prdctPrps,
          colourId, selectedFeatures, isOffer, isGallery
          , selectedColours, /* selectedSizes, */ isNew, isSvPrpsDisabled, selectedCats /* subcatId */} = this.state;

         const lng = this.props.selectedLanguage;

        return (
                   
          <div className="row">

                        
<Helmet><title>{lng.updtPrdctTxt}</title></Helmet>

                  
<div className="col-md-12">
        
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item"><Link to="/admin"><i className="fa fa-home"></i></Link></li>
            <li className="breadcrumb-item"><Link to="/admin/products">{lng.productsTxt}</Link></li>
            <li className="breadcrumb-item active" aria-current="page">{lng.updtPrdctTxt}</li>
          </ol>
        </nav>
      </div>


          <div className="col-md-8">
            <div className="card">
              <div className="card-header card-header-primary">
                <h4 className="card-title">{lng.updtPrdctTxt}</h4>
                <p className="card-category"></p>
              </div>
              <div className="card-body">
                <form>

                  
                  <div className="row mb-3">
                      <div className="col-md-6">
                      <label className="bmd-label-floating">{lng.categoryTxt}</label>

                      <CatsSelect 
                          selected={selectedCats}
                          onCatsSelectChange={this.onCatsSelectChange}
                          defaultSets={this.props.defaultSets}
                          selectedLanguage={this.props.selectedLanguage} />
                          {/* <CategoriesSelect
                          onChangeValue={this.onChangeValue} 
                          categoryId={categoryId} 
                          defaultSets={this.props.defaultSets}
                          selectedLanguage={this.props.selectedLanguage} /> */}
                      </div>

                      
                      <div className="col-md-6">
                      <label className="bmd-label-floating">{lng.featuresTxt}</label>

                      <FeaturesSelect 
                          selected={selectedFeatures}
                          onFeaturesSelectChange={this.onFeaturesSelectChange}
                          defaultSets={this.props.defaultSets}
                          selectedLanguage={this.props.selectedLanguage} />
                      </div>
                  </div>

                  <div className="row mb-4">
                      

                      <div className="col-md-12">
                        <div className="form-group">
                          <label className="bmd-label-floating">{lng.isAccessoryTxt}</label>                     
                          <select 
                          name="isAccessory"
                          defaultValue={isAccessory}
                          className="form-control"
                          onChange={this.onChangeValue}>
                            <option value="0">{lng.noTxt}</option>
                            <option value="1">{lng.yesTxt}</option>
                          </select>
                        </div>
                      </div>

                    </div> 
                    

                    <div className="row">

                    <div className="col-md-6">
                      <div className="form-group">
                        <label className="bmd-label-floating">{lng.prcBfrDscnt}</label>                            
                        <input 
                        value={priceBeforeDiscount}
                        className="form-control"
                        type="text"
                        name="priceBeforeDiscount"
                        onChange={this.onChangeValue} />
                      </div>
                    </div>


                    <div className="col-md-6">
                      <div className="form-group">
                        <label className="bmd-label-floating">{lng.prcAftrDscnt}</label>                            
                        <input 
                        value={priceAfterDiscount}
                        className="form-control"
                        type="text"
                        name="priceAfterDiscount"
                        onChange={this.onChangeValue} />
                      </div>
                    </div>

                    
                  </div>
               

                  <div className="row">

                    <div className="col-md-6">
                      <div className="form-group">
                        <label className="bmd-label-floating">{lng.statusTxt}</label>                     
                        <select 
                        value={status}
                        name="status"
                        className="form-control"
                        onChange={this.onChangeValue}>
                          <option value="1">{lng.activeTxt}</option>
                          <option value="0">{lng.inActiveTxt}</option>
                        </select>
                      </div>
                    </div>

                    <div className="col-md-6">
                      <div className="form-group">
                        <label className="bmd-label-floating">{lng.newTxt}</label>                     
                        <select 
                        value={isNew}
                        name="isNew"
                        className="form-control"
                        onChange={this.onChangeValue}>
                          <option value="1">{lng.yesTxt}</option>
                          <option value="0">{lng.noTxt}</option>
                        </select>
                      </div>
                    </div>
                  </div>


                  <div className="row">

                    <div className="col-md-6">
                      <div className="form-group">
                        <label className="bmd-label-floating">{lng.isGalleryTxt}</label>                     
                        <select 
                        value={isGallery}
                        name="isGallery"
                        className="form-control"
                        onChange={this.onChangeValue}>
                          <option value="1">{lng.yesTxt}</option>
                          <option value="0">{lng.noTxt}</option>
                        </select>
                      </div>
                    </div>


                    <div className="col-md-6">
                      <div className="form-group">
                        <label className="bmd-label-floating">{lng.isOfferTxt}</label>                     
                        <select 
                        value={isOffer}
                        name="isOffer"
                        className="form-control"
                        onChange={this.onChangeValue}>
                          <option value="1">{lng.yesTxt}</option>
                          <option value="0">{lng.noTxt}</option>
                        </select>
                      </div>
                    </div>


                  </div>

                 
                  <button 
                  type="submit" 
                  onClick={this.handleSubmit}
                  disabled={isDisabled}
                  className="btn btn-primary pull-right">{lng.saveTxt}
                  </button>
                  <div className="clearfix"></div>
                </form>
              </div>
            </div>
          
          
          
            <div className="card">
                <div className="card-header card-header-primary">
                  <h4 className="card-title">{lng.coloursTxt}</h4>
                  <p className="card-category"></p>
                </div>
                <div className="card-body">
                  <form>

                  <div className="row"> 
                      
                      <div className="col-md-12">
                        <label className="bmd-label-floating">{lng.colourTxt}</label>                     
                          
      
                          <ColoursSelect 
                          selected={selectedColours}
                          onColoursSelectChange={this.onColoursSelectChange}
                          defaultSets={this.props.defaultSets}
                          selectedLanguage={this.props.selectedLanguage} />
                      </div>
                    
                  </div>                  
                  
 
                  <button 
                  type="submit" 
                  onClick={this.handleSvPrps}
                  disabled={isSvPrpsDisabled}
                  className="btn btn-primary pull-right">
                    {isSvPrpsDisabled? lng.svPrdctFrstTxt: lng.saveTxt}
                  </button>
                  <div className="clearfix"></div>
                  </form>
                </div>
              </div>
        
{/* 
              <div className="card">
                <div className="card-header card-header-primary">
                  <h4 className="card-title">{sizesTxt}</h4>
                  <p className="card-category"></p>
                </div>
                <div className="card-body">
                  <form>

                  <div className="row">

                      <div className="col-md-6">
                        <label className="bmd-label-floating">{colourTxt}</label>
                          <select 
                          className="form-control" 
                          name="propId"                  
                          onChange={this.onChangeValue}>
                            
                            <option>{prdctPrps && prdctPrps.length == 0 ? lng.svPrpsFrstTxt : lng.chooseTxt }</option>
                            
                            {prdctPrps && prdctPrps.map((item, i) => {
                              return(<option
                                key={i}
                                value={item.colour? item.propId : item.id}>
                                  {item.colour? eval("item.colour.title"+langCode) : eval("item.title"+langCode)}
                                  </option>)
                            })}
                          </select>
                      </div> 

                      <div className="col-md-6">
                        <label className="bmd-label-floating">{sizeTxt}</label>                     

                          <SizesSelect 
                          selected={selectedSizes}
                          onSizesSelectChange={this.onSizesSelectChange}
                          defaultSets={this.props.defaultSets}
                          selectedLanguage={this.props.selectedLanguage} />  
                      </div> 

                      
                      
                    
                  </div>

                  
                  <div className="row">
                  
                        <div className="col-md-6">
                          <div className="form-group">
                            <label className="bmd-label-floating">{lng.theCountTxt}</label>                       
                            <input 
                            className="form-control"
                            type="text"
                            name="theCount"
                            onChange={this.onChangeValue} />
                          </div>
                        </div>


                      <div className="col-md-6">
                        <div className="form-group">
                          <label className="bmd-label-floating">{lng.skuTxt}</label>                           
                          <input 
                          className="form-control"
                          type="text"
                          name="sku"
                          onChange={this.onChangeValue} />
                        </div>
                      </div>

                    </div>
                 
 
                  <button 
                  type="submit" 
                  onClick={this.handleSvValues}
                  disabled={isSvVlsDisabled}
                  className="btn btn-primary pull-right">
                    {lng.savePropsTxt}
                  </button>
                  <div className="clearfix"></div>
                  </form>
                </div>
              </div>
         */}
          
          </div>
          

          
        <div className="col-md-4">
       
       </div>

        </div>
      
        )
    }
}


const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        defaultSets: state.DfltSts,
        userInfo: state.User.info
    }
}
export default connect(mapStateToProps, null)(Edit);
