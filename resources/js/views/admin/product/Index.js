import React, { Component, Fragment} from 'react'

import {toast} from 'react-toastify';
import { connect } from 'react-redux';

import {Helmet} from "react-helmet";
import { Link } from 'react-router-dom';

import ProductRow from '../../../components/ProductRow';

import TitleFilter from '../../../components/filters/TitleFilter';
import StatusFilter from '../../../components/filters/StatusFilter';
import CategoriesSelect from '../../../components/CategoriesSelect';
import getData from '../../../helpers/getData';

 class Index extends Component { 
    constructor(props) {
        super(props);
        this.state = {
            rows: '',
            rowsBfrFltr: [],
            title: '',
            status: '',
            categoryId: '0',
            sku: ''
        };
        this.onValueChange = this.onValueChange.bind(this);
    }
  
   
    componentDidMount(){

      const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

      getData(this.props.defaultSets.apiUrl+'products', this.props.userInfo.accessToken)
          .then(response => {
            //console.log(response.data)

            this.setState({ rows: response.data.rows, rowsBfrFltr: response.data.rows });
            toast.dismiss(ldToast)
          })
          .catch(function (error) {
              console.log(error);
          })
    }

    onValueChange(e) {
      //console.log(e.target.value)
      this.setState({
        [e.target.name]: e.target.value
    }, () => {
        this.tblFilter();
    });
    }

    tblFilter() {
              
       let newRows = this.state.rowsBfrFltr;      
       
        if(this.state.categoryId != '0') {
          newRows = newRows.filter(row => row.categoryId == this.state.categoryId);
        } 
        
        if(this.state.sku != '') {
          newRows = newRows.filter(row => row.sku && row.sku.search(this.state.sku) !== -1);
        }

        if(this.state.title != '') {
          newRows = newRows.filter(row => (row.titleAR && row.titleAR.search(this.state.title) !== -1) || (row.titleEN && row.titleEN.toLowerCase().search(this.state.title.toLowerCase()) !== -1));
        }
 
        if(this.state.status != '') {
          newRows = newRows.filter(row => row.status == this.state.status);
        }       
       
       this.setState({rows: newRows});
    }
 
    render() {
        
        //const {isLoading} = this.state;
        const lng = this.props.selectedLanguage;


        return (
           
         <Fragment>
                
                <Helmet><title>{lng.productsTxt}</title></Helmet>

          <div className="row">

      <div className="col-md-12">
        
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item"><Link to="/admin"><i className="fa fa-home"></i></Link></li>
            <li className="breadcrumb-item active" aria-current="page">{lng.productsTxt}</li>
          </ol>
        </nav>
      </div>


      <div className="col-md-12">

<div className="row">
    <div className="col-md-2">
    <Link to='/admin/create-product' className="btn btn-primary">
        {lng.crtNewTxt}
    </Link>
    </div>

    <div className="col-md-2">
    <CategoriesSelect
    chosenOne={this.state.categoryId} 
    onChangeValue={this.onValueChange}
    defaultSets={this.props.defaultSets}
    selectedLanguage={this.props.selectedLanguage} />
    </div>

    <div className="col-md-2">
                <input 
                type="text" 
                className="form-control" 
                name="sku"
                placeholder={lng.skuTxt}
                onKeyUp={this.onValueChange}
                />
    </div>

      <TitleFilter 
      selectedLanguage={this.props.selectedLanguage}
      onKeyUp={this.onValueChange}
      />                  
    
      <StatusFilter                     
      selectedLanguage={this.props.selectedLanguage}
      onChange={this.onValueChange} 
      />

</div>
  
</div>

          </div>

          <div className="row">
          <div className="col-md-12">
            <div className="card">
              
              <div className="card-header card-header-primary">
                <h4 className="card-title ">{lng.productsTxt}</h4>
                <p className="card-category"></p>
              </div>

              <div className="card-body">
                <div className="table-responsive">
                  <table className="table">
                    <thead className=" text-primary">                    
                      <tr>
                      <th>#</th>
                      <th>{lng.titleTxt}</th>
                      <th>{lng.languageTxt}</th>
                      <th>{lng.categoryTxt}</th>
                      <th>{lng.asAccessorytxt}</th>
                      <th>{lng.prcBfrDscnt}</th>
                      <th>{lng.prcAftrDscnt}</th>
                      <th>{lng.colourTxt}</th>                    
                      <th>{lng.skuTxt}</th>
                      <th>{lng.statusTxt}</th>
                      <th>{lng.actionTxt}</th>
                      </tr>                      
                    </thead>
                    <tbody>
                            {this.state.rows ? 
                            this.state.rows.map((item, index) => {
                              return(<ProductRow obj={item} 
                                key={'pr'+index}
                                keyNum={index + 1}
                                defaultSets = {this.props.defaultSets}
                                selectedLanguage = {this.props.selectedLanguage}
                                userInfo = {this.props.userInfo}                               
                              />)
                            }) : null
                            }                            
                    </tbody>
                  </table>
                </div>
              </div>
            
            </div>
          </div>

          </div>
      
         </Fragment>

        )
    }
}


const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        defaultSets: state.DfltSts,
        userInfo: state.User.info
    }
}
export default connect(mapStateToProps, null)(Index);
