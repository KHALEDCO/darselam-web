import React, { Component} from 'react'

import { Link } from 'react-router-dom';

 class Footer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            
        };
    }
 
    render() {
        
      const lng = this.props.selectedLanguage;
        const sts = this.props.settings;

        return (
          
      <footer className="footer">
      <div className="container-fluid">
        <nav className="float-left">{/* 
          <ul>
            <li>
              <a href="#">
                Creative Tim
              </a>
            </li>
            <li>
              <a href="#">
                About Us
              </a>
            </li>
            <li>
              <a href="#">
                Blog
              </a>
            </li>
            <li>
              <a href="#">
                Licenses
              </a>
            </li>
          </ul>
         */}</nav>
        <div className="copyright float-right">
          &copy; made with <i className="fa fa-heart"></i> by
          <a href="#" target="_blank">ITECH</a> 
          {sts.copyRightsAR && eval("sts.copyRights"+lng.langCode)}
        </div>
      </div>
    </footer>
  

        )
    }
}


export default Footer

               
