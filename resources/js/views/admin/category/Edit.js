import React, { Component, Fragment} from 'react'

import {toast} from 'react-toastify';
import { connect } from 'react-redux';

import ImgToBeUploaded from '../../../components/ImageToUpload';
import RowThumbnail from '../../../components/RowThumbnail';

import {Helmet} from "react-helmet";
import { Link } from 'react-router-dom';
import getData from '../../../helpers/getData';
import putData from '../../../helpers/putData';
import { checkMimeType } from '../../../helpers/checkMimeType';

 class Edit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id:0,
            status: 1,
            errorMessage: '',
        };

        
        this.onChangeValue = this.onChangeValue.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.fetchData = this.fetchData.bind(this);

    }
  
     
  
    componentDidMount(){

      this.fetchData(this.props.match.params.id);    
      //console.log('khaled', this.props.match.params.id)                
 }

 fetchData(elmntId) {
   getData(this.props.defaultSets.apiUrl+'categories/'+elmntId+'/edit', this.props.userInfo.accessToken)

       //.then(response => response.json())
       .then(response => {
        //console.log('k', elmntId)                

         this.setState({ 
             id: elmntId,
             status: response.data.status,             
           });
           //console.log(response.data)
       })
       .catch(function (error) {
           console.log(error);
       });
 }


    onChangeValue(e) {
      //console.log(e.target.value)
        this.setState({
            [e.target.name]: e.target.value
        });
    }
    
    handleSubmit(e){
//console.log('k'+this.state.status)
        e.preventDefault();
        //this.setState({isDisabled: true});
        const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

        putData(this.props.defaultSets.apiUrl+'categories/'+this.state.id, {
            id: this.state.id,            
            status: this.state.status,
            langCode: this.props.selectedLanguage.langCode
        }, this.props.userInfo.accessToken)
            .then( (response) => {
 
                toast.update(ldToast, {
                    type: toast.TYPE.SUCCESS,
                    render: response.data.msg
                });

               /*  setTimeout( () => {
                    this.props.history.push('/categories');
                }, 3000); */
            })
            .catch( (error) => {
                //console.log(error.response);
                //this.setState({  isDisabled: false})

                if(error.response !== undefined && error.response.status === 422){
                    let errorTxt = '';
                    let errorMessage = error.response.data.errors;

                    Object.keys(errorMessage).map((key, i) => {
                        errorTxt = (<span> {errorTxt} {errorMessage[key][0]} <br /></span>);});
                    toast.update(ldToast, {
                        type: toast.TYPE.ERROR,
                        render: errorTxt
                    });
                }
            });

    }


    render() {
        
        
        const {status} = this.state;
        const lng = this.props.selectedLanguage;

        return (
                   

          <div className="row">
            
<Helmet><title>{lng.updtCatTxt}</title></Helmet>

                  
<div className="col-md-12">
        
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item"><Link to="/admin"><i className="fa fa-home"></i></Link></li>
            <li className="breadcrumb-item"><Link to="/admin/categories">{lng.categoriesTxt}</Link></li>
            <li className="breadcrumb-item active" aria-current="page">{lng.updtCatTxt}</li>
          </ol>
        </nav>
      </div>


          <div className="col-md-8">
            <div className="card">
              <div className="card-header card-header-primary">
                <h4 className="card-title">{lng.updtCatTxt}</h4>
                <p className="card-category"></p>
              </div>
              <div className="card-body">
                <form>
                  
               
                  <div className="row">

                    <div className="col-md-12">
                      <div className="form-group">
                        <label className="bmd-label-floating">{lng.statusTxt}</label>                     
                        <select 
                        value={status}
                        name="status"
                        className="form-control"
                        onChange={this.onChangeValue}>
                          <option value="1">{lng.activeTxt}</option>
                          <option value="0">{lng.inActiveTxt}</option>
                        </select>
                      </div>
                    </div>

                  </div>

                  <button 
                  type="submit" 
                  onClick={this.handleSubmit}
                  
                  className="btn btn-primary pull-right">{lng.saveTxt}
                  </button>
                  <div className="clearfix"></div>
                </form>
              </div>
            </div>
          </div>
          

        
        </div>
      
        )
    }
}


const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        defaultSets: state.DfltSts,
        userInfo: state.User.info
    }
}
export default connect(mapStateToProps, null)(Edit);
 