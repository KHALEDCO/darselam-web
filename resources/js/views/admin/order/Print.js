import React, { Component, Fragment} from 'react'

import { connect } from 'react-redux';

import getData from '../../../helpers/getData';import Moment from 'moment';


 class Print extends Component {
    constructor(props) {
        super(props);
        this.state = { 
           item: ''
        };

    }
  
     
  
    componentDidMount(){

      this.fetchData(this.props.match.params.id);    
 }

 fetchData(elmntId) {

  getData(this.props.defaultSets.apiUrl+'orders/'+elmntId, this.props.userInfo.accessToken)

       //.then(response => response.json())
       .then(response => {

         this.setState({ item: response.data});
           console.log(response.data)
       })
       .catch(function (error) {
           console.log(error);
       });
 }

  print_specific_div_content(){

  var win = window.open('','','left=0,top=0,width=764,height=677,toolbar=0,scrollbars=0,status =0');

  var content = this.props.selectedLanguage.langCode == "AR" ? "<html dir='rtl'>" : "<html dir='ltr'>";
  content += "<head><title>"+this.state.item.user.full_name+"</title>";
  content += "<style> table { font-family: Arial, Helvetica, sans-serif; border-collapse: collapse; width: 100%; } table td, table th { border: 1px solid #ddd; padding: 8px; } table tr:nth-child(even){background-color: #f2f2f2;} table tr:hover {background-color: #ddd;} table th { padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; color: white; } </style>"
  content += "</head>";
  content += "<body onload=\"window.print(); window.close();\">";
  content += document.getElementById("divToPrint").innerHTML ;
  content += "</body>";
  content += "</html>";
  win.document.write(content);
  win.document.close();
}
    render() {
        
        const {isDisabled, item } = this.state;
        const lng = this.props.selectedLanguage;
        let subTotalSum = 0;
        return (
                   
<Fragment>

  <div id="divToPrint">

<div className="row">
          <div className="col-md-12">
            <div className="card">
              
              <div className="card-header card-header-primary">
                <h4 className="card-title ">{lng.shippingTxt}</h4>
                <p className="card-order"></p>
              </div>

              <div className="card-body">
                <div className="table-responsive">
                  <table className="table">
                    
                  <thead className="text-primary">
                        <tr>
                            <th>{lng.orderIdTxt}</th>
                            <th>{lng.customerNameTxt}</th>
                            <th>{lng.totalTxt}</th>
                            <th>{lng.countryTxt}</th>
                            <th>{lng.cityTxt}</th>
                            <th>{lng.adrsTxt}</th>                            
                            <th>{lng.dateTxt}</th>
                        </tr>
                    </thead>
                    <tbody>
                      {item && 
                                <tr>
                                    <td>{item.orderId}</td>

                                 <td>{item.user.full_name}</td>
                                  <td >
                                      {item.finalTotal} {item.transaction_currency}
                                  </td>
                                  <td>{eval("item.shipmentadrs.country.title"+lng.langCode)}</td>
                                  <td>{eval("item.shipmentadrs.city.title"+lng.langCode)}</td>
                                  <td>{item.shipmentadrs.adrs}</td>
                                  <td >
                                  {Moment(item.created_at).format("YYYY-MM-DD  H:m")}
                                  </td>                                   
                                
                                </tr>}
                            
                    </tbody>
                  </table>
                </div>
              </div>
            
            </div>
          </div>

          </div>
      
  
          <div className="row">
          <div className="col-md-12">
            <div className="card">
              
              <div className="card-header card-header-primary">
                <h4 className="card-title ">{lng.orderDetailsTxt}</h4>
                <p className="card-order"></p>
              </div>

              <div className="card-body">
                <div className="table-responsive">
                 
                 
                <table className="table table-bordered table-hover">
                                      <thead>
                                          <tr>
                                              <th >{'#'}</th>
                                              <th>{lng.productTitleTxt}</th>
                                              <th>{lng.prcBfrDscnt}</th>
                                              <th>{lng.prcAftrDscnt}</th>
                                              <th>{lng.colourTxt}</th>
                                              <th>{lng.sizeTxt}</th>
                                              <th>{lng.qtyTxt}</th>
                                              <th>{lng.subTotalTxt}</th>
                                          </tr>
                                      </thead>
                                      <tbody>
                                      {item.order_items ? item.order_items.map((row, k) => {
                                        let subTotal = row.quantity * row.priceAfterDiscount;
                                        subTotalSum += subTotal;
                                        const elmntTrns = row.elment_trans.languageCode == lng.langCode ? row.elment_trans : null;

                                                      return(
                                                          
                                          <tr key={'oi'+k}>
                                            <td>
                                                  {k + 1}
                                              </td>
                                              <td>{elmntTrns && elmntTrns.title}</td>
                                                    
                                            <td >
                                                {row.priceBeforeDiscount}
                                            </td>
                                                                                    
                                            <td >
                                                {row.priceAfterDiscount}
                                            </td>
                                                                                    
                                            <td>
                                            {row.colour && eval("row.colour.title"+lng.langCode)}
                                            </td>
                                                                                    
                                            <td>
                                            {row.size && eval("row.size.title"+lng.langCode)}
                                            </td>
                                                                                    
                                            <td>
                                                {row.quantity}
                                            </td>
                                                                                    
                                            <td>
                                                {subTotal}
                                            </td>
                                            
                                          </tr>
                                          
                                          );
                                      }
                                      ): null}                    
                                      </tbody>
                                        <tfoot>
                                          <tr>
                                              <th colSpan="8" className="text-center">{lng.totalTxt} : {subTotalSum}</th>
                                              
                                          </tr>
                                      </tfoot> 
                                  </table>
                                     
                
                </div>
              </div>
            
            </div>
          </div>


          </div>
  
          </div>


          <button 
                                                  className="btn btn-primary"
                                                  onClick={() => this.print_specific_div_content()}>
                                                    {lng.printTxt}
                                                    </button>
</Fragment>
      
        )
    }
}


const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        defaultSets: state.DfltSts,
        userInfo: state.User.info
    }
}
export default connect(mapStateToProps, null)(Print);
