import React, { Component, Fragment} from 'react'

import {toast} from 'react-toastify';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { Link } from 'react-router-dom';
import RolesSelect from '../../../components/RolesSelect';
import getData from '../../../helpers/getData';
import putData from '../../../helpers/putData';
import RowThumbnail from '../../../components/RowThumbnail';
import ImgToBeUploaded from '../../../components/ImageToUpload';
import { checkMimeType } from '../../../helpers/checkMimeType';
 class Edit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: 0,
            firstName: '',
            lastName: '',
            phone: '',
            email: '',
            website: '',
            region: '',
            password: '', 
            adrs: '',
            status:1,
            groupId: 1,
            notes: '',
            //agent info.
            companyName: '',
            companyOwner: '',
            city: '',
            theState: '',
            zipCode: '',
            fax: '',
            foundationYear: '',
            employeesCount: '',
            salePointsCount: '',
            licenseType: '',
            otherSourcesNames: '',
            activityCountries: '',
            // agent info end.

            errorMessage: '',
            selectedRoles: [],
            isDisabled: false,
            
            // here i load original images of an element that comes from db.
            photo: '',
            //.
            
            // here the new selected images will be loaded.
            mainImg: null,            
            //.
        };

        
        this.onChangeValue = this.onChangeValue.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.onRolesSelectChange = this.onRolesSelectChange.bind(this);

        
        //the next functions handle images on selected, change and upload.
        this.onMainImageChange = this.onMainImageChange.bind(this);
        this.rmvSlctdMnImg = this.rmvSlctdMnImg.bind(this);        
        this.hndlUpldMnImg = this.hndlUpldMnImg.bind(this);        
        this.rmvMnImg = this.rmvMnImg.bind(this);      
        //.

    }
  
    componentDidMount(){

      const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);
 
      getData(this.props.defaultSets.apiUrl+'users/'+this.props.match.params.id+'/edit', this.props.userInfo.accessToken)

          //.then(response => response.json())
          .then(response => {

            let roles = [];
            {response.data.roles && response.data.roles.map((item, i) => {
              roles.push({
                "label": eval("item.title"+this.props.selectedLanguage.langCode), 
                "value": item.id
              })
            })};

            this.setState({ 
                id: response.data.id,
                firstName: response.data.firstName,
                lastName: response.data.lastName,
                phone: response.data.phone,
                email: response.data.email,
                website: response.data.website,
                region: response.data.region,
                adrs: response.data.adrs,
                country: response.data.country,
                notes: response.data.notes,

                //agent info.
                companyName: response.data.companyName,
                companyOwner: response.data.companyOwner,
                city:response.data.city,
                theState: response.data.theState,
                zipCode: response.data.zipCode,
                fax: response.data.fax,
                foundationYear: response.data.foundationYear,
                employeesCount: response.data.employeesCount,
                salePointsCount: response.data.salePointsCount,
                licenseType: response.data.licenseType,
                otherSourcesNames: response.data.otherSourcesNames,
                activityCountries: response.data.activityCountries,
                // agent info end.                

                selectedRoles: roles,                
                status: response.data.status,        
                groupId: response.data.groupId,
              });
              toast.dismiss(ldToast)
              //console.log(response.data)
          })
          .catch(function (error) {
              console.log(error);
          })
    }


    
 onMainImageChange(event) {
  if (event.target.files && event.target.files[0]) {

    let img = event.target.files[0];

    let mimeTypeCheck = checkMimeType(event, this.props.selectedLanguage.notAllowedTxt);
    if(mimeTypeCheck == true){

      return this.setState({
        mainImg: img 
      }); 
    } 
    toast.error(mimeTypeCheck);
      
  }
};
    // The next function is used inside handleSubmit function not used directrly anywhere 
    // so after row have been inserted it uploads its photo.
    
    hndlUpldMnImg(rowId) {
      if (this.state.mainImg == null ) {
        return false;
      }

      // then prepare a form to send files.
      const formData = new FormData()
      
      // The next two lines go to FilesController to specify the row and tbl where inserted.
      formData.append('rowId', rowId);
      formData.append('type', 'user');
      formData.append('mainPhoto', '1');
      //.

      
      // append the main photo to formData.
      formData.append(
        'files[]',
        this.state.mainImg,
        this.state.mainImg.name
      )
      //.

      fetch(this.props.defaultSets.apiUrl+'upload-files', {
        method: 'POST',
        body: formData
      })
      .then(response => response.json())
      .then(response => {
        this.setState({
          mainImg: null,
        });
        /* onUploadProgress: progressEvent => {
          console.log(progressEvent.loaded / progressEvent.total)
        } */
      })
    }

 rmvSlctdMnImg() { // handle selected photo not main photo that comes from db.
  this.setState({
    mainImg: null
  })
}

rmvMnImg() { //handle main photo that comes from db not a selected one.
  this.setState({
    photo: null
  });
}

    onRolesSelectChange(selectedRoles) {
      this.setState({selectedRoles})
    }

    onChangeValue(e) {

        this.setState({
            [e.target.name]: e.target.value
        });
    }
    
    handleSubmit(e){

        e.preventDefault();
        this.setState({isDisabled: true});
        const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

        // prepare selected roles to be sent.
        let roles = [];
        {this.state.selectedRoles && this.state.selectedRoles.map((item, i) => {
          roles.push(item.value);
        })}
        //.

          putData(this.props.defaultSets.apiUrl+'users/'+this.state.id, {
            id: this.props.match.params.id,
            firstName: this.state.firstName,
            lastName: this.state.lastName,
            phone: this.state.phone,
            email: this.state.email,
            website: this.state.website,
            region: this.state.region,
            password: this.state.password,
            adrs: this.state.adrs,
            notes: this.state.notes,

            // agent info start. 
            companyName: this.state.companyName,
            companyOwner: this.state.companyOwner,
            email: this.state.email,
            adrs: this.state.adrs,
            city: this.state.city,
            theState: this.state.theState,
            zipCode: this.state.zipCode,
            country: this.state.country,
            phone: this.state.phone,
            fax: this.state.fax,
            website: this.state.website,
            foundationYear: this.state.foundationYear,
            employeesCount: this.state.employeesCount,
            salePointsCount: this.state.salePointsCount,
            licenseType: this.state.licenseType,
            otherSourcesNames: this.state.otherSourcesNames,
            activityCountries: this.state.activityCountries,
            // agent info end.                

            roles: roles,
            status: this.state.status,
            groupId: this.state.groupId,
            photo: this.state.mainImg || this.state.photo,
            langCode: this.props.selectedLanguage.langCode
        }, this.props.userInfo.accessToken)
            .then( (response) => {

              if(response.data.success == true) {
                this.hndlUpldMnImg(this.state.id);

                toast.update(ldToast, {
                    type: toast.TYPE.SUCCESS,
                    render: response.data.msg
                });

                setTimeout( () => {
                   // this.props.history.push('/admin/users');
                }, 3000);

              } else {

                toast.update(ldToast, {
                    type: toast.TYPE.ERROR,
                    render: response.data.msg
                });
              }

              
            })
            .catch( (error) => {
                //console.log(error.response);
                this.setState({  isDisabled: false})

                if(error.response !== undefined && error.response.status === 422){
                    let errorTxt = '';
                    let errorMessage = error.response.data.errors;

                    Object.keys(errorMessage).map((key, i) => {
                        errorTxt = (<span> {errorTxt} {errorMessage[key][0]} <br /></span>);});
                    toast.update(ldToast, {
                        type: toast.TYPE.ERROR,
                        render: errorTxt
                    });
                }
            });

    }


    render() {
        
        const {isDisabled,firstName, lastName, phone, email, website, adrs, notes, 
          selectedRoles, groupId, photo, mainImg, region,
          status,
          companyName,
          companyOwner,
          country,
          city,
          theState,
          zipCode,
          fax,
          foundationYear,
          employeesCount,
          salePointsCount,
          licenseType,
          otherSourcesNames,
          activityCountries} = this.state;
        
        const lng = this.props.selectedLanguage;

        return (
          <div className="row">

<Helmet><title>{lng.updtAccntTxt}</title></Helmet> 

                  
<div className="col-md-12">
        
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item"><Link to="/admin"><i className="fa fa-home"></i></Link></li>
            <li className="breadcrumb-item"><Link to="/admin/users">{lng.usersTxt}</Link></li>
            <li className="breadcrumb-item active" aria-current="page">{lng.updtAccntTxt}</li>
          </ol>
        </nav>
      </div>

          <div className="col-md-8">
            <div className="card">
              <div className="card-header card-header-primary">
                <h4 className="card-title">{lng.updtAccntTxt}</h4>
                <p className="card-category"></p>
              </div>
              <div className="card-body">
                <form>
                  
                  {groupId != 3 && 
                  <div className="row">
                    <div className="col-md-6">
                      <div className="form-group">
                        <label className="bmd-label-floating">{lng.firstNameTxt}</label>
                         
                        <input 
                        className="form-control"
                        type="text"
                        name="firstName"
                        defaultValue={firstName}
                        onChange={this.onChangeValue} />
                      </div>
                    </div>

                    <div className="col-md-6">
                      <div className="form-group">
                        <label className="bmd-label-floating">{lng.lastNameTxt}</label>                            
                        <input 
                        className="form-control"
                        type="text"
                        name="lastName"
                        defaultValue={lastName}
                        onChange={this.onChangeValue} />
                      </div>
                    </div>

                  </div>}


                  <div className="row">
                    <div className="col-md-6">
                      <div className="form-group">
                        <label className="bmd-label-floating">{lng.phoneTxt}</label>                           
                        <input 
                        className="form-control"
                        type="text"
                        name="phone"
                        defaultValue={phone}
                        onChange={this.onChangeValue} />
                      </div>
                    </div>
                    <div className="col-md-6">
                      <div className="form-group">
                        <label className="bmd-label-floating">{lng.passwordTxt}</label>                       
                        <input 
                        className="form-control"
                        type="password"
                        name="password"
                        /*autoComplete={false}*/
                        onChange={this.onChangeValue} />
                      </div>
                    </div>
                  </div>
                  
                  
                  <div className="row">                      

<div className="col-md-6">
    <div className="form-group">
      <label className="bmd-label-floating">{lng.emailTxt}</label>                            
      <input 
      className="form-control"
      type="text"
      name="email"
      defaultValue={email}
      onChange={this.onChangeValue} />
    </div>
</div>


<div className="col-md-6">
  <div className="form-group">
  <label className="bmd-label-floating">{lng.typeTxt}</label>                     

    <select 
    value={groupId}
    name="groupId"
    className="form-control"
    onChange={this.onChangeValue}>
      <option value="1">{lng.adminTxt}</option>
      <option value="2">{lng.clientTxt}</option>
      <option value="3">{lng.agentTxt}</option>
    </select>
  </div>
</div>

</div>

    <div className="row">
{ groupId == 1 &&
    <div className="col-md-12">
    <label className="bmd-label-floating">{lng.rolesTxt}</label>                     


        <RolesSelect
        selected={selectedRoles}
        onRolesSelectChange={this.onRolesSelectChange}
        defaultSets={this.props.defaultSets}
        userInfo={this.props.userInfo}
        selectedLanguage={this.props.selectedLanguage} />
    </div>

  }
                  </div>  

                  <div className="row">

                    <div className="col-md-6">
                      <div className="form-group">
                        <label className="bmd-label-floating">{lng.statusTxt}</label>                     
                        <select 
                        value={status}
                        name="status"
                        className="form-control"
                        onChange={this.onChangeValue}>
                          <option value="1">{lng.activeTxt}</option>
                          <option value="0">{lng.inActiveTxt}</option>
                        </select>
                      </div>
                    </div>

                    <div className="col-md-6">
                      <div className="form-group">
                        <label className="bmd-label-floating">{lng.regionTxt}</label>                     
                        <select 
                        value={region}
                        name="region"
                        className="form-control"
                        onChange={this.onChangeValue}>
                          <option value="1">{lng.americaTxt}</option>
                          <option value="2">{lng.russiaTxt}</option>
                          <option value="3">{lng.middleEastTxt}</option>
                          <option value="4">{lng.latinAmercaTxt}</option>
                          <option value="5">{lng.europeTxt}</option>
                          <option value="6">{lng.asiaPacificTxt}</option>
                          <option value="7">{lng.australiaTxt}</option>
                        </select>
                      </div>
                    </div>
                  
                  </div>
                  <div className="row mt-3">
                    <div className="col-md-12">
                      <div className="form-group">
                        <label className="bmd-label-floating">{lng.adrsTxt}</label>                     
                        <input 
                        className="form-control"
                        type="text"
                        name="adrs"
                        defaultValue={adrs}
                        onChange={this.onChangeValue} />
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-12">
                      <div className="form-group">
                        <label></label>
                        <div className="form-group">
                          <label className="bmd-label-floating">{lng.notesTxt}</label>
                          <textarea className="form-control" 
                          rows="5" name="notes"
                          value={notes || ''}
                          onChange={this.onChangeValue}>
                          </textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                  
                  


 {groupId == 3 && 
 <div>
                  <div className="row">
                    <div className="col-md-6">
                      <div className="form-group">
                        <label className="bmd-label-floating">{lng.companyNameTxt}</label>
                         
                        <input 
                        className="form-control"
                        type="text"
                        name="companyName"
                        defaultValue={companyName}
                        onChange={this.onChangeValue} />
                      </div>
                    </div>

                    <div className="col-md-6">
                      <div className="form-group">
                        <label className="bmd-label-floating">{lng.companyOwnerTxt}</label>
                         
                        <input 
                        className="form-control"
                        type="text"
                        name="companyOwner"
                        defaultValue={companyOwner}
                        onChange={this.onChangeValue} />
                      </div>
                    </div>

                  </div>
 
                  <div className="row">
  

                    <div className="col-md-6">
                      <div className="form-group">
                        <label className="bmd-label-floating">{lng.cityTxt}</label>                            
                        <input 
                        className="form-control"
                        type="text"
                        name="city"
                        defaultValue={city}
                        onChange={this.onChangeValue} />
                      </div>
                    </div>


                    <div className="col-md-6">
                      <div className="form-group">
                        <label className="bmd-label-floating">{lng.licenseTypeTxt}</label>                            
                        <input 
                        className="form-control"
                        type="text"
                        name="licenseType"
                        defaultValue={licenseType}
                        onChange={this.onChangeValue} />
                      </div>
                    </div>

                  </div>
 
                  <div className="row">
                    <div className="col-md-6">
                      <div className="form-group">
                        <label className="bmd-label-floating">{lng.zipCodeTxt}</label>
                         
                        <input 
                        className="form-control"
                        type="text"
                        name="zipCode"
                        defaultValue={zipCode}
                        onChange={this.onChangeValue} />
                      </div>
                    </div>

                    <div className="col-md-6">
                      <div className="form-group">
                        <label className="bmd-label-floating">{lng.countryTxt}</label>                            
                        <input 
                        className="form-control"
                        type="text"
                        name="country"
                        defaultValue={country}
                        onChange={this.onChangeValue} />
                      </div>
                    </div>

                  </div>
 
                  <div className="row">
                    <div className="col-md-6">
                      <div className="form-group">
                        <label className="bmd-label-floating">{lng.faxTxt}</label>
                         
                        <input 
                        className="form-control"
                        type="text"
                        name="fax"
                        defaultValue={fax}
                        onChange={this.onChangeValue} />
                      </div>
                    </div>

                    <div className="col-md-6">
                      <div className="form-group">
                        <label className="bmd-label-floating">{lng.websiteTxt}</label>                            
                        <input 
                        className="form-control"
                        type="text"
                        name="website"
                        defaultValue={website}
                        onChange={this.onChangeValue} />
                      </div>
                    </div>

                  </div>
 
                  <div className="row">
                    <div className="col-md-6">
                      <div className="form-group">
                        <label className="bmd-label-floating">{lng.foundationYearTxt}</label>
                         
                        <input 
                        className="form-control"
                        type="text"
                        name="foundationYear"
                        defaultValue={foundationYear}
                        onChange={this.onChangeValue} />
                      </div>
                    </div>

                    <div className="col-md-6">
                      <div className="form-group">
                        <label className="bmd-label-floating">{lng.employeesCountTxt}</label>                            
                        <input 
                        className="form-control"
                        type="text"
                        name="employeesCount"
                        defaultValue={employeesCount}
                        onChange={this.onChangeValue} />
                      </div>
                    </div>

                  </div>
 
                  <div className="row">
                    <div className="col-md-6">
                      <div className="form-group">
                        <label className="bmd-label-floating">{lng.salePointsCountTxt}</label>
                         
                        <input 
                        className="form-control"
                        type="text"
                        name="salePointsCount"
                        defaultValue={salePointsCount}
                        onChange={this.onChangeValue} />
                      </div>
                    </div>


                    <div className="col-md-6">
                      <div className="form-group">
                        <label className="bmd-label-floating">{lng.activityCountriesTxt}</label>                            
                        <input 
                        className="form-control"
                        type="text"
                        name="activityCountries"
                        defaultValue={activityCountries}
                        onChange={this.onChangeValue} />
                      </div>
                    </div>

                  </div>
 
                  <div className="row">
                    <div className="col-md-12">
                      <div className="form-group">
                        <label className="bmd-label-floating">{lng.otherSourcesNamesTxt}</label>
                         
                        <input 
                        className="form-control"
                        type="text"
                        name="otherSourcesNames"
                        defaultValue={otherSourcesNames}
                        onChange={this.onChangeValue} />
                      </div>
                    </div>

                  </div>

 </div>}
                  
                  
                  <button 
                  type="submit" 
                  onClick={this.handleSubmit}
                  disabled={isDisabled}
                  className="btn btn-primary pull-right">{lng.saveTxt}
                  </button>
                  <div className="clearfix"></div>
                </form>
              </div>
            </div>
          </div>
          

          
              
        <div className="col-md-4">
         

         <div className="col-md-12">
            
         <div className="card card-profile">
               <div className="card-avatar">.</div>
               <div className="card-body">
                 <h6 className="card-size text-gray"></h6>
                 { <h5 className="card-title mb-3">{lng.mainPhotoTxt}</h5>}
                 
                 <div className="card-description">
                   <RowThumbnail imgName={photo} rmvMnImg={this.rmvMnImg} />
                    
                   <ImgToBeUploaded
                   image={mainImg} 
                   rmvSlctdMnImg={this.rmvSlctdMnImg} />
                   
                   <input type="file"
                   accept=".gif,.jpg,.jpeg,.png" 
                   onChange={this.onMainImageChange} />
                 </div>
                 
                 {/* <button 
                 onClick={this.handleUpload}
                 className="btn btn-primary btn-round">
                   Upload
                 </button> */}
               </div>
             </div>
          
         </div>
       
      
       </div>


        </div>
      
        

        )
    }
}


const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        defaultSets: state.DfltSts,
        userInfo: state.User.info
    }
}
export default connect(mapStateToProps, null)(Edit);
