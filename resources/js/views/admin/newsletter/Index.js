import React, { Component, Fragment} from 'react'

import {toast} from 'react-toastify';
import { connect } from 'react-redux';
import NewsletterRow from '../../../components/NewsletterRow';

import {Helmet} from "react-helmet";
import { Link } from 'react-router-dom';
import TitleFilter from '../../../components/filters/TitleFilter';
import StatusFilter from '../../../components/filters/StatusFilter';
import getData from '../../../helpers/getData';

 class Index extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            rows: '',
            rowsBfrFltr: [],
            email: '',
        };
        this.onValueChange = this.onValueChange.bind(this);

    }
   
   
    componentDidMount(){

      const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

      getData(this.props.defaultSets.apiUrl+'newsletters', this.props.userInfo.accessToken)

          //.then(response => response.json())
          .then(response => {
              this.setState({ rows: response.data.rows, rowsBfrFltr: response.data.rows });
              toast.dismiss(ldToast)
              //console.log(response.data)
          })
          .catch(function (error) {
              console.log(error);
          })
    }

    onValueChange(e) {
      //console.log(e.target.value)
      this.setState({
        [e.target.name]: e.target.value
    }, () => {
        this.tblFilter();
    });
    }

    tblFilter() {
              
       let newRows = this.state.rowsBfrFltr;      
       
        if(this.state.email != '') {
          newRows = newRows.filter(row => row.email.search(this.state.email) !== -1 );
        }     
       
       this.setState({rows: newRows});
    }
 

    render() {
        
        //const {isLoading} = this.state;
        const { 
           emailTxt, 
           //crtNewTxt, 
           newsletterTxt,
           actionTxt} = this.props.selectedLanguage;

        return (
           
         <Fragment>
                <Helmet><title>{newsletterTxt}</title></Helmet>

          <div className="row">                    
                  
            <div className="col-md-12">
        
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item"><Link to="/admin"><i className="fa fa-home"></i></Link></li>
            <li className="breadcrumb-item active" aria-current="page">{newsletterTxt}</li>
          </ol>
        </nav>
</div>


            <div className="col-md-12">

              <div className="row">
                  {/* <div className="col-md-6">
                  <Link to='/admin/create-newsletter' className="btn btn-primary">
                      {crtNewTxt}
                  </Link>
                  </div> */}


                  <div className="col-md-12">
                <input 
                type="text" 
                className="form-control" 
                name="email"
                placeholder={this.props.selectedLanguage.emailTxt}
                onKeyUp={this.onValueChange}
                />
            </div>                 
                  
          
              </div>
                
            </div>
          
          </div>

          <div className="row">
          <div className="col-md-12">
            <div className="card">
              
              <div className="card-header card-header-primary">
                <h4 className="card-title ">{newsletterTxt}</h4>
                <p className="card-newsletter"></p>
              </div>

              <div className="card-body">
                <div className="table-responsive">
                  <table className="table">
                    <thead className=" text-primary">                    
                      <tr>
                      <th>{'#'}</th>
                      <th>{emailTxt}</th>
                      <th>{actionTxt}</th>
                      </tr>                      
                    </thead>
                    <tbody>
                            {this.state.rows ? 
                            this.state.rows.map((item, index) => {
                              return(<NewsletterRow 
                                obj={item} num={index + 1} key={index}
                                defaultSets = {this.props.defaultSets}
                                selectedLanguage = {this.props.selectedLanguage}
                                userInfo = {this.props.userInfo}
                                />)
                            }) : null
                            }
                            
                    </tbody>
                  </table>
                </div>
              </div>
            
            </div>
          </div>

          </div>
      
         </Fragment>

        )
    }
}


const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        defaultSets: state.DfltSts,
        userInfo: state.User.info
    }
}
export default connect(mapStateToProps, null)(Index);
