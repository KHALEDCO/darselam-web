import React, { Component, Fragment} from 'react'

import {toast} from 'react-toastify';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import GuaranteeRow from '../../../components/GuaranteeRow';

import {Helmet} from "react-helmet";
import getData from '../../../helpers/getData';

 class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            rows: ''
        };
    } 
  
   
    componentDidMount(){

      const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

      getData(this.props.defaultSets.apiUrl+'guarantees', this.props.userInfo.accessToken)
          //.then(response => response.json())
          .then(response => {
              this.setState({ rows: response.data.rows });
              toast.dismiss(ldToast)
              //console.log(response.data)
          })
          .catch(function (error) {
              console.log(error);
          })
    }

    render() {
        
        //const {isLoading} = this.state;
        const lng = this.props.selectedLanguage;

        return (
           
         <Fragment>
                
          <div className="row">
          <Helmet><title>{lng.guaranteesTxt}</title></Helmet>

          <div className="col-md-12">
                  
                  <nav aria-label="breadcrumb">
                    <ol className="breadcrumb">
                      <li className="breadcrumb-item"><Link to="/admin"><i className="fa fa-home"></i></Link></li>
                      <li className="breadcrumb-item active" aria-current="page">{lng.guaranteesTxt}</li>
                    </ol>
                  </nav>
                </div>
                
            <div className="col-md-12">
                <Link to='/admin/create-guarantee' className="btn btn-primary">
                    {lng.crtNewTxt}
                </Link>
            </div>
          </div>

          <div className="row">
          <div className="col-md-12">
            <div className="card">
              
              <div className="card-header card-header-primary">
                <h4 className="card-title ">{lng.guaranteesTxt}</h4>
                <p className="card-category"></p>
              </div>
 
              <div className="card-body">
                <div className="table-responsive">
                  <table className="table">
                    <thead className=" text-primary">                    
                      <tr>
                      <th>{'#'}</th>
                      <th>{lng.productTitleTxt}</th>
                      <th>{lng.serialNumTxt}</th>
                      <th>{lng.guaranteeNumTxt}</th>
                      <th>{lng.statusTxt}</th>
                      <th>{lng.actionTxt}</th>
                      </tr>                      
                    </thead>
                    <tbody>
                            {this.state.rows ? 
                            this.state.rows.map((item, index) => {
                              return(<GuaranteeRow obj={item} key={item.id} num={index+1}
                                defaultSets = {this.props.defaultSets}
                                selectedLanguage = {this.props.selectedLanguage}
                                userInfo = {this.props.userInfo}
                                />)
                            }) : null
                            }
                            
                    </tbody>
                  </table>
                </div>
              </div>
            
            </div>
          </div>

          </div>
      
         </Fragment>

        )
    }
}


const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        defaultSets: state.DfltSts,
        userInfo: state.User.info
    }
}
export default connect(mapStateToProps, null)(Index);
