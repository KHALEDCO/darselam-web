import React, { Component, Fragment} from 'react'

import {toast} from 'react-toastify';
import { connect } from 'react-redux';

import ImgToBeUploaded from '../../../components/ImageToUpload';
import RowThumbnail from '../../../components/RowThumbnail';

import { Link } from 'react-router-dom';
import {Helmet} from "react-helmet";
import CKEditor from 'ckeditor4-react';

import getData from '../../../helpers/getData';
import putData from '../../../helpers/putData';
import { checkMimeType } from '../../../helpers/checkMimeType';
import ProductsSelect from '../../../components/ProductsSelect';
 
 class Edit extends Component {
    constructor(props) {
        super(props);
        this.state = {
          
            id:0,
            productId: '',
            serialNum: '',
            guaranteeNum: '',
            status: 1,
            errorMessage: '',
            isDisabled: false,
        };

        
        this.onChangeValue = this.onChangeValue.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.fetchData = this.fetchData.bind(this);

    
        
    }
  
     
    componentDidMount(){

      this.fetchData(this.props.match.params.id);    

    }

 fetchData(elmntId) {
   
  getData(this.props.defaultSets.apiUrl+'guarantees/'+elmntId+'/edit', this.props.userInfo.accessToken)
       //.then(response => response.json())
       .then(response => {

         this.setState({ 
             id: elmntId,
             productId: response.data.productId,        
             serialNum: response.data.serialNum,
             guaranteeNum: response.data.guaranteeNum,     
             status: response.data.status,
             
           });
           //console.log(response.data)
       })
       .catch(function (error) {
           console.log(error);
       });
 }



    onChangeValue(e) {
      //console.log(e.target.value)
        this.setState({
            [e.target.name]: e.target.value,  isDisabled: false
        });
    }
    
    handleSubmit(e){
//console.log('k'+this.state.status)
        e.preventDefault();
        this.setState({isDisabled: true});
        const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

        
        putData(this.props.defaultSets.apiUrl+'guarantees/'+this.state.id, {
          productId: this.state.productId,
          id: this.state.id,
          serialNum: this.state.serialNum,
          guaranteeNum: this.state.guaranteeNum,
          status: this.state.status,
          langCode: this.props.selectedLanguage.langCode
      }, this.props.userInfo.accessToken)
        
        
            .then( (response) => {

              if(response.data.success == false) {
                return toast.update(ldToast, {
                    type: toast.TYPE.ERROR,
                    render: response.data.msg
                });
              }
              return  toast.update(ldToast, {
                    type: toast.TYPE.SUCCESS,
                    render: response.data.msg
                });
            })
            .catch( (error) => {
                //console.log(error.response);
                this.setState({  isDisabled: false})

                if(error.response !== undefined && error.response.status === 422){
                    let errorTxt = '';
                    let errorMessage = error.response.data.errors;

                    Object.keys(errorMessage).map((key, i) => {
                        errorTxt = (<span> {errorTxt} {errorMessage[key][0]} <br /></span>);});
                    toast.update(ldToast, {
                        type: toast.TYPE.ERROR,
                        render: errorTxt
                    });
                }
            });

    }


    render() {
        
        
      const { isDisabled, status, productId, serialNum, guaranteeNum } = this.state;

        const lng = this.props.selectedLanguage;

        return (
                   
          
          <div className="row">
            <Helmet><title>{lng.updtGuaranteeTxt}</title></Helmet>

          <div className="col-md-12">
                  
            <nav aria-label="breadcrumb">
              <ol className="breadcrumb">
                <li className="breadcrumb-item"><Link to="/admin"><i className="fa fa-home"></i></Link></li>
                <li className="breadcrumb-item"><Link to="/admin/guarantees">{lng.guaranteesTxt}</Link></li>
                <li className="breadcrumb-item active" aria-current="page">{lng.updtGuaranteeTxt}</li>
              </ol>
            </nav>
          </div>
          
          <div className="col-md-8">
            <div className="card">
              <div className="card-header card-header-primary">
                <h4 className="card-title">{lng.updtGuaranteeTxt}</h4>
                <p className="card-category"></p>
              </div>
              <div className="card-body">
                <form>
                      
                
                <div className="row mb-3">
                  <div className="col-md-6">
                    <label className="bmd-label-floating">{lng.productTitleTxt}</label>

                    <ProductsSelect
                      productId={productId}
                      onChangeValue={this.onChangeValue}
                      defaultSets={this.props.defaultSets}
                      selectedLanguage={this.props.selectedLanguage} />
                  </div>

                  <div className="col-md-6">
                      <div className="form-group">
                        <label className="bmd-label-floating">{lng.statusTxt}</label>                     
                        <select 
                        value={status}
                        name="status"
                        className="form-control"
                        onChange={this.onChangeValue}>
                          <option value="1">{lng.activeTxt}</option>
                          <option value="0">{lng.inActiveTxt}</option>
                        </select>
                      </div>
                    </div>

                </div> 


                <div className="row">

<div className="col-md-6">
  <div className="form-group">
    <label className="bmd-label-floating">{lng.serialNumTxt}</label>                          
    <input 
    className="form-control"
    type="text"
    name="serialNum"
    value={serialNum}
    onChange={this.onChangeValue} />
  </div>
</div>

<div className="col-md-6">
  <div className="form-group">
    <label className="bmd-label-floating">{lng.guaranteeNumTxt}</label>                          
    <input 
    className="form-control"
    type="text"
    name="guaranteeNum"
    value={guaranteeNum}
    onChange={this.onChangeValue} />
  </div>
</div>
</div>


                  <button 
                  type="submit" 
                  onClick={this.handleSubmit}
                  disabled={isDisabled}
                  className="btn btn-primary pull-right">{lng.saveTxt}
                  </button>
                  <div className="clearfix"></div>
                </form>
              </div>
            </div>
          </div>
          

          
        </div>
      
        )
    }
}


const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        defaultSets: state.DfltSts,
        userInfo: state.User.info,
    }
}
export default connect(mapStateToProps, null)(Edit);
