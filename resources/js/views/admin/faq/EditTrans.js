import React, { Component, Fragment} from 'react'

import {toast} from 'react-toastify';
import { connect } from 'react-redux';

import CKEditor from 'ckeditor4-react';

import {Helmet} from "react-helmet";
import { Link } from 'react-router-dom';
import getData from '../../../helpers/getData';
import { checkMimeType } from '../../../helpers/checkMimeType';
import putData from '../../../helpers/putData';

 class EditTrans extends Component {
    constructor(props) { 
        super(props); 
        this.state = {
            id:0,
            description1: '',
            description2: '',
            errorMessage: '',
        };

        
        this.onChangeValue = this.onChangeValue.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.fetchData = this.fetchData.bind(this);

    }
  
     
  
    componentDidMount(){

      this.fetchData(this.props.match.params.id);               
 }

 fetchData(elmntId) {

  getData(this.props.defaultSets.apiUrl+'faqs-trans/'+elmntId+'/edit', this.props.userInfo.accessToken)

       //.then(response => response.json())
       .then(response => {

         this.setState({ 
             id: elmntId,
             
             description1: response.data.description1,
             description2: response.data.description2,
             //photo: response.data.photo,
             
           });
           //console.log(response.data)
       })
       .catch(function (error) {
           console.log(error);
       });
 }




    onChangeValue(e) {
      //console.log(e.target.value)
        this.setState({
            [e.target.name]: e.target.value
        });
    }
    
    handleSubmit(e){
//console.log('k'+this.state.status)
        e.preventDefault();
        
        const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

          putData(this.props.defaultSets.apiUrl+'faqs-trans/'+this.state.id, {
            id: this.state.id,
            
            description1: this.state.description1,
            description2: this.state.description2,
            //photo: this.state.mainImg || this.state.photo,
            langCode: this.props.selectedLanguage.langCode
        }, this.props.userInfo.accessToken)
            .then( (response) => {

                toast.update(ldToast, {
                    type: toast.TYPE.SUCCESS,
                    render: response.data.msg
                });

               /*  setTimeout( () => {
                    this.props.history.push('/Faqs');
                }, 3000); */
            })
            .catch( (error) => {
                //console.log(error.response);

                if(error.response !== undefined && error.response.status === 422){
                    let errorTxt = '';
                    let errorMessage = error.response.data.errors;

                    Object.keys(errorMessage).map((key, i) => {
                        errorTxt = (<span> {errorTxt} {errorMessage[key][0]} <br /></span>);});
                    toast.update(ldToast, {
                        type: toast.TYPE.ERROR,
                        render: errorTxt
                    });
                }
            });

    }


    render() {
        
        
        const {description1, description2} = this.state;

        const lng = this.props.selectedLanguage;

        return (
                   

          <div className="row">
            
<Helmet><title>{lng.updateTransTxt}</title></Helmet>

                  
<div className="col-md-12">
        
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item"><Link to="/admin"><i className="fa fa-home"></i></Link></li>
            <li className="breadcrumb-item"><Link to="/admin/faqs">{lng.faqsTxt}</Link></li>
            <li className="breadcrumb-item active" aria-current="page">{lng.updateTransTxt}</li>
          </ol>
        </nav>
      </div>


          <div className="col-md-8">
            <div className="card">
              <div className="card-header card-header-primary">
                <h4 className="card-title">{lng.updateTransTxt}</h4>
                <p className="card-faq"></p>
              </div>
              <div className="card-body">
                <form>
                 
                  
                  <div className="row">
                    <div className="col-md-12">
                      <div className="form-group">
                        <label></label>
                        <div className="form-group">
                          <label className="bmd-label-floating">{lng.descriptionTxt} 1</label>
                          <input 
                          className="form-control"
                          type="text"
                          name="description1"
                          defaultValue={description1}
                          onChange={this.onChangeValue} />

                        </div>
                      </div>
                    </div>
                  </div>


                  <div className="row">
                    <div className="col-md-12">
                      <div className="form-group">
                        <label></label>
                        <div className="form-group">
                          <label className="bmd-label-floating">{lng.descriptionTxt} 2</label>
                          <CKEditor
                          data={description2}
                                onChange={evt => {
                                  this.setState({description2: evt.editor.getData()})

                                }}
                                onKey={evt => {
                                  this.setState({description2: evt.editor.getData()})

                                }}                               
        
                            />
                        </div>
                      </div>
                    </div>
                  </div>


                  <button 
                  type="submit" 
                  onClick={this.handleSubmit}
                  className="btn btn-primary pull-right">{lng.saveTxt}
                  </button>
                  <div className="clearfix"></div>
                </form>
              </div>
            </div>
          </div>
          


        
        </div>
      
        )
    }
}


const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        defaultSets: state.DfltSts,
        userInfo: state.User.info
    }
}
export default connect(mapStateToProps, null)(EditTrans);
