
import React, {Component, Fragment} from 'react'
//import {HashRouter as Router, Route} from "react-router-dom";
import {ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.min.css';
import Sidebar from './partials/Sidebar';
import { connect } from 'react-redux';

import Nav from './partials/Nav';
import Footer from './partials/Footer';
import '../../../../public/assets/frontend/css/afonts.css'

import {appendScript, appendCss} from '../../helpers/appendScript';
import ScrollToTop from '../../components/ScrollToTop';

import {ar} from '../../lang/ar';
import {en} from '../../lang/en';

class Layout extends Component {
constructor(props) {
        super(props);
        this.state = {
            loading: true,
        }
    }
    
    componentDidMount() {
        
        this.loadCss()
        this.loadJs();

        //adjust directions of html container document
        document.getElementsByTagName("html")[0].setAttribute("dir", this.props.selectedLanguage.langDir);
        document.getElementsByTagName("body")[0].setAttribute("class", this.props.selectedLanguage.langDir);
        this.props.selectedLanguage.langCode == 'AR'? this.props.selectLanguage(ar) : this.props.selectLanguage(en);

        if (this.state.loading) {
            setTimeout(() => { 
              this.setState(() => ({loading: false}))
            }, 1000);
          }
    }

     loadJs() {
        
        appendScript("/assets/admin/js/core/jquery.min.js");    
        appendScript("/assets/admin/js/plugins/perfect-scrollbar.jquery.min.js");
        appendScript("/assets/admin/js/material-dashboard.js?v=2.1.2");
        appendScript("/assets/admin/js/core/bootstrap-material-design.min.js");    
        appendScript("/assets/admin/js/scripts.js");
        
    }

     loadCss() {   
    
        appendCss("/assets/admin/css/material-dashboard.css");

            /* appendCss("/assets/frontend/css/bootstrap.min.css");
            appendCss("/assets/frontend/css/bootstrap.rtl.css");
            appendCss("/assets/admin/css/custom.css").then(res => {
                this.setState({loading: false})
            }).catch(err => {
                console.log(err)
            });  */       
        
    }

    componentWillUnmount(){

        // remove css links.
        var element = document.getElementsByTagName("link"), index;

        for (index = element.length - 1; index >= 0; index--) {
            element[index].parentNode.removeChild(element[index]);
        }

        // remove js scripts.
        var element = document.getElementsByTagName("script"), index;

        for (index = element.length - 1; index >= 0; index--) {
            element[index].parentNode.removeChild(element[index]);
        }
    }

    componentDidUpdate(){
        //this.loadJs();
    }
    render() {

        if(this.state.loading){
            return (<div className="loader-wrapper"><div className="loader"></div></div>);

        }

        return (

                <Fragment>
                    <ScrollToTop />

                    <ToastContainer
                        rtl={true}
                        autoClose={5000}
                        position="top-center"
                        pauseOnHover={true}
                        hideProgressBar={true}
                    />
                    <div className="wrapper">                        
                        <Sidebar 
                        userInfo={this.props.userInfo}
                        selectedLanguage={this.props.selectedLanguage} />
                        


                        <div className="main-panel">                         
                            <Nav 
                            selectedLanguage={this.props.selectedLanguage}                             userInfo={this.props.userInfo}
                            userInfo={this.props.userInfo}
                            logout={this.props.logout} 
                            />
                        

                            <div className="content">
                                <div className="container-fluid">
                                {this.props.children}
                                </div>      
                            </div>
                            

                            <Footer selectedLanguage={this.props.selectedLanguage}
                            settings={this.props.defaultSets} />
                            
                        </div>

                    </div>  
                
                
                </Fragment>
        )
    }
}



const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        defaultSets: state.DfltSts,
        userInfo: state.User.info,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        logout: () => dispatch({ type: 'LOGOUT', payload: null }),
        //selectLanguage: (val) => dispatch({ type: 'language_data', payload: val }),

        selectLanguage: (val) => 
        {

            document.getElementsByTagName("html")[0].setAttribute("dir", val.langDir);
            document.getElementsByTagName("body")[0].setAttribute("class", val.langDir);
            dispatch({ type: 'language_data', payload: val })
        }
    }
}; 
export default connect(mapStateToProps, mapDispatchToProps)(Layout);