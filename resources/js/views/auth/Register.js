import React, {Component, Fragment} from 'react';

import { connect } from 'react-redux';
import {toast} from 'react-toastify';
import BreadCrumb from '../frontend/partials/BreadCrumb';
import postData from '../../helpers/postData';

class Register extends Component {
    constructor(props){
        super(props);
        this.state = {
            firstName: '',
            lastName: '',
            phone: '',
            password: '',
            email: '', 
            status: 0,
            errorMessage: '',
            isDisabled: false
        };

        this.onChangeValue = this.onChangeValue.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    onChangeValue(e) {
        this.setState({
            [e.target.name]: e.target.value, isDisabled: false
        });
    }




    handleSubmit(e){

        e.preventDefault();
        this.setState({isDisabled: true});
        const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

        postData(this.props.defaultSets.apiUrl+'register', {

            firstName: this.state.firstName,
            lastName: this.state.lastName,
            phone: this.state.phone,
            email: this.state.email,
            password: this.state.password,
            status: this.state.status,
            groupId: 2,
            langCode: this.props.selectedLanguage.langCode
        }) 
            .then( (response) => {

                if(response.data.success == true) {

                    let theUser = response.data.user;
                    theUser.accessToken = response.data.accessToken;
                    this.props.saveUserInfo(response.data.user);

                    toast.update(ldToast, {
                        type: toast.TYPE.SUCCESS,
                        render: response.data.msg
                    });

                    this.props.history.push({
                        pathname: '/activation',
                        //state: { id: response.data.user.id }
                      })
                    //this.props.history.push('/activation', {id: response.data});
                /* setTimeout( () => {
                    this.props.history.push('/');
                }, 3000); */

                } else {
                    toast.update(ldToast, {
                        type: toast.TYPE.ERROR,
                        render: response.data.msg
                    });
                }
            })
            .catch( (error) => {
                //console.log(error.response);
                this.setState({  isDisabled: false})

                if(error.response !== undefined && error.response.status === 422){
                    let errorTxt = '';
                    let errorMessage = error.response.data.errors;

                    Object.keys(errorMessage).map((key, i) => {
                        errorTxt = (<span> {errorTxt} {errorMessage[key][0]} <br /></span>);});
                    toast.update(ldToast, {
                        type: toast.TYPE.ERROR,
                        render: errorTxt
                    });
                }
            });

    }




    render() {


        const {isDisabled} = this.state;
        const { phoneTxt, passwordTxt, firstNameTxt, lastNameTxt, crtNewAccntTxt,
            phoneExTxt, emailTxt, registerTxt, alreadyHaveAcct} = this.props.selectedLanguage;
        return (

            <Fragment>
                 <BreadCrumb title={registerTxt} />




                 <section className="register-page section-b-space">
        <div className="container">
            <div className="row">
                <div className="col-lg-12">
                    <h3>{crtNewAccntTxt}</h3>
                    <div className="theme-card">
                        <form className="theme-form" onSubmit={this.handleSubmit}>                        

                            <div className="form-row"> 
                                <div className="col-md-6">
                                    <label for="email">{firstNameTxt}</label>
                                            
                                    <input type="text" 
                                    className="form-control"
                                    name="firstName"
                                    placeholder={firstNameTxt}
                                    onChange={this.onChangeValue} />
                                </div>
                                <div className="col-md-6">
                                    <label for="review">{lastNameTxt}</label>
                                    
                                    <input 
                                    type="text" 
                                    className="form-control"
                                    name="lastName"
                                    placeholder={lastNameTxt}
                                    onChange={this.onChangeValue} />
                                </div>
                            </div>

                            <div className="form-row">
                                <div className="col-md-6">
                                    <label for="email">{emailTxt}</label>
                                    <input 
                                placeholder={emailTxt}
                                type="text" 
                                onChange={this.onChangeValue}
                                className="form-control" 
                                name="email" />
                                </div>
                                <div className="col-md-6">
                                    <label for="review">{phoneTxt}</label>
                                    <input 
                                placeholder={phoneTxt}
                                type="text" 
                                onChange={this.onChangeValue}
                                className="form-control" 
                                name="phone" />
                                    
                                <span className="text-danger">
                                   ex: 90555********
                                </span>
                                </div>
                       
                          
                            </div>

                            <div className="form-row">
                                <div className="col-md-6">
                                    <label for="email">{passwordTxt}</label>
                                            
                                    <input 
                                    type="password" 
                                    className="form-control"
                                    name="password"
                                    placeholder={passwordTxt}
                                    onChange={this.onChangeValue} />
                                </div>
                                <div className="col-md-6"></div>

                                
                                <button 
                        type="submit" 
                        disabled={isDisabled} className="btn btn-solid">
                            {crtNewAccntTxt}</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
   

         


            </Fragment>


        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        saveUserInfo: (userInfo) => dispatch({ type: 'SAVE_USER', payload: userInfo }),

    }
};
 
const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        defaultSets: state.DfltSts,
        userInfo: state.User.info,
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Register);
