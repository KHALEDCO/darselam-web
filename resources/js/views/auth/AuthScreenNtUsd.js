import React, {Component} from 'react';
import Layout from "../frontend/Layout";
import BreadCrumb from "../frontend/partials/BreadCrumb";
import LeftNav from "../frontend/partials/LeftNav";
import { connect } from 'react-redux';
import AuthBox from "../../components/AuthBox";



class AuthScreen extends Component {
    constructor(props){
        super(props);
    }



    render() {


        return (
               <Layout>

                   <BreadCrumb />

                   <div className="banner">

                   <LeftNav/>

                       <div className="w3l_banner_nav_right">
                           <AuthBox />
                       </div>
                           <div className="clearfix"></div>
                       </div>
               </Layout>

        )
    }
}

const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        //defaultSets: state.DfltSts
    }
};

export default connect(mapStateToProps, null)(AuthScreen);
