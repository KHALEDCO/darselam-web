import React, {Component, Fragment} from 'react';

import { connect } from 'react-redux';
import {toast} from 'react-toastify';
import BreadCrumb from '../frontend/partials/BreadCrumb';
import postData from '../../helpers/postData';

class Register extends Component {
    constructor(props){
        super(props);
        this.state = {
            firstName: '',
            lastName: '',
            phone: '',
            password: '',
            email: '', 
            status: 0,
            errorMessage: '',
            isDisabled: false
        };

        this.onChangeValue = this.onChangeValue.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    onChangeValue(e) {
        this.setState({
            [e.target.name]: e.target.value, isDisabled: false
        });
    }




    handleSubmit(e){

        e.preventDefault();
        this.setState({isDisabled: true});
        const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

        postData(this.props.defaultSets.apiUrl+'register', {

            firstName: this.state.firstName,
            lastName: this.state.lastName,
            phone: this.state.phone,
            email: this.state.email,
            password: this.state.password,
            status: this.state.status,
            groupId: 2,
            langCode: this.props.selectedLanguage.langCode
        }) 
            .then( (response) => {

                if(response.data.success == true) {
                    this.props.saveUserInfo(response.data.user);

                    toast.update(ldToast, {
                        type: toast.TYPE.SUCCESS,
                        render: response.data.msg
                    });

                    this.props.history.push({
                        pathname: '/activation',
                        //state: { id: response.data.user.id }
                      })
                    //this.props.history.push('/activation', {id: response.data});
                /* setTimeout( () => {
                    this.props.history.push('/');
                }, 3000); */

                } else {
                    toast.update(ldToast, {
                        type: toast.TYPE.ERROR,
                        render: response.data.msg
                    });
                }
            })
            .catch( (error) => {
                //console.log(error.response);
                this.setState({  isDisabled: false})

                if(error.response !== undefined && error.response.status === 422){
                    let errorTxt = '';
                    let errorMessage = error.response.data.errors;

                    Object.keys(errorMessage).map((key, i) => {
                        errorTxt = (<span> {errorTxt} {errorMessage[key][0]} <br /></span>);});
                    toast.update(ldToast, {
                        type: toast.TYPE.ERROR,
                        render: errorTxt
                    });
                }
            });

    }




    render() {


        const {isDisabled} = this.state;
        const { phoneTxt, passwordTxt, firstNameTxt, lastNameTxt, crtNewAccntTxt,
            phoneExTxt, emailTxt, registerTxt, alreadyHaveAcct} = this.props.selectedLanguage;
        return (

            <Fragment>
                 <BreadCrumb title={registerTxt} />

                 <div className="contact-form spad">
        <div className="container">
           
            <form  onSubmit={this.handleSubmit}>

                
            <div className="row">
                    <div className="col-lg-6 col-md-6">
                                
                    <input type="text" 
                                    className="form-control"
                                    name="firstName"
                                    placeholder={firstNameTxt}
                                    onChange={this.onChangeValue} />
                    </div>
                    <div className="col-lg-6 col-md-6">
                                
                    <input 
                                    type="text" 
                                    className="form-control"
                                    name="lastName"
                                    placeholder={lastNameTxt}
                                    onChange={this.onChangeValue} />
                    </div>
                </div>
            

                <div className="row">
                    <div className="col-lg-12 col-md-12">
                                <input 
                                placeholder={emailTxt}
                                type="text" 
                                onChange={this.onChangeValue}
                                className="form-control" 
                                name="email" />                                
                                
                    </div>
                </div>
            
                <div className="row">
                    <div className="col-lg-6 col-md-6">
                                <input 
                                placeholder={phoneTxt}
                                type="text" 
                                onChange={this.onChangeValue}
                                className="form-control" 
                                name="phone" />
                                    
                                <span className="text-danger">
                                   ex: 96655********
                                </span>
                                
                                
                    </div>
                    <div className="col-lg-6 col-md-6">
                                <input 
                                placeholder={passwordTxt}
                                type="password" 
                                onChange={this.onChangeValue}
                                className="form-control" 
                                name="password"
                                /*id="review"*/
                                 />
                    </div>
                    
                    
                    <div className="col-lg-12 text-center">
                        <button type="submit" disabled={isDisabled} className="site-btn">{registerTxt}</button>
                    </div>
                </div>
            
            
            

            </form>
        </div>
    </div>
   


            </Fragment>


        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        saveUserInfo: (userInfo) => dispatch({ type: 'SAVE_USER', payload: userInfo }),

    }
};
 
const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        defaultSets: state.DfltSts,
        userInfo: state.User.info,
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Register);
