import React, {Component, Fragment} from 'react';

import { connect } from 'react-redux';
import {toast} from 'react-toastify';
import BreadCrumb from '../frontend/partials/BreadCrumb';
import putData from '../../helpers/putData';
import getData from '../../helpers/getData';

class Profile extends Component {
    constructor(props){
        super(props);
        this.state = {
            firstName: '',
            lastName: '',
            phone: '',
            password: '',
            email: '',
            errorMessage: '',
            isDisabled: false
        };

        this.onChangeValue = this.onChangeValue.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

      
    componentDidMount(){
        this.fetchData(this.props.userInfo.id);   
    }
  
   fetchData(elmntId) {
     getData(this.props.defaultSets.apiUrl+'users/'+elmntId+'/edit', this.props.userInfo.accessToken)
         //.then(response => response.json())
         .then(response => {
  
           this.setState({ 
               id: elmntId,
               firstName: response.data.firstName,
               lastName: response.data.lastName,
               phone: response.data.phone,
               email: response.data.email,
               
             });
             //console.log(this.state.phone)
         })
         .catch(function (error) {
             console.log(error);
         });
   }
  
    onChangeValue(e) {
        this.setState({
            [e.target.name]: e.target.value, isDisabled: false
        });
    }

    
    handleSubmit(e){
        //console.log('k'+this.state.status)
                e.preventDefault();
                this.setState({isDisabled: true});
                const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);
        
                putData(this.props.defaultSets.apiUrl+'users/'+this.state.id, {
        
                    id: this.state.id,                    
                    firstName: this.state.firstName,
                    lastName: this.state.lastName,
                    phone: this.state.phone,
                    email: this.state.email,
                    password: this.state.password,                    
                    langCode: this.props.selectedLanguage.langCode
                }, this.props.userInfo.accessToken)
                    .then( (response) => {

                        if(response.data.success === true) {
                            this.props.saveUserInfo(response.data.user);
                        }
                        toast.update(ldToast, {
                            type: toast.TYPE.SUCCESS,
                            render: response.data.msg
                        });
        
                       /*  setTimeout( () => {
                            this.props.history.push('/colours');
                        }, 3000); */
                    })
                    .catch( (error) => {
                        //console.log(error.response);
                        this.setState({  isDisabled: false})
        
                        if(error.response !== undefined && error.response.status === 422){
                            let errorTxt = '';
                            let errorMessage = error.response.data.errors;
        
                            Object.keys(errorMessage).map((key, i) => {
                                errorTxt = (<span> {errorTxt} {errorMessage[key][0]} <br /></span>);});
                            toast.update(ldToast, {
                                type: toast.TYPE.ERROR,
                                render: errorTxt
                            });
                        }
                    });
        
            }
        

    render() {


        const {isDisabled, firstName, lastName, phone, email} = this.state;
        const { phoneTxt, passwordTxt, firstNameTxt, lastNameTxt, updateTxt,
            previousMember, emailTxt, profileTxt
        } = this.props.selectedLanguage;
        return (

            <Fragment>
                 <BreadCrumb title={profileTxt} />

                 <section className="register-page section-b-space">
        <div className="container">
            <div className="row">
                <div className="col-lg-12">
                    <h3>{profileTxt}</h3>
                    <div className="theme-card">
                        <form className="theme-form" onSubmit={this.handleSubmit}>                        

                            <div className="form-row">
                                <div className="col-md-6">
                                    <label for="email">{firstNameTxt}</label>
                                            
                                    <input type="text" 
                                    className="form-control"
                                    name="firstName"
                                    value={firstName}
                                    placeholder={firstNameTxt}
                                    onChange={this.onChangeValue} />
                                </div>
                                <div className="col-md-6">
                                    <label for="review">{lastNameTxt}</label>
                                    
                                    <input 
                                    type="text" 
                                    className="form-control"
                                    name="lastName"
                                    value={lastName}
                                    placeholder={lastNameTxt}
                                    onChange={this.onChangeValue} />
                                </div>
                            </div>

                            <div className="form-row">
                                <div className="col-md-6">
                                    <label for="email">{emailTxt}</label>
                                    <input 
                                placeholder={emailTxt}
                                type="text" 
                                value={email}
                                onChange={this.onChangeValue}
                                className="form-control" 
                                name="email" />
                                </div>
                                <div className="col-md-6">
                                    <label for="review">{phoneTxt}</label>
                                    <input 
                                placeholder={phoneTxt}
                                value={phone}
                                type="text" 
                                onChange={this.onChangeValue}
                                className="form-control" 
                                name="phone" />
                                    
                                <span className="text-danger">
                                   ex: 90555********
                                </span>
                                </div>
                       
                          
                            </div>

                            <div className="form-row">
                                <div className="col-md-6">
                                    <label for="email">{passwordTxt}</label>
                                            
                                    <input 
                                    type="password" 
                                    className="form-control"
                                    name="password"
                                    placeholder={passwordTxt}
                                    onChange={this.onChangeValue} />
                                </div>
                                <div className="col-md-6"></div>

                                
                                <button 
                        type="submit" 
                        disabled={isDisabled} className="btn btn-solid">
                            {updateTxt}</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
   



            </Fragment>


        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        saveUserInfo: (userInfo) => dispatch({ type: 'SAVE_USER', payload: userInfo })
    }
};

const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        defaultSets: state.DfltSts,
        userInfo: state.User.info
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Profile);
