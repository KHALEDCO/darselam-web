import React, {Component, Fragment} from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import {Helmet} from "react-helmet";

 
// Demo styles, see 'Styles' section below for some notes on use.
import 'react-accessible-accordion/dist/fancy-example.css';
import getData from '../../../helpers/getData';

class Agents extends Component {

    constructor(props) {
        super(props);
        this.state = {
            rows: [],
        };
        this.returnRegion = this.returnRegion.bind(this);
    }

    componentDidMount(){
        this.fetchAgents();        
    }

    fetchAgents() {
        
        getData(this.props.defaultSets.apiUrl+'users/group/3/'+this.props.match.params.regionId, this.props.userInfo.accessToken)
            .then(response => {  
              this.setState({rows: response.data.rows});
                console.log(response.data)
            })
            .catch(function (error) {
                console.log(error);
            });
      
    }

    returnRegion(regionId) {
        console.log(regionId)
        switch(parseInt(regionId)) {
            case 1: return this.props.selectedLanguage.americaTxt;
            case 2: return this.props.selectedLanguage.russiaTxt;
            case 3: return this.props.selectedLanguage.middleEastTxt;
            case 4: return this.props.selectedLanguage.latinAmercaTxt;
            case 5: return this.props.selectedLanguage.europeTxt;
            case 6: return this.props.selectedLanguage.asiaPacificTxt;
            case 7: return this.props.selectedLanguage.australiaTxt;
            default: return '';
        }
    }

    render() {
 
        const {rows} = this.state;
        const lng = this.props.selectedLanguage;
        return (
    
            <Fragment>
<Helmet><title>{lng.agentsTxt}</title></Helmet>

<div className="breadcrumb-section">
    <div className="container">
        <div className="row">
            <div className="col-sm-6">
                <div className="page-title">
                    <h2>{this.returnRegion(this.props.match.params.regionId)}</h2>
                </div>
            </div>
            <div className="col-sm-6">
                <nav aria-label="breadcrumb" className="theme-breadcrumb">
                    <ol className="breadcrumb">
                        <li className="breadcrumb-item"><Link to="/"><i className="fa fa-home"></i></Link></li>
                        <li className="breadcrumb-item"><Link to="/agents">{lng.agentsTxt}</Link></li>
                        <li className="breadcrumb-item active" aria-current="page">{this.returnRegion(this.props.match.params.regionId)}</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>

 <div className="distributor pb-5">



<section className="banner-goggles ratio2_1">
  <div className="container">
    <div className="row partition3">
      
    {rows && rows.map((item, i) => {
        return(
<div className="col-md-4 pb-4" key={i}>
        <a href="#" onClick={(e) => e.preventDefault()}>
          <div className="collection-banner p-right text-center">
            <div className="img-part">
              <img
                src={"/uploads/files/"+item.photo}
                className="img-fluid blur-up lazyload bg-img"
                alt=""
              />
            </div>
            <div className="contain-banner banner-3">
              <div>
                {/* <h3>MWF USA LLC</h3>
                <hr /> */}
                <h4>{item.full_name}</h4>
                <p>
                  <b>{lng.phoneTxt} :</b>
                  <a href="tel:+1 708 364 9602">{item.phone}</a>
                </p>
                <p>
                  <b>{lng.emailTxt} :</b>
                  <a href="mailto:info@mwf-usa.com">{item.email}</a>
                </p>
                <p>
                  <b>{lng.websiteTxt} :</b> <a href="mwf-usa.com">{item.website}</a>
                </p>
                <p>
                  <b>{lng.adrsTxt} :</b>
                  <a href={item.mapUrl}>
                  {item.adrs}
                  </a>
                </p>
              </div>
            </div>
          </div>
        </a>
      </div>

        )
    })}
      
    </div>
  </div>
</section>

</div>

 </Fragment>

    )
    }
}





const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        //cartItems: state.cartItems.cart,
        userInfo: state.User.info,
        //selectedCurr: state.Curr.slctdCurr,
        defaultSets: state.DfltSts
    }
}
export default connect(mapStateToProps, null)(Agents);