import React, {Component, Fragment} from 'react';
import { connect } from 'react-redux';
import BreadCrumb from '../partials/BreadCrumb';
import {Helmet} from "react-helmet";
import { Link } from 'react-router-dom';
 
// Demo styles, see 'Styles' section below for some notes on use.
//import 'react-accessible-accordion/dist/fancy-example.css';
//import getData from '../../../helpers/getData';

class Index extends Component {

    constructor(props) {
        super(props);
        this.state = {
           // rows: [],
        };
        
    }

    componentDidMount(){
        //this.fetchAgents();        
    }

 

    render() {
 
        const lng = this.props.selectedLanguage;
        return (
    
            <Fragment>
<Helmet><title>{lng.agentsTxt}</title></Helmet>

 <BreadCrumb title={lng.agentsTxt} />
 
 
 <div className="distributors">
          <section className="banner-goggles ratio2_1 p-5">
          <div className="container">
            <div className="row partition3">
              <div className="col-md-4 py-5 dis-box">
                <Link to="agents/1">
                  <div className="collection-banner p-right text-center">
                    <div className="img-part">
                      <img
                        src="/assets/frontend/images/distributors/USA.png"
                        className="img-fluid blur-up lazyload bg-img"
                        alt=""
                      />
                    </div>
                    <h3 className="pt-5">{lng.americaTxt}</h3>

                  </div>
                </Link>
              </div>
              <div className="col-md-4 py-5 dis-box">
                <Link to="agents/2">
                  <div className="collection-banner p-right text-center">
                    <div className="img-part">
                      <img
                        src="/assets/frontend/images/distributors/russia.png"
                        className="img-fluid blur-up lazyload bg-img"
                        alt=""
                      />
                    </div>
                    
                    <h3 className="pt-5">{lng.russiaTxt}</h3>
                  </div>
                </Link>
              </div>
              <div className="col-md-4 py-5 dis-box">
                <Link to="agents/3">
                  <div className="collection-banner p-right text-center">
                    <div className="img-part">
                      <img
                        src="/assets/frontend/images/distributors/middle-east.png"
                        className="img-fluid blur-up lazyload bg-img"
                        alt=""
                      />
                    </div>
                    <h3 className="pt-5">{lng.middleEastTxt}</h3>
                  </div>
                </Link>
              </div>
              <div className="col-md-4 py-5 dis-box">
                <Link to="agents/4">
                  <div className="collection-banner p-right text-center">
                    <div className="img-part">
                      <img
                        src="/assets/frontend/images/distributors/latin.png"
                        className="img-fluid blur-up lazyload bg-img"
                        alt=""
                      />
                    </div>
                    <h3 className="pt-5">{lng.latinAmercaTxt}</h3>
                  </div>
                </Link>
              </div>
              <div className="col-md-4 py-5 dis-box">
                <Link to="agents/5">
                  <div className="collection-banner p-right text-center">
                    <div className="img-part">
                      <img
                        src="/assets/frontend/images/distributors/europe.png"
                        className="img-fluid blur-up lazyload bg-img"
                        alt=""
                      />
                    </div>
                    <h3 className="pt-5">{lng.europeTxt}</h3>

                  </div>
                </Link>
              </div>
              <div className="col-md-4 py-5 dis-box">
                <Link to="agents/6">
                  <div className="collection-banner p-right text-center">
                    <div className="img-part">
                      <img
                        src="/assets/frontend/images/distributors/asia.png"
                        className="img-fluid blur-up lazyload bg-img"
                        alt=""
                      />
                    </div>
                    <h3 className="pt-5">{lng.asiaPacificTxt}</h3>
                  </div>
                </Link>
              </div>
              <div className="col-md-4 py-5 dis-box">
                <Link to="agents/7">
                  <div className="collection-banner p-right text-center">
                    <div className="img-part">
                      <img
                        src="/assets/frontend/images/distributors/ANZ.png"
                        className="img-fluid blur-up lazyload bg-img"
                        alt=""
                      />
                    </div>
                    <h3 className="pt-5">{lng.australiaTxt}</h3>
                  </div>
                </Link>
              </div>
            </div>
          </div>
        </section>
        </div>

        <section className="home-distributor dis py-3">
          <div className="container">
            <div className="row">
              <div className="col-lg-9 col-md-12">
                <div className="">
                  <h4>
                    Do you want to be our distributor in your ( Area / Country )
                    … ?
                  </h4>
                </div>
              </div>
              <div className="col-lg-3 col-md-12">
                <div className="">
                  <a href="#" className="btn btn-solid">
                    Click Here
                  </a>
                </div>
              </div>
            </div>
          </div>
        </section>

 </Fragment>

    )
    }
}





const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        //cartItems: state.cartItems.cart,
        //userInfo: state.User.info
        //selectedCurr: state.Curr.slctdCurr,
        defaultSets: state.DfltSts
    }
}
export default connect(mapStateToProps, null)(Index);