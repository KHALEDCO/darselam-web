import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import BreadCrumb from "../partials/BreadCrumb";
import { Helmet } from "react-helmet";
import { Link } from "react-router-dom";

// Demo styles, see 'Styles' section below for some notes on use.
//import 'react-accessible-accordion/dist/fancy-example.css';
//import getData from '../../../helpers/getData';

class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            // rows: [],
        };
    }

    componentDidMount() {
        //this.fetchAgents();
    }

    render() {
        const lng = this.props.selectedLanguage;
        return (
            <Fragment>
                <Helmet>
                    <title>{lng.agentsTxt}</title>
                </Helmet>

                <BreadCrumb title={lng.agentsTxt} />

                <div className="distributors">
                    <section className="banner-goggles ratio2_1 p-5">
                        <div className="container">
                            <div className="row partition3">
                                <div
                                    className="col-md-4 py-5 dis-box"
                                    data-toggle="modal"
                                    data-target="#exampleModal"
                                >
                                    <Link to="#">
                                        <div className="collection-banner p-right text-center">
                                            <div className="img-part">
                                                <img
                                                    src="/assets/frontend/images/distributors/USA.png"
                                                    className="img-fluid blur-up lazyload bg-img"
                                                    alt=""
                                                />
                                            </div>
                                            <h3 className="pt-5">
                                                {lng.americaTxt}
                                            </h3>
                                        </div>
                                    </Link>
                                </div>

                                <div
                                    className="modal fade"
                                    id="exampleModal"
                                    tabindex="-1"
                                    aria-labelledby="exampleModalLabel"
                                    aria-hidden="true"
                                >
                                    <div className="modal-dialog">
                                        <div className="modal-content">
                                            <div className="modal-header">
                                                <h5
                                                    className="modal-title"
                                                    id="exampleModalLabel"
                                                >
                                                    Modal title
                                                </h5>
                                                <button
                                                    type="button"
                                                    className="close"
                                                    data-dismiss="modal"
                                                    aria-label="Close"
                                                >
                                                    <span aria-hidden="true">
                                                        &times;
                                                    </span>
                                                </button>
                                            </div>
                                            <div className="modal-body">
                                                <div className="container-fluid">
                                                    <div className="row">
                                                        <div className="col-md-6 py-3">
                                                            <a href="#">
                                                                <div className="collection-banner p-right text-center">
                                                                    <div className="img-part">
                                                                        <img
                                                                            src="/assets/frontend/images/distributors/americaa-2.jpg"
                                                                            className="img-fluid blur-up lazyload bg-img"
                                                                            alt=""
                                                                        />
                                                                    </div>
                                                                    <div className="contain-banner banner-3">
                                                                        <div>
                                                                            <h3>
                                                                                MWF
                                                                                USA
                                                                                LLC
                                                                            </h3>
                                                                            <hr />
                                                                            <h4>
                                                                                UNITED
                                                                                STATES
                                                                                OF
                                                                                AMERICA
                                                                                -
                                                                                ILLINOIS
                                                                            </h4>
                                                                            <p>
                                                                                <b>
                                                                                    phone
                                                                                    :
                                                                                </b>
                                                                                <a
                                                                                    href="tel:+1 708 364 9602"
                                                                                    target="_blank"
                                                                                >
                                                                                    +1
                                                                                    708
                                                                                    364
                                                                                    9602
                                                                                </a>
                                                                            </p>
                                                                            <p>
                                                                                <b>
                                                                                    Email
                                                                                    :
                                                                                </b>
                                                                                <a
                                                                                    href="mailto:info@mwf-usa.com"
                                                                                    target="_blank"
                                                                                >
                                                                                    info@mwf-usa.com
                                                                                </a>
                                                                            </p>
                                                                            <p>
                                                                                <b>
                                                                                    website
                                                                                    :
                                                                                </b>
                                                                                <a
                                                                                    href="http://mwf-usa.com"
                                                                                    target="_blank"
                                                                                >
                                                                                    mwf-usa.com
                                                                                </a>
                                                                            </p>
                                                                            <p>
                                                                                <b>
                                                                                    Address
                                                                                    :
                                                                                </b>
                                                                                <a
                                                                                    href="https://www.google.com.tr/maps/place/6730+107th+St,+Chicago+Ridge,+IL+60415,+Amerika+Birle%C5%9Fik+Devletleri/@41.6977677,-87.7892435,17z/data=!3m1!4b1!4m5!3m4!1s0x880e397797536073:0x776a96d1007e26c7!8m2!3d41.6977637!4d-87.7870548?hl=tr"
                                                                                    target="_blank"
                                                                                >
                                                                                    6730
                                                                                    107th
                                                                                    St,
                                                                                    Chicago
                                                                                    Ridge,
                                                                                    IL-
                                                                                    USA
                                                                                </a>
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </a>
                                                        </div>
                                                        <div className="col-md-6 py-3">
                                                            <a href="#">
                                                                <div className="collection-banner p-right text-center">
                                                                    <div className="img-part">
                                                                        <img
                                                                            src="/assets/frontend/images/distributors/americaa-2.jpg"
                                                                            className="img-fluid blur-up lazyload bg-img"
                                                                            alt=""
                                                                        />
                                                                    </div>
                                                                    <div className="contain-banner banner-3">
                                                                        <div>
                                                                            <h3>
                                                                                DETECTOR
                                                                                POWER
                                                                                CO.
                                                                            </h3>
                                                                            <hr />
                                                                            <h4>
                                                                                UNITED
                                                                                STATES
                                                                                OF
                                                                                AMERICA
                                                                                -
                                                                                FLORIDA
                                                                            </h4>
                                                                            <p>
                                                                                <b>
                                                                                    phone
                                                                                    :
                                                                                </b>
                                                                                <a
                                                                                    href="tel:+1 877 220 7510"
                                                                                    target="_blank"
                                                                                >
                                                                                    +1
                                                                                    877
                                                                                    220
                                                                                    7510
                                                                                </a>{" "}
                                                                                -{" "}
                                                                                <a
                                                                                    href="tel:+1 786 352 7210"
                                                                                    target="_blank"
                                                                                >
                                                                                    +1
                                                                                    786
                                                                                    352
                                                                                    7210
                                                                                </a>
                                                                            </p>
                                                                            <p>
                                                                                <b>
                                                                                    Email
                                                                                    :
                                                                                </b>
                                                                                <a
                                                                                    href="mailto:detectorpower@gmail.com"
                                                                                    target="_blank"
                                                                                >
                                                                                    detectorpower@gmail.com
                                                                                </a>
                                                                            </p>
                                                                            <p>
                                                                                <b>
                                                                                    website
                                                                                    :
                                                                                </b>
                                                                                <a
                                                                                    href="https://www.mwf-florida.com"
                                                                                    target="_blank"
                                                                                >
                                                                                    mwf-florida.com
                                                                                </a>
                                                                            </p>
                                                                            <p>
                                                                                <b>
                                                                                    Address
                                                                                    :
                                                                                </b>
                                                                                <a
                                                                                    href="https://www.google.com.tr/maps/place/21950+Soundview+Terrace+APT+109,+Boca+Raton,+FL+33433,+USA/@26.3519945,-80.1483544,17z/data=!4m5!3m4!1s0x88d91c2da262ce9f:0xb67bc641343c25fb!8m2!3d26.3519945!4d-80.1461657"
                                                                                    target="_blank"
                                                                                >
                                                                                    21950
                                                                                    Soundview
                                                                                    Terrace
                                                                                    Apt
                                                                                    109,
                                                                                    Boca
                                                                                    Raton,
                                                                                    FL,
                                                                                    33433.
                                                                                    Florida
                                                                                </a>
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="col-md-4 py-5 dis-box">
                                    <Link to="agents/2">
                                        <div className="collection-banner p-right text-center">
                                            <div className="img-part">
                                                <img
                                                    src="/assets/frontend/images/distributors/russia.png"
                                                    className="img-fluid blur-up lazyload bg-img"
                                                    alt=""
                                                />
                                            </div>

                                            <h3 className="pt-5">
                                                {lng.russiaTxt}
                                            </h3>
                                        </div>
                                    </Link>
                                </div>
                                <div className="col-md-4 py-5 dis-box">
                                    <Link to="agents/3">
                                        <div className="collection-banner p-right text-center">
                                            <div className="img-part">
                                                <img
                                                    src="/assets/frontend/images/distributors/middle-east.png"
                                                    className="img-fluid blur-up lazyload bg-img"
                                                    alt=""
                                                />
                                            </div>
                                            <h3 className="pt-5">
                                                {lng.middleEastTxt}
                                            </h3>
                                        </div>
                                    </Link>
                                </div>
                                <div className="col-md-4 py-5 dis-box">
                                    <Link to="agents/4">
                                        <div className="collection-banner p-right text-center">
                                            <div className="img-part">
                                                <img
                                                    src="/assets/frontend/images/distributors/latin.png"
                                                    className="img-fluid blur-up lazyload bg-img"
                                                    alt=""
                                                />
                                            </div>
                                            <h3 className="pt-5">
                                                {lng.latinAmercaTxt}
                                            </h3>
                                        </div>
                                    </Link>
                                </div>
                                <div className="col-md-4 py-5 dis-box">
                                    <Link to="agents/5">
                                        <div className="collection-banner p-right text-center">
                                            <div className="img-part">
                                                <img
                                                    src="/assets/frontend/images/distributors/europe.png"
                                                    className="img-fluid blur-up lazyload bg-img"
                                                    alt=""
                                                />
                                            </div>
                                            <h3 className="pt-5">
                                                {lng.europeTxt}
                                            </h3>
                                        </div>
                                    </Link>
                                </div>
                                <div className="col-md-4 py-5 dis-box">
                                    <Link to="agents/6">
                                        <div className="collection-banner p-right text-center">
                                            <div className="img-part">
                                                <img
                                                    src="/assets/frontend/images/distributors/asia.png"
                                                    className="img-fluid blur-up lazyload bg-img"
                                                    alt=""
                                                />
                                            </div>
                                            <h3 className="pt-5">
                                                {lng.asiaPacificTxt}
                                            </h3>
                                        </div>
                                    </Link>
                                </div>
                                <div className="col-md-4 py-5 dis-box">
                                    <Link to="agents/7">
                                        <div className="collection-banner p-right text-center">
                                            <div className="img-part">
                                                <img
                                                    src="/assets/frontend/images/distributors/ANZ.png"
                                                    className="img-fluid blur-up lazyload bg-img"
                                                    alt=""
                                                />
                                            </div>
                                            <h3 className="pt-5">
                                                {lng.australiaTxt}
                                            </h3>
                                        </div>
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

                <section className="home-distributor dis py-3">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-9 col-md-12">
                                <div className="">
                                    <h4>
                                        Do you want to be our distributor in
                                        your ( Area / Country ) … ?
                                    </h4>
                                </div>
                            </div>
                            <div className="col-lg-3 col-md-12">
                                <div className="">
                                    <Link to="#" className="btn btn-solid">
                                        Click Here
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        //cartItems: state.cartItems.cart,
        //userInfo: state.User.info
        //selectedCurr: state.Curr.slctdCurr,
        defaultSets: state.DfltSts
    };
};
export default connect(mapStateToProps, null)(Index);
