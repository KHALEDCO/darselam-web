import React, {Component, Fragment} from 'react';
import { connect } from 'react-redux';
import BreadCrumb from '../partials/BreadCrumb';
import ProductBoxInCat from '../../../components/ProductBoxInCat';
import ItemsCarousel from 'react-items-carousel';
import {toast} from "react-toastify";
import ProductBoxOffer from '../../../components/ProductBoxOffer';
import {Helmet} from "react-helmet";
import getData from '../../../helpers/getData';
import postData from '../../../helpers/postData';




class Show extends Component {

    constructor(props) {
        super(props);
        this.state = {
            rows: [],
            catInfo: '',
            
            offers: [],
            activeItemIndex: 0
        };
        this.addToFav = this.addToFav.bind(this); 

    }

    componentDidMount(){
        //this.fetchProducts(this.props.match.params.id);
        this.fetchProducts();
        this.fetchOffers();

    }

    fetchProducts() {
        
        getData(this.props.defaultSets.apiUrl+'products/status/1')
            .then(response => {  
              this.setState({rows: response.data.data});
                console.log(response.data)
            })
            .catch(function (error) {
                console.log(error);
            });
      
    }
    
    
    fetchOffers() {
        
        axios.get(this.props.defaultSets.apiUrl+'/offer-products')
            .then(response => {  
              this.setState({offers: response.data});
                //console.log('offers:', response.data)
                
            })
            .catch(function (error) {
                console.log(error);
            });
      
    }

    addToFav(item) {
        
        if(this.props.userInfo.id == 0) {
            return toast.info(this.props.selectedLanguage.plsLgnFrst);
        }
        const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

        postData(this.props.defaultSets.apiUrl+'favourites', {
            userId: this.props.userInfo.id,
            itemId: item.id,
            langCode: this.props.selectedLanguage.langCode
        })
            .then( (response) => {

                if(response.data.success == true) {
                    toast.update(ldToast, {
                        type: toast.TYPE.SUCCESS,
                        render: response.data.msg
                    });
                    item.favId = response.data.favId;

                    return this.props.addToFavourites(item);
                }
                return toast.update(ldToast, {
                    type: toast.TYPE.ERROR,
                    render: response.data.msg
                });
            })
            .catch( (error) => {
               
            });

    }
    
    render() {
 
        const {rows, catInfo, offers, activeItemIndex} = this.state;
        const {langCode, productFoundTxt, specialOffersTxt, allProductsTxt
        } = this.props.selectedLanguage;
        return (
    
            <Fragment>
<Helmet><title>{allProductsTxt}</title></Helmet>
{/*  <BreadCrumb title={catInfo && eval('catInfo.title'+langCode)} />
 */}
 
  <BreadCrumb title={allProductsTxt} />


 <section className="product spad">
        <div className="container">
            <div className="row">
                
                <div className="col-lg-12 col-md-12">
                    <div className="product__discount">
                        <div className="section-title product__discount__title">
                            <h2>{specialOffersTxt}</h2>
                        </div>
                        <div className="">
                            

                        <div style={{"padding":"0 60px","maxWidth":'90%',"margin":"0 auto"}}>
  <ItemsCarousel
    infiniteLoop={false}
    gutter={12}
    activePosition={'center'}
    chevronWidth={60}
    disableSwipe={false}
    alwaysShowChevrons={false}
    numberOfCards={3}
    slidesToScroll={3}
    outsideChevron={true}
    showSlither={false}
    firstAndLastGutter={false}
    activeItemIndex={this.state.activeItemIndex}
    requestToChangeActive={value => this.setState({ activeItemIndex: value })}
    rightChevron={<button className="site-btn"><i className="fa fa-arrow-right"></i></button>}
    leftChevron={<button className="site-btn"><i className="fa fa-arrow-left"></i></button>}
  >
    {offers && offers.map((item, i) =>
      <ProductBoxOffer 
        key={i}
        obj={item} 
        selectedLanguage={this.props.selectedLanguage}                            
        key={'offer'+i}                                
        addToCart={this.props.addToCart} 
        addToFav={this.addToFav}                           
        favedItems={this.props.favedItems} 
        />
    )}
  </ItemsCarousel>
</div>





                        
                        </div>
                    </div>

                    <div className="filter__item">
                        <div className="row">
                            <div className="col-lg-4 col-md-5">
                                <div className="filter__sort">
                                    <span>Sort By</span>
                                    <select>
                                        <option value="0">Default</option>
                                        <option value="0">Default</option>
                                    </select>
                                </div>
                            </div>
                            <div className="col-lg-4 col-md-4">
                                <div className="filter__found">
                                    <h6><span>{rows && rows.length}</span>{productFoundTxt}</h6>
                                </div>
                            </div>
                            <div className="col-lg-4 col-md-3">
                                <div className="filter__option">
                                    <span className="icon_grid-2x2"></span>
                                    <span className="icon_ul"></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="row">
                    {rows && rows.map((item, i) => {
                        return(<ProductBoxInCat
                            obj={item} 
                            selectedLanguage={this.props.selectedLanguage}                            
                            key={'prdctBox'+i}                
                            addToCart={this.props.addToCart} 
                            addToFav={this.addToFav}                           
                            favedItems={this.props.favedItems} 
                            />);
                        })
                    }
                    </div>
                    <div className="product__pagination">
                        <a href="#">1</a>
                        <a href="#">2</a>
                        <a href="#">3</a>
                        <a href="#"><i className="fa fa-long-arrow-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    





 </Fragment>

    )
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        addToCart: (product, msg) => {
            toast.success(msg);
            dispatch({ type: 'ADD_TO_CART', payload: product })
        },
        addToFavourites: (item) => {
            dispatch({ type: 'ADD_TO_FAV', payload: item });
        }

    }
}


const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        //cartItems: state.cartItems.cart,
        favedItems: state.Favourites.rows,
        userInfo: state.User.info,
        slctdCurr: state.Currs.currencyInfo,
        defaultSets: state.DfltSts
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Show);