import React, {Component, Fragment} from 'react';
import { connect } from 'react-redux';
import BreadCrumb from "../partials/BreadCrumb";
import {toast} from "react-toastify";
import { Helmet } from 'react-helmet';
import postData from '../../../helpers/postData';
import ReactFlagsSelect from 'react-flags-select';

class Verify extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isDisabled: false,
            isRgstrDisabled: false,
            serialNum: '',
            guaranteeNum: '',

            firstName: '',
            lastName: '',
            phone: '',
            email: '', 
            countryCode: 'TR',
            adrs: ''
        };
        this.onChangeValue = this.onChangeValue.bind(this);
        this.verifyPrdct = this.verifyPrdct.bind(this);
        this.registerPrdct = this.registerPrdct.bind(this);
    }

    componentDidMount(){    

    }

    onChangeValue(e) {
        this.setState({
            [e.target.name]: e.target.value, isDisabled: false, isRgstrDisabled: false
        });
        //console.log(this.state.cityId)

    }
    

    verifyPrdct(e){

        e.preventDefault();
        this.setState({isDisabled: true});
        const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

        axios.post(this.props.defaultSets.apiUrl+'verify-product', {
            serialNum: this.state.serialNum,
            guaranteeNum: this.state.guaranteeNum,
            langCode: this.props.selectedLanguage.langCode
        })
            .then( (response) => {

                if(response.data.success == true){
                    return toast.update(ldToast, {
                        type: toast.TYPE.SUCCESS,
                        render: response.data.msg
                    });
                }
                return toast.update(ldToast, {
                    type: toast.TYPE.ERROR,
                    render: response.data.msg
                });
                
            })
            .catch( (error) => {
                //console.log(error.response);
                this.setState({  isDisabled: false})

                if(error.response !== undefined && error.response.status === 422){
                    let errorTxt = '';
                    let errorMessage = error.response.data.errors;

                    Object.keys(errorMessage).map((key, i) => {
                        errorTxt = (<span> {errorTxt} {errorMessage[key][0]} <br /></span>);});
                    toast.update(ldToast, {
                        type: toast.TYPE.ERROR,
                        render: errorTxt
                    });
                }
            });

    }

    registerPrdct(e){

        e.preventDefault();
        this.setState({isRgstrDisabled: true});
        const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

        axios.post(this.props.defaultSets.apiUrl+'register-product', {
            firstName: this.state.firstName,
            lastName: this.state.lastName,
            email: this.state.email,
            phone: this.state.phone,
            countryCode: this.state.countryCode,
            serialNum: this.state.serialNum,
            guaranteeNum: this.state.guaranteeNum,
            adrs: this.state.adrs,
            langCode: this.props.selectedLanguage.langCode
        })
            .then( (response) => {

                if(response.data.success == true){
                    return toast.update(ldToast, {
                        type: toast.TYPE.SUCCESS,
                        render: response.data.msg
                    });
                }
                return toast.update(ldToast, {
                    type: toast.TYPE.ERROR,
                    render: response.data.msg
                });
                
            })
            .catch( (error) => {
                //console.log(error.response);
                this.setState({  isRgstrDisabled: false})

                if(error.response !== undefined && error.response.status === 422){
                    let errorTxt = '';
                    let errorMessage = error.response.data.errors;

                    Object.keys(errorMessage).map((key, i) => {
                        errorTxt = (<span> {errorTxt} {errorMessage[key][0]} <br /></span>);});
                    toast.update(ldToast, {
                        type: toast.TYPE.ERROR,
                        render: errorTxt
                    });
                }
            });

    }
    render() {
 
        //const {} = this.state;
        const lng = this.props.selectedLanguage;
        const sts = this.props.defaultSets;

        return (
    
            <Fragment>
                <Helmet><title>{lng.contactTxt}</title></Helmet>

<BreadCrumb title={lng.verifyPrdctTxt} />


<section className="register-page section-b-space">
      <div className="container">
        <div className="row">
          <div className="col-lg-12">
            <h3>{lng.registerPrdctTxt}</h3>
            <div className="theme-card">
            <form className="theme-form" onSubmit={this.registerPrdct}>                   
              
            <div className="form-row"> 
                                <div className="col-md-6">
                                    <label htmlFor="email">{lng.firstNameTxt}</label>
                                            
                                    <input type="text" 
                                    className="form-control"
                                    name="firstName"
                                    placeholder={lng.firstNameTxt}
                                    onChange={this.onChangeValue} />
                                </div>
                                <div className="col-md-6">
                                    <label htmlFor="review">{lng.lastNameTxt}</label>
                                    
                                    <input 
                                    type="text" 
                                    className="form-control"
                                    name="lastName"
                                    placeholder={lng.lastNameTxt}
                                    onChange={this.onChangeValue} />
                                </div>
                            </div>

                            <div className="form-row">
                                <div className="col-md-6">
                                    <label htmlFor="email">{lng.emailTxt}</label>
                                    <input 
                                placeholder={lng.emailTxt}
                                type="text" 
                                onChange={this.onChangeValue}
                                className="form-control" 
                                name="email" />
                                </div>
                                <div className="col-md-6">
                                    <label htmlFor="review">{lng.phoneTxt}</label>
                                    <input 
                                placeholder="+905541710000"
                                type="text" 
                                onChange={this.onChangeValue}
                                className="form-control" 
                                name="phone" />
                                    
                                <span className="text-danger">
                                   
                                </span>
                                </div>
                       
                          
                            </div>

                <div className="form-row">
                  <div className="col-md-6">
                    <label htmlFor="text">{lng.serialNumTxt}</label>
                    <input
                      type="text"
                      className="form-control"
                      name="serialNum"
                      onChange={this.onChangeValue}
                    />
                  </div>
                  <div className="col-md-6">
                    <label htmlFor="text">{lng.guaranteeNumTxt}</label>
                    <input
                      type="text"
                      className="form-control"
                      name="guaranteeNum"                      
                      onChange={this.onChangeValue}
                    />
                  </div>
                </div>
                <div className="form-row">
                  <div className="col-md-12">
                    <label htmlFor="text">{lng.countryTxt}</label>
                    <ReactFlagsSelect
                    searchable={true}
                    defaultCountry="TR"
                    searchPlaceholder={lng.countryTxt}    
                    onSelect={(countryCode) => {this.setState({countryCode})}}
                    />
                  </div>
                </div>
                <div className="form-row">
                  <div className="col-md-12">
                  <label htmlFor="email">{lng.adrsTxt}</label>                                            
                    <input 
                    placeholder={lng.adrsTxt}
                    type="text" 
                    onChange={this.onChangeValue}
                    className="form-control" 
                    name="adrs"
                    />
                  </div>
                </div>
                <button type="submit" disabled={this.state.isRgstrDisabled} className="btn btn-solid">{lng.registerPrdctTxt}</button>
              </form>
            </div>
          </div>
          <div className="col-lg-12 mt-5">
            <h3>{lng.verifyPrdctTxt}</h3>
            <div className="theme-card">
            <form className="theme-form" onSubmit={this.verifyPrdct}>                        
                <div className="form-row">
                  <div className="col-md-6">
                    <label htmlFor="text">{lng.serialNumTxt}</label>
                    <input
                      type="text"
                      className="form-control"
                      name="serialNum"
                      onChange={this.onChangeValue}
                    />
                  </div>
                  <div className="col-md-6">
                    <label htmlFor="text">{lng.guaranteeNumTxt}</label>
                    <input
                      type="text"
                      className="form-control"
                      name="guaranteeNum"                      
                      onChange={this.onChangeValue}
                    />
                  </div>
                </div>
                <button 
                        type="submit" 
                        disabled={this.state.isDisabled} 
                        className="btn btn-solid">
                        {lng.verifyTxt}
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
    


          </Fragment>

    )
    }
}



const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        cartItems: state.cartItems.cart,
        //userInfo: state.User.info
        //selectedCurr: state.Curr.slctdCurr,
        defaultSets: state.DfltSts
    }
}
export default connect(mapStateToProps, null)(Verify);