import React, {Component, Fragment} from 'react';
import { connect } from 'react-redux';
import BreadCrumb from '../partials/BreadCrumb';
import {toast} from "react-toastify";
import ProductBox from '../../../components/ProductBox';

import {Helmet} from "react-helmet";
import postData from '../../../helpers/postData';


class Show extends Component {

    constructor(props) {
        super(props);
        this.state = {
            rows: [],
            catInfo: '',
            
            offers: [],
            //catId: 0,
            //isLoaded: false
        };
        this.addToFav = this.addToFav.bind(this); 

    }

    componentDidMount(){
        //this.setState({catId: this.props.match.params.id, isLoaded: true})
        this.fetchProducts(this.props.match.params.id);

    }

    componentDidUpdate(prevProps, prevState) {

        if (prevProps.match.params.id != this.props.match.params.id ) {
            this.fetchProducts(this.props.match.params.id);
        }
      }
    
      addToFav(item) {
        
        if(this.props.userInfo.id == 0) {
            return toast.info(this.props.selectedLanguage.plsLgnFrst);
        }
        const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

        postData(this.props.defaultSets.apiUrl+'favourites', {
            userId: this.props.userInfo.id,
            itemId: item.id,
            langCode: this.props.selectedLanguage.langCode
        })
            .then( (response) => {

                if(response.data.success == true) {
                    toast.update(ldToast, {
                        type: toast.TYPE.SUCCESS,
                        render: response.data.msg
                    });
                    item.favId = response.data.favId;

                    return this.props.addToFavourites(item);
                }
                return toast.update(ldToast, {
                    type: toast.TYPE.ERROR,
                    render: response.data.msg
                });
            })
            .catch( (error) => {
               
            });

    }
    fetchProducts(categoryId) {
        
        axios.get(this.props.defaultSets.apiUrl+'prdcts-by-catId/'+categoryId)
            .then(response => {  
              this.setState({rows: response.data, catInfo: response.data[0].category});
                //console.log(response.data)
            })
            .catch(function (error) {
                console.log(error);
            });
      
    }

  

    
    fetchOffers() {
        
        axios.get(this.props.defaultSets.apiUrl+'offer-products')
            .then(response => {  
              this.setState({offers: response.data});
                
            })
            .catch(function (error) {
                console.log(error);
            });
      
    }


    render() {
 
        const {rows, catInfo} = this.state;
        const lng = this.props.selectedLanguage;
        return (
    
            <Fragment>
            <Helmet><title>{catInfo && eval('catInfo.title'+lng.langCode)}</title></Helmet>

 <BreadCrumb title={catInfo && eval('catInfo.title'+lng.langCode)} />



 <section className="about-page  section-b-space">
          <div className="container">
            <div className="row">
              <div className="col-lg-5 col-sm-12">
                <div className="banner-section">
                  <img
                    src="../assets/images/about/mwf-logo.png"
                    className="img-fluid"
                    alt=""
                  />
                </div>
              </div>
              <div className="col-sm-12 col-lg-7">
                <h4>{catInfo && eval('catInfo.title'+lng.langCode)}</h4>
                <p>
                  Works this system of gold and metal detectors by feature to
                  monitoring and sensing the targets from far distance, sends
                  this kind of detectors and receives a different signal
                  frequency, Including digital signal frequency, or
                  low-frequency resonance signal and other, through broadcast
                  units and broadcast antennas of the signal.
                  <br />
                </p>
              </div>
            </div>
          </div>
        </section>




        <div className="title1 pt-5">
        <h4>{lng.bestOffersTxt}</h4>
        <h2 className="title-inner1">{lng.newProductsTxt}</h2>
    </div>
    <section className="p-t-0 ratio_asos pb-5">
        <div className="container">
            <div className="row">
                <div className="col">
                    <div className="product-5 product-m no-arrow">
                        
                        {rows && rows.map((item, i) => {
                            return(<ProductBox
                                obj={item} 
                                selectedLanguage={this.props.selectedLanguage}                            
                                key={'prdctBox'+i}                
                                addToCart={this.props.addToCart} 
                                addToFav={this.addToFav}          
                                favedItems={this.props.favedItems} 
                                />);
                            })
                        }
                    </div>
                    
                </div>
            </div>
        </div> 
    </section>
         </Fragment>  
    )
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        addToCart: (product, msg) => {
            toast.success(msg);
            dispatch({ type: 'ADD_TO_CART', payload: product })
        },
        addToFavourites: (item) => {
            dispatch({ type: 'ADD_TO_FAV', payload: item });
        }
    }
}


const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        //cartItems: state.cartItems.cart,
        userInfo: state.User.info,
        favedItems: state.Favourites.rows,
        //selectedCurr: state.Curr.slctdCurr,
        defaultSets: state.DfltSts,
        cats: state.Cats.rows,
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Show);