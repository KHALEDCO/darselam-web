import React, {Component, Fragment} from 'react';
import { connect } from 'react-redux';
import BreadCrumb from '../partials/BreadCrumb';
import {Helmet} from "react-helmet";

import {
    Accordion,
    AccordionItem,
    AccordionItemHeading,
    AccordionItemButton,
    AccordionItemPanel,
} from 'react-accessible-accordion';
 
// Demo styles, see 'Styles' section below for some notes on use.
import 'react-accessible-accordion/dist/fancy-example.css';

class Index extends Component {

    constructor(props) {
        super(props);
        this.state = {
            rows: [],
        };
        
    }

    componentDidMount(){
        this.fetchFaqs();        
    }

    fetchFaqs() {
        
        axios.get(this.props.defaultSets.apiUrl+'faqs/status/1')
            .then(response => {  
              this.setState({rows: response.data.rows});
                //console.log(response.data.rows)
            })
            .catch(function (error) {
                console.log(error);
            });
      
    }

    render() {
 
        const {rows} = this.state;
        const lng = this.props.selectedLanguage;

        return (
    
            <Fragment>
<Helmet><title>{lng.faqsTxt}</title></Helmet>

 <BreadCrumb title={lng.faqsTxt} />

 <section class="faq-section section-b-space">
        <div class="container">
            <div className="row">
                <div className="col-lg-12">         
        <Accordion>
            {rows && rows.map((item, i) => {
    let elmntTrns = item.elment_trans && item.elment_trans.find(row => row.languageCode == lng.langCode);

                return(
                    
            <AccordionItem key={item.id}>
            <AccordionItemHeading>
                <AccordionItemButton>
                    {elmntTrns && elmntTrns.description1}
                </AccordionItemButton>
            </AccordionItemHeading>
            <AccordionItemPanel>
            {elmntTrns && 
                  <div dangerouslySetInnerHTML={ { __html: elmntTrns.description2 } }/> 
                  }

            </AccordionItemPanel>
        </AccordionItem>
        
                )
            })}
        </Accordion>
  
        </div>
            </div>
        </div>
    </section>    
 </Fragment>

    )
    }
}





const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        //cartItems: state.cartItems.cart,
        //userInfo: state.User.info
        //selectedCurr: state.Curr.slctdCurr,
        defaultSets: state.DfltSts
    }
}
export default connect(mapStateToProps, null)(Index);