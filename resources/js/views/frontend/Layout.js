
import React, {Component, Fragment} from 'react'
import { connect } from 'react-redux';
import {ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.min.css';

// import theme styles.

// import theme scripts.
import {appendScript, appendCss} from '../../helpers/appendScript';

// import theme partials.
import Header from './partials/Header';
import Footer from './partials/Footer';
// import modals.
//import ResponsiveNav from './partials/ResponsiveNav';
import getData from '../../helpers/getData';

import {ar} from '../../lang/ar';
import {en} from '../../lang/en';
import {tr} from '../../lang/tr';

class Layout extends Component {

    constructor(props) {
        super(props);
        this.state = {
            cpages: [],
            loading: true
        }
    }

    
    loadCss() {

        appendCss("/assets/frontend/css/fontawesome.css");/* 
        appendCss("/assets/frontend/css/slick.css");
        appendCss("/assets/frontend/css/slick-theme.css"); */
        appendCss("/assets/frontend/css/themify-icons.css");
        appendCss("/assets/frontend/css/bootstrap.css");
        appendCss("/assets/frontend/css/color3.css")
        
}

/*
loadJs() {

    appendScript("/assets/frontend/js/jquery-3.3.1.min.js")
.then(function() {
return appendScript("/assets/frontend/js/menu.js");
})
.then(function() {
return appendScript("/assets/frontend/js/lazysizes.min.js");
})
.then(function() {
return appendScript("/assets/frontend/js/popper.min.js");
})
.then(function() {
return appendScript("/assets/frontend/js/slick.js");
})
.then(function() {
return appendScript("/assets/frontend/js/bootstrap.js");
})
.then(function() {
return appendScript("/assets/frontend/js/bootstrap-notify.min.js");
})
.then(function() {
return appendScript("/assets/frontend/js/script.js");
})
.then((script) => {
// use functions declared in scripts
// to show that they indeed loaded
$('.product-4 .product-5').slick({
    infinite: true,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 4,
    autoplay: true,
    autoplaySpeed: 3000,
    responsive: [{
            breakpoint: 1200,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3
            }
        },
        {
            breakpoint: 991,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        }
    ]
});
this.setState({loading: false})

});

     appendScript("/assets/frontend/js/jquery-3.3.1.min.js")
    .then(script1 => {
        appendScript("/assets/frontend/js/menu.js").then(script2 => {
            appendScript("/assets/frontend/js/lazysizes.min.js").then(script3 => {
                appendScript("/assets/frontend/js/popper.min.js").then(script3 => {
                    appendScript("/assets/frontend/js/slick.js").then(script3 => {
                        appendScript("/assets/frontend/js/bootstrap.js").then(script3 => {
                            appendScript("/assets/frontend/js/bootstrap-notify.min.js").then(script3 => {
                                appendScript("/assets/frontend/js/script.js").then(script3 => {
                                    this.setState(() => ({loading: false}))
                                   
                                });
                            });
                        });
                    });
                });
          });
        });
      }); 
    appendScript("/assets/frontend/js/jquery-3.3.1.min.js"); 
    appendScript("/assets/frontend/js/menu.js");
    appendScript("/assets/frontend/js/lazysizes.min.js");
    appendScript("/assets/frontend/js/popper.min.js");
    appendScript("/assets/frontend/js/slick.js");
    appendScript("/assets/frontend/js/bootstrap.js");
    appendScript("/assets/frontend/js/bootstrap-notify.min.js");
    appendScript("/assets/frontend/js/script.js"); 
}*/

    loadJs() {
        appendScript("/assets/frontend/js/jquery-3.3.1.min.js"); 
        appendScript("/assets/frontend/js/menu.js");
        appendScript("/assets/frontend/js/lazysizes.min.js");
        appendScript("/assets/frontend/js/popper.min.js");
        /* appendScript("/assets/frontend/js/slick.js"); */
        appendScript("/assets/frontend/js/bootstrap.js");
        appendScript("/assets/frontend/js/bootstrap-notify.min.js");
        appendScript("/assets/frontend/js/script.js");
    }
    componentDidMount() {
                    //console.log(this.props.slctdCurr)

        this.loadCss();

        window.addEventListener('load', this.loadJs());

        this.fetchSettings();
        this.fetchCategories();
        this.fetchFeatures();
        this.fetchCpages();
        this.fetchPrdcts();
        this.fetchCurrencies();
        this.fetchLanguages();

         //adjust directions of html container document
         document.getElementsByTagName("html")[0].setAttribute("dir", this.props.selectedLanguage.langDir);
         document.getElementsByTagName("html")[0].setAttribute("lang", this.props.selectedLanguage.langCode);
         document.getElementsByTagName("body")[0].setAttribute("class", this.props.selectedLanguage.langDir);
        this.props.selectedLanguage.langCode == 'AR'? this.props.selectLanguage(ar) : this.props.selectLanguage(en);


        if (this.state.loading) {
            setTimeout(() => { 
              this.setState(() => ({loading: false}))
            }, 1500);
          }
    }

    
    componentWillUnmount(){
        // remove css links.
        var element = document.getElementsByTagName("link"), index;

        for (index = element.length - 1; index >= 0; index--) {
            element[index].parentNode.removeChild(element[index]);
        }

        // remove scripts.
        var element = document.getElementsByTagName("script"), index;

        for (index = element.length - 1; index >= 0; index--) {
            element[index].parentNode.removeChild(element[index]);
        }
    }

    componentDidUpdate(){

        // remove old scripts.
        var element = document.getElementsByTagName("script"), index;

        for (index = element.length - 1; index >= 0; index--) {
            element[index].parentNode.removeChild(element[index]);
        }
        
        //this.loadJs();       
        window.addEventListener('load', this.loadJs());


    }
    fetchCpages() {
        
        getData(this.props.defaultSets.apiUrl+'cpages/status/1')
            .then(response => {  
                //console.log(response.data.rows)
                this.setState({cpages: response.data.rows})
            })
            .catch(function (error) {
                console.log(error);
            });
      
    }
    fetchCategories() {

        getData(this.props.defaultSets.apiUrl+'categories/status/1')

            .then(response => {  
                this.props.storeCats(response.data.rows)
            })
            .catch(function (error) {
                console.log(error);
            });
      
    }

    fetchCurrencies() {

        getData(this.props.defaultSets.apiUrl+'currencies/status/1')

            .then(response => {  
                this.props.storeCurrs(response.data.rows)
            })
            .catch(function (error) {
                console.log(error);
            });
      
    }

    fetchLanguages() {

        getData(this.props.defaultSets.apiUrl+'languages/status/1')

            .then(response => {  
                this.props.storeLanguages(response.data.rows)
            })
            .catch(function (error) {
                console.log(error);
            });
      
    }
    fetchFeatures() {

        getData(this.props.defaultSets.apiUrl+'features/status/1')

            .then(response => {  
                //console.log(response.data)
                this.props.storeFeatures(response.data.rows)
            })
            .catch(function (error) {
                console.log(error);
            });
      
    }
    
    fetchPrdcts() {

        getData(this.props.defaultSets.apiUrl+'products/status/1')
  
          .then(response => {  
              this.props.storePrdcts(response.data.rows)
          })
          .catch(function (error) {
              console.log(error);
          });
      
    }

    fetchSettings() {
        
        getData(this.props.defaultSets.apiUrl+'settings')

            .then(response => {  
                //console.log('set', response.data)
              this.props.storeSettings(response.data);

            })
            .catch(function (error) {
                console.log(error);
            });
      
    }

    render() {
        if(this.state.loading){
            //console.log(this.state.loading)
            return (<div className="loader-wrapper"><div className="loader"></div></div>);
        }
        
        return (

        <Fragment>
            
            <ToastContainer
                        rtl={true}
                        autoClose={5000}
                        position="top-center"
                        pauseOnHover={true}
                        hideProgressBar={true}
                    />
   {/*  <div id="preloder">
        <div className="loader"></div>
    </div> */}
        
         <Header  
         userInfo={this.props.userInfo} 
         slctdCurr={this.props.slctdCurr}          
         changeCurrency={this.props.changeCurrency}
         logout={this.props.logout} 
         defaultSets={this.props.defaultSets} 
         selectedLanguage={this.props.selectedLanguage}
         selectLanguage={this.props.selectLanguage}
         cartItems={this.props.cartItems}
         removeItem={this.props.removeItem}
         cpages={this.state.cpages}
         favedItems={this.props.favedItems}
         cats={this.props.cats}
         currs={this.props.currs}
         features={this.props.features}
         products={this.props.products}
         languages={this.props.languages}
         ar={ar}
         en={en}
         tr={tr} />
         
         

         {this.props.children}

         <Footer 
         selectedLanguage={this.props.selectedLanguage}
         defaultSets={this.props.defaultSets} 
         cats={this.props.cats}
         cpages={this.state.cpages} />

         
    <div className="tap-top">
      <div><i className="fa fa-angle-double-up"></i></div>
    </div>       
        </Fragment>
 
        );
     }
}


const mapDispatchToProps = (dispatch) => {
    return {
        //selectLanguage: (val) => dispatch({ type: 'language_data', payload: val }),
        selectLanguage: (val) =>  
        {
            document.getElementsByTagName("html")[0].setAttribute("lang", val.langCode);
            document.getElementsByTagName("html")[0].setAttribute("dir", val.langDir);
            document.getElementsByTagName("body")[0].setAttribute("class", val.langDir);
            dispatch({ type: 'language_data', payload: val })
        },
        changeCurrency: (curr) => dispatch({ type: 'CHANGE_CURRENCY', payload: curr }),
        removeItem: (product) => dispatch({ type: 'REMOVE_FROM_CART', payload: product }),
        storeCats: (cats) => dispatch({ type: 'SAVE_CATS', payload: cats }),
        storeLanguages: (languages) => dispatch({ type: 'SAVE_LANGUAGES', payload: languages }),
        storeCurrs: (currs) => dispatch({ type: 'SAVE_CURRS', payload: currs }),
        storeFeatures: (features) => dispatch({ type: 'SAVE_FEATURES', payload: features }),
        storePrdcts: (prdcts) => dispatch({ type: 'SAVE_PRODUCTS', payload: prdcts }),
        storeSettings: (sets) => dispatch({ type: 'SAVE_SETTINGS', payload: sets }),
        logout: () => dispatch({ type: 'LOGOUT', payload: null }),

    }
}; 

const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,        
        slctdCurr: state.Currs.currencyInfo,
        userInfo: state.User.info,
        cartItems: state.cartItems,
        defaultSets: state.DfltSts,
        favedItems: state.Favourites.rows,
        cats: state.Cats.rows,
        currs: state.Currs.rows,
        languages: state.Languages.rows,
        features: state.Features.rows,
        products: state.Products.rows,
    }
};
  export default connect(mapStateToProps, mapDispatchToProps)(Layout);



