import React, {Component, Fragment} from 'react';
import { connect } from 'react-redux';
import BreadCrumb from '../partials/BreadCrumb';
import {toast} from "react-toastify";
import ProductBox from '../../../components/ProductBox';

import {Helmet} from "react-helmet";
import postData from '../../../helpers/postData';
//import ProductBoxInCat from '../../../components/ProductBoxInCat';
import Carousel, { consts } from 'react-elastic-carousel';


class Show extends Component {

    constructor(props) {
        super(props);
        this.state = {
            rows: [],
            featureInfo: '',
            
            //offers: [],
            //catId: 0,
            //isLoaded: false
        };
        this.addToFav = this.addToFav.bind(this); 

    }

    componentDidMount(){
        //this.setState({catId: this.props.match.params.id, isLoaded: true})
        this.fetchProducts(this.props.match.params.id);

    }

    componentDidUpdate(prevProps, prevState) {

        if (prevProps.match.params.id != this.props.match.params.id ) {
            this.fetchProducts(this.props.match.params.id);
        }
      }
    
      addToFav(item) {
        
        if(this.props.userInfo.id == 0) {
            return toast.info(this.props.selectedLanguage.plsLgnFrst);
        }
        const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

        postData(this.props.defaultSets.apiUrl+'favourites', {
            userId: this.props.userInfo.id,
            itemId: item.id,
            langCode: this.props.selectedLanguage.langCode
        }, this.props.userInfo.accessToken)
            .then( (response) => {

                if(response.data.success == true) {
                    toast.update(ldToast, {
                        type: toast.TYPE.SUCCESS,
                        render: response.data.msg
                    });
                    item.favId = response.data.favId;

                    return this.props.addToFavourites(item);
                }
                return toast.update(ldToast, {
                    type: toast.TYPE.ERROR,
                    render: response.data.msg
                });
            })
            .catch( (error) => {
               
            });

    }
    fetchProducts(featureId) {
        
        axios.get(this.props.defaultSets.apiUrl+'prdcts-by-featureId/'+featureId)
            .then(response => {  
                //console.log(response.data.featureInfo);

              this.setState({rows: response.data.rows, featureInfo: response.data.featureInfo});
            })
            .catch(function (error) {
                console.log(error);
            });
      
    }

  
    getPrdctDesc1(description1) {
        return { __html: description1 };
      }

      getPrdctDesc2(description2) {
        return { __html: description2 };
      }

    render() {
 
        const {rows, featureInfo} = this.state;
        const lng = this.props.selectedLanguage;
        const elmntInfoTrns = featureInfo && featureInfo.elment_trans.find(item => item.languageCode == lng.langCode);

        const breakPoints = [
            { width: 1, itemsToShow: 1 },
            { width: 550, itemsToShow: 2, itemsToScroll: 2 },
            { width: 850, itemsToShow: 4 },
            { width: 1150, itemsToShow: 4, itemsToScroll: 2 },
            { width: 1450, itemsToShow: 5 },
            { width: 1750, itemsToShow: 6 },
          ]
        return (
    
            <Fragment>
            <Helmet><title>{elmntInfoTrns && elmntInfoTrns.title}</title></Helmet>

 <BreadCrumb title={elmntInfoTrns && elmntInfoTrns.title} />


 <section className="about-page  section-b-space">
          <div className="container">
            <div className="row">
              <div className="col-lg-5 col-sm-12">
                <div className="banner-section">
                  <img
                  src={"/uploads/files/"+featureInfo.photo} 
                  className="img-fluid" 
                  alt={elmntInfoTrns && elmntInfoTrns.title}
                  />
                </div>
              </div>
              <div className="col-sm-12 col-lg-7">
                {/* <h4>Geophysical Survey Systems</h4> */}
                
                {elmntInfoTrns && 
                  <div dangerouslySetInnerHTML={this.getPrdctDesc1(elmntInfoTrns.description1)}/> 
                  }
              </div>
            </div>
          </div>
        </section>


        <div className="title1 pt-5">
        <h4>{featureInfo ? eval('featureInfo.title'+lng.langCode) : lng.allProductsTxt}</h4>
        <h2 className="title-inner1">{lng.newProductsTxt}</h2>
    </div>
    <section className="p-t-0 ratio_asos pb-5">
        <div className="container">
            <div className="row">
                <div className="col">
                    <div className="product-m no-arrow">
                             
                    <Carousel
                    
                    enableAutoPlay={true}
                    autoPlaySpeed={5000}
                    disableArrowsOnEnd={false}
                        breakPoints={breakPoints} 
                        pagination={false}
                        isRTL={lng.langCode == 'AR'? true: false}
                        renderArrow={({type, onClick}) => <div onClick={onClick}>{type === consts.PREV ? 
                            ''
                         : 
                          ''  }
                        </div>}
    
                        itemPadding={[0, 10]} 
                        itemsToShow={4} 
                        itemPosition={consts.CENTER}>
                    {rows && rows.map((item, i) => {
                        return(<ProductBox 
                            obj={item} 
                            selectedLanguage={this.props.selectedLanguage}      
                            slctdCurr={this.props.slctdCurr}                        
                            favedItems={this.props.favedItems} 
                            key={'la'+i}                                
                            addToCart={this.props.addToCart} 
                            addToCompare={this.props.addToCompare}
                            addToFav={this.addToFav}
                            />);
                        })
                    }
                            </Carousel>
                     
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section className="detector-system py-5">
          <div className="container">
            <div className="row">
              <div className="col-sm-12">
                <div className="pt-5">
                  {elmntInfoTrns && 
                  <div dangerouslySetInnerHTML={this.getPrdctDesc2(elmntInfoTrns.description2)}/> 
                  }
                </div>
              </div>
            </div>
          </div>
        </section>

</Fragment>  
    
    )
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        addToCart: (product, msg) => {
            toast.success(msg);
            dispatch({ type: 'ADD_TO_CART', payload: product })
        },
        addToFavourites: (item) => {
            dispatch({ type: 'ADD_TO_FAV', payload: item });
        }
    }
}


const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        //cartItems: state.cartItems.cart,
        userInfo: state.User.info,
        favedItems: state.Favourites.rows,
        slctdCurr: state.Currs.currencyInfo,
        defaultSets: state.DfltSts,
        cats: state.Cats.rows,
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Show);