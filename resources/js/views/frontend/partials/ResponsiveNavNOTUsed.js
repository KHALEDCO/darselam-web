import React, {Component, Fragment} from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import {ar} from '../../../lang/ar';
import {en} from '../../../lang/en';

class ResponsiveNav extends Component {

    render() {

        const { home, aboutUs, shoppingCart, homeTxt, servicesTxt, articlesTxt,
        aboutUsTxt, distributorsTxt, productsTxt, backTxt, langCode} = this.props.selectedLanguage;
 
        const {welcomeMsgAR, welcomeMsgTR, welcomeMsgEN, phone1} = this.props.settings;

        return (

    
            
    <Fragment>
        <div className="humberger__menu__overlay"></div>
    <div className="humberger__menu__wrapper">
        <div className="humberger__menu__logo">
            <a href="#"><img src="/assets/frontend/img/logo.png" alt="" /></a>
        </div>
        <div className="humberger__menu__cart">
            <ul>
                <li><a href="#"><i className="fa fa-heart"></i> 
                <span>1</span></a>
                </li>
                <li><a href="#"><i className="fa fa-shopping-bag"></i> 
                <span>3</span>
                </a></li>
            </ul>
            <div className="header__cart__price">item: <span>$150.00</span></div>
        </div>
        <div className="humberger__menu__widget">
            <div className="header__top__right__language">
                <img src="/assets/frontend/img/language.png" alt="" />
                <div>English</div>
                <span className="arrow_carrot-down"></span>
                <ul>
                    <li>
                        <a href="#" onClick={(e)=> {e.preventDefault(); this.props.selectLanguage(ar)}}>
                            عربي
                        </a>
                    </li>
                    
                    <li>
                        <a href="#" onClick={(e)=> {e.preventDefault(); this.props.selectLanguage(en)}}>
                            English
                        </a>
                    </li>
                </ul>
            </div>
            <div className="header__top__right__auth">
                <a href="#"><i className="fa fa-user"></i> Loginnn</a>
            </div>
        </div>
        <nav className="humberger__menu__nav mobile-menu">
            <ul>
                <li className="active"><a href="./index.html">Home</a></li>
                <li><a href="./shop-grid.html">Shop</a></li>
                <li><a href="#">Pages</a>
                    <ul className="header__menu__dropdown">
                        <li><a href="./shop-details.html">Shop Details</a></li>
                        <li><a href="./shoping-cart.html">Shoping Cart</a></li>
                        <li><a href="./checkout.html">Check Out</a></li>
                        <li><a href="./blog-details.html">Blog Details</a></li>
                    </ul>
                </li>
                <li><a href="./blog.html">Blog</a></li>
                <li><a href="./contact.html">Contact</a></li>
            </ul>
        </nav>
        <div id="mobile-menu-wrap"></div>
        <div className="header__top__right__social">
            <a href="#"><i className="fa fa-facebook"></i></a>
            <a href="#"><i className="fa fa-twitter"></i></a>
            <a href="#"><i className="fa fa-linkedin"></i></a>
            <a href="#"><i className="fa fa-pinterest-p"></i></a>
        </div>
        <div className="humberger__menu__contact">
            <ul>
                <li><i className="fa fa-envelope"></i> {phone1}</li>
                <li>{this.props.settings && eval("welcomeMsg"+langCode)}</li>
            </ul>
        </div>
    </div>
    
    </Fragment>
    )
    }
}

const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        userInfo: state.User.info,
        cartItems: state.cartItems.cart,
        defaultSets: state.DfltSts
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        selectLanguage: (val) => dispatch({ type: 'language_data', payload: val }),
        //changeCurrency: (curr) => dispatch({ type: 'change_currency', payload: curr }),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ResponsiveNav);
