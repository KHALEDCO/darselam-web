import React, {Component} from 'react';
import { Link } from 'react-router-dom';

class BreadCrumb extends Component {

    constructor(props) {
        super(props);
        this.state = {
            //row: '',
        };
        
    }
    render() {
        const {title} = this.props;
        return (

            
    <div className="breadcrumb-section">
    <div className="container">
        <div className="row">
            <div className="col-sm-6">
                <div className="page-title">
                    <h2>{title? title: ''}</h2>
                </div>
            </div>
            <div className="col-sm-6">
                <nav aria-label="breadcrumb" className="theme-breadcrumb">
                    <ol className="breadcrumb">
                        <li className="breadcrumb-item"><Link to="/"><i className="fa fa-home"></i></Link></li>
                        <li className="breadcrumb-item active" aria-current="page">{title? title: ''}</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>

)
    }
}
export default BreadCrumb;
