import React, {Component, Fragment} from 'react';
import { connect } from 'react-redux';
import BreadCrumb from '../partials/BreadCrumb';
import Moment from 'moment';




class Show extends Component {

    constructor(props) {
        super(props);
        this.state = {
            row: '',
        };

        this.fetchArticle = this.fetchArticle.bind(this);
        
    }

    componentDidMount(){
        this.fetchArticle(this.props.match.params.id);
    }

    fetchArticle(id) {
        
        axios.get(this.props.defaultSets.apiUrl+'articles/'+id)
            .then(response => {  
              this.setState({row: response.data});
                console.log(response.data)
            })
            .catch(function (error) {
                console.log(error);
            });
      
    }

    render() {
 
        const item = this.state.row;
        const lng = this.props.selectedLanguage;
        let elmntTrns = item.elment_trans && item.elment_trans.find(row => row.languageCode == this.props.selectedLanguage.langCode);

        return (
    
            <Fragment>
 <BreadCrumb title={elmntTrns && elmntTrns.title} />

    
 <section className="blog-detail-page section-b-space ratio2_3">
        <div className="container">
        <div className="row">
                <div className="col-sm-12 blog-detail">
                    
                        
                <img
                    src={"/uploads/files/"+item.photo} 
                    alt={elmntTrns && elmntTrns.title} 
                    className="img-fluid blur-up lazyload" />

                    <h3>{elmntTrns && elmntTrns.title}</h3>
                    <ul className="post-social">
                        <li>{Moment(item.created_at).format("YYYY-MM-DD")}</li>
                        <li>{lng.writtenByAdminTxt}</li>{/* 
                        <li><i className="fa fa-heart"></i> 5 Hits</li>
                        <li><i className="fa fa-comments"></i> 10 Comment</li> */}
                    </ul>
                    
                    {elmntTrns && elmntTrns.description ?  
                        <div dangerouslySetInnerHTML={ { __html: elmntTrns.description } }></div>
                        : null
                    }
                          
                </div>
            </div>
        </div>
</section>
          </Fragment>

    )
    }
}





const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        //cartItems: state.cartItems.cart,
        //userInfo: state.User.info
        //selectedCurr: state.Curr.slctdCurr,
        defaultSets: state.DfltSts
    }
}
export default connect(mapStateToProps, null)(Show);