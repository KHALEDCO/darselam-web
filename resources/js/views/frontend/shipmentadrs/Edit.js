import React, {Component, Fragment} from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import BreadCrumb from '../partials/BreadCrumb';
import {toast} from "react-toastify";
//import CountriesSelect from '../../../components/CountriesSelect';
import CitiesSelect from '../../../components/CitiesSelect';
import {Helmet} from "react-helmet";
import getData from '../../../helpers/getData';
import putData from '../../../helpers/putData';
import ReactFlagsSelect from 'react-flags-select';
import AsyncSelect from 'react-select/async';


class Edit extends Component {

    constructor(props) {
        super(props);
        this.state = {
            firstName: '',
            lastName: '',
            receiverMobile: '',
            receiverMobile2: '',
            //password: '',
            countryId: '',
            countryCode: '',
            cityId: '',
            adrs: '',
            email: '',
            errorMessage: '',
            isDisabled: false,
            
            cities: [],
            cityName: '',
        };

        this.onChangeValue = this.onChangeValue.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.filterCities = this.filterCities.bind(this);
        this.loadCities = this.loadCities.bind(this);
        
    }

    componentDidMount(){    
        this.fetchData(this.props.match.params.id);    

    }

    componentWillUnmount() {
        //this.setState({galleryProducts: []})
        //document.getElementById("mishu").remove;
    }

    fetchData(elmntId) {

        getData(this.props.defaultSets.apiUrl+'shipmentadrs/'+elmntId+'/edit', this.props.userInfo.accessToken)
            //.then(response => response.json())
            .then(response => {
     
              this.setState({ 
                  id: elmntId,
                  firstName: response.data.firstName,
                  lastName: response.data.lastName,
                  receiverMobile: response.data.receiverMobile,
                  receiverMobile2: response.data.receiverMobile2,
                  countryId: response.data.countryId,
                  countryCode: response.data.country.countryCode,
                  cityId: response.data.cityId,        
                  cityTitleAR: response.data.city.titleAR,                  
                  cityTitleEN: response.data.city.titleEN,                  
                  adrs: response.data.adrs,
                  
                }, () => this.fetchCities());
                console.log(this.state.countryCode)
            })
            .catch(function (error) {
                console.log(error);
            });
      }
     
     
    onChangeValue(e) {
        this.setState({
            [e.target.name]: e.target.value, isDisabled: false
        });
        //console.log(this.state.cityId)

    }




    handleSubmit(e){

        e.preventDefault();
        this.setState({isDisabled: true});
        const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

        putData(this.props.defaultSets.apiUrl+'shipmentadrs/'+this.state.id, {
            //userId: this.props.userInfo.id,
            id: this.state.id,
            firstName: this.state.firstName,
            lastName: this.state.lastName,
            receiverMobile: this.state.receiverMobile,
            receiverMobile2: this.state.receiverMobile2,
            countryId: this.state.countryId,
            countryCode: this.state.countryCode,
            cityId: this.state.cityId,
            adrs: this.state.adrs,
            langCode: this.props.selectedLanguage.langCode
        }, this.props.userInfo.accessToken)
            .then( (response) => {

                toast.update(ldToast, {
                    type: toast.TYPE.SUCCESS,
                    render: response.data.msg
                });
                
                setTimeout( () => {
                    //this.props.history.push('/');
                }, 3000);
            })
            .catch( (error) => {
                //console.log(error.response);
                this.setState({  isDisabled: false})

                if(error.response !== undefined && error.response.status === 422){
                    let errorTxt = '';
                    let errorMessage = error.response.data.errors;

                    Object.keys(errorMessage).map((key, i) => {
                        errorTxt = (<span> {errorTxt} {errorMessage[key][0]} <br /></span>);});
                    toast.update(ldToast, {
                        type: toast.TYPE.ERROR,
                        render: errorTxt
                    });
                }
            });

    }

   
    
    /* cities selectbox functions */
    filterCities(cityName){
        //console.log(cityName)
        
        return this.state.cities.filter(i =>
          i.label.toLowerCase().includes(cityName.toLowerCase())
        );
      }; 

     loadCities(cityName, callback){

        setTimeout(() => {
          callback(this.filterCities(cityName));
        }, 1000);
      }; 

    fetchCities(){
        const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);
        this.setState({ isDisabled: true, isLoading: true, cities: []});

        axios.get(this.props.defaultSets.apiUrl+'country-cities/'+this.state.countryCode)  
            //.then(response => response.json())
            .then(response => {
                let cities = [];
                this.setState({ cities, isDisabled: false, isLoading: false });

                {response.data.rows && response.data.rows.map((item, i) => {                    
                    cities.push({ value: item.id, label: item.titleAR+'-'+item.titleEN})
                })} 
                //console.log(response.data.rows)

                toast.dismiss(ldToast)
                
            })
            .catch(function (error) {
                console.log(error);
            })
        
    }


      /* cities selectbox functions end */

    render() {
 
        const {isDisabled, firstName, lastName, receiverMobile, receiverMobile2,
        countryId, cityId, cityTitleAR, cityTitleEN, adrs, countryCode, cities, 
        isLoading} = this.state;
        
        const { receiverMobileTxt, receiverMobile2Txt,countryTxt, cityTxt, firstNameTxt, lastNameTxt, addShipmentAdrsTxt,
            saveTxt, updtShpmntAdrsTxt, showShipmentAdrsesTxt, adrsTxt
        , langCode} = this.props.selectedLanguage;
        return (

            <Fragment>
                <Helmet><title>{updtShpmntAdrsTxt}</title></Helmet>
                 <BreadCrumb title={updtShpmntAdrsTxt} />


                 <section className="register-page section-b-space">
        <div className="container">
            <div className="row">
                <div className="col-lg-12">
                    <h4>
                    <Link to="/shipment-adrs">
                    {showShipmentAdrsesTxt}
                    </Link>
                    </h4>
                    <div className="theme-card">
                        <form className="theme-form" onSubmit={this.handleSubmit}>                        

                            <div className="form-row">
                                <div className="col-md-6">
                                    <label htmlFor="email">{firstNameTxt}</label>
                                            
                                    <input type="text" 
                                    className="form-control"
                                    name="firstName"
                                    defaultValue={firstName}
                                    placeholder={firstNameTxt}
                                    onChange={this.onChangeValue} />
                                </div>
                                <div className="col-md-6">
                                    <label htmlFor="review">{lastNameTxt}</label>
                                    
                                    <input 
                                    type="text" 
                                    className="form-control"
                                    name="lastName"
                                    defaultValue={lastName}
                                    placeholder={lastNameTxt}
                                    onChange={this.onChangeValue} />
                                </div>
                            </div>

                            <div className="form-row">
                                <div className="col-md-6">
                                    <label htmlFor="email">{receiverMobileTxt}</label>
                                    <input 
                                placeholder={receiverMobileTxt}
                                type="text" 
                                onChange={this.onChangeValue}
                                className="form-control" 
                                name="receiverMobile"
                                defaultValue={receiverMobile} />
                                </div>
                                <div className="col-md-6">
                                    <label htmlFor="review">{receiverMobile2Txt}</label>
                                    <input 
                                placeholder={receiverMobile2Txt}
                                type="text" 
                                onChange={this.onChangeValue}
                                className="form-control" 
                                name="receiverMobile2"
                                defaultValue={receiverMobile2} />
                                </div>
                       
                          
                            </div>



                            <div className="form-row">
                                <div className="col-md-6">
                                    <label htmlFor="email">{countryTxt}</label>
                                   {countryCode && <ReactFlagsSelect
                                                       className="form-control"

                    searchable={true}
                    defaultCountry={countryCode}
                    searchPlaceholder={countryTxt}    
                    onSelect={(countryCode) => {this.setState({countryCode}, () => this.fetchCities())}}
                    />}
                                </div>
                                <div className="col-md-6">
                                    {/* <label htmlFor="review">{cityTxt}</label> */}
                                    {cityId && <AsyncSelect
                    classNamePrefix={cityTxt}
                    defaultValue={cityId}
                    defaultInputValue={langCode == 'AR'? cityTitleAR: cityTitleEN}

                    /* isDisabled={isDisabled} */
                    isLoading={isLoading}
                    isRtl={langCode == 'AR'? true: false}
                    isSearchable={true}
                    name="cityId"
                    onChange={(cityInfo) => this.setState({cityId: cityInfo.value})}
                    /*options={cities}*/
                    loadOptions={this.loadCities}
                    onInputChange={(cityName) => this.setState({ cityName })}

                    />}
                                </div>
                       
                          
                            </div>

                            <div className="form-row">
                                <div className="col-md-6">
                                    <label htmlFor="email">{adrsTxt}</label>
                                            
                                    <input 
                                placeholder={adrsTxt}
                                type="text" 
                                onChange={this.onChangeValue}
                                className="form-control" 
                                name="adrs"
                                defaultValue={adrs}
                                 />
                                </div>
                                <div className="col-md-6"></div>

                                <button type="submit" disabled={isDisabled} className="btn btn-solid">{saveTxt}</button>

                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>


            </Fragment>



    )
    }
}



const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        //cartItems: state.cartItems.cart,
        userInfo: state.User.info,
        //selectedCurr: state.Curr.slctdCurr,
        defaultSets: state.DfltSts
    }
}
export default connect(mapStateToProps, null)(Edit);