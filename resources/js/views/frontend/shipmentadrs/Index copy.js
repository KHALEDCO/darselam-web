import React, { Component, Fragment} from 'react'

import {toast} from 'react-toastify';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import BreadCrumb from '../partials/BreadCrumb';
//import ShipmentadrsRow from '../../../components/ShipmentadrsRow';
import {Helmet} from "react-helmet";
import getData from '../../../helpers/getData';
import deleteData from '../../../helpers/deleteData';

import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css

 class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            rows: []
        };
    }
  
   
    componentDidMount(){

      const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

      getData(this.props.defaultSets.apiUrl+'user-shpmnt-adrs/'+this.props.userInfo.id, this.props.userInfo.accessToken)

          //.then(response => response.json())
          .then(response => {
              this.setState({ rows: response.data.rows });
              toast.dismiss(ldToast)
              //console.log(response.data)
          })
          .catch(function (error) {
              console.log(error);
          })
    }

  
    
    deleteRow(id) {

        confirmAlert({
              
          customUI: ({ onClose }) => {
              return (
                  <div id="react-confirm-alert">
                      <div className="react-confirm-alert-overlay">
                          <div className="react-confirm-alert">
                              <div className="react-confirm-alert-body">
                                  <h1>{this.props.selectedLanguage.rUSureTxt}</h1>
                                  <div 
                                  style={{flexDirection: this.props.selectedLanguage.langCode == "AR" ? 'row-reverse': 'row'}}
                                  className="react-confirm-alert-button-group">
                                  <button
                                  className="yes"
                                  onClick={() => {
                                      deleteData(this.props.defaultSets.apiUrl+'shipmentadrs/'+id, this.props.userInfo.accessToken)
                                      .then(res => {
                                          document.getElementById(id).remove();            
                                      })
                                      .catch(err => {
                                          console.log(err);
                                      });
                                      onClose();
                                  }}
                                  >
                                          {this.props.selectedLanguage.yesTxt}
                                      </button>
  
                                      <button className="no" onClick={onClose}>
                                      {this.props.selectedLanguage.noTxt}
                                      </button>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
  
              );
            }
        });
    
  
      }
    render() {
        
        //const {isLoading} = this.state;
        const { firstNameTxt, lastNameTxt, countryTxt, cityTxt, adrsTxt,
          receiverMobileTxt, receiverMobile2Txt, shipmentAdrsesTxt, actionTxt
        , langCode, deleteTxt, crtNewTxt, updateTxt} = this.props.selectedLanguage;

        return (
           
         <Fragment>
<Helmet><title>{shipmentAdrsesTxt}</title></Helmet>

         <BreadCrumb title={shipmentAdrsesTxt} />


<section className="shoping-cart spad">
        <div className="container">
        <div className="row mb-4">
        <div className="col-lg-12">
              <Link to='create-shipment-adrs' className="site-btn">
                    {crtNewTxt}
              </Link> 
        </div>
        </div>
            <div className="row">
                <div className="col-lg-12">
                    <div className="shoping__cart__table">
                        <table>
                            <thead>
                                <tr>
                                    <th className="shoping__product">{firstNameTxt}</th>
                                    <th>{lastNameTxt}</th>
                                    <th>{receiverMobileTxt}</th>
                                    <th>{receiverMobile2Txt}</th>
                                    <th>{countryTxt}</th>
                                    <th>{cityTxt}</th>
                                    <th>{adrsTxt}</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            {this.state.rows && this.state.rows.map((item, i) => {
                                            return(
                                <tr id={item.id} key={i}>
                                                                    
                                  <td className="shoping__cart__item">
                                      {item.firstName}
                                  </td>
                                                                         
                                  <td className="shoping__cart__item">
                                      {item.lastName}
                                  </td>
                                                                         
                                  <td className="shoping__cart__item">
                                      {item.receiverMobile}
                                  </td>
                                                                         
                                  <td className="shoping__cart__item">
                                      {item.receiverMobile2}
                                  </td>
                                                                         
                                  <td className="shoping__cart__item">
                                      {eval("item.country.title"+langCode)}
                                  </td>
                                                                         
                                  <td className="shoping__cart__item">
                                      {eval("item.city.title"+langCode)}
                                  </td>
                                                                         
                                  <td className="shoping__cart__item">
                                      {item.adrs}
                                  </td>
                                   
                                   
                                    <td className="row shoping__cart__item__close">
                                        <span title={deleteTxt} 
                                        onClick={() => this.deleteRow(item.id)}>   
                                        <i className="fa fa-trash text-danger"></i>                                     
                                        </span>

                                        <span title={updateTxt}>   
                                        <Link 
                                        className="text-info"
                                          to={{
                                              pathname: "/edit-shipment-adrs/"+item.id,
                                              //data: this.props.obj // your data array of objects
                                            }}
                                        ><i className="fa fa-pencil"></i>   
                                        </Link>
                                                                          
                                        </span>
                                    </td>
                                </tr>
                               )
                            })}
                            
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>


         </Fragment>

        )
    }
}


const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        defaultSets: state.DfltSts,
        userInfo: state.User.info,
    }
}
export default connect(mapStateToProps, null)(Index);
