import React, {Component, Fragment} from 'react';
import { connect } from 'react-redux';
import BreadCrumb from './partials/BreadCrumb';
import {toast} from "react-toastify";
import { Helmet } from 'react-helmet';
import postData from '../../helpers/postData';

class Contact extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isDisabled: false
        };
        this.onChangeValue = this.onChangeValue.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount(){    

    }

    onChangeValue(e) {
        this.setState({
            [e.target.name]: e.target.value, isDisabled: false
        });
        //console.log(this.state.cityId)

    }
    

    handleSubmit(e){

        e.preventDefault();
        this.setState({isDisabled: true});
        const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

        postData(this.props.defaultSets.apiUrl+'contact', {
            name: this.state.name,
            phone: this.state.phone,
            email: this.state.email,
            msg: this.state.msg,
            langCode: this.props.selectedLanguage.langCode
        })
            .then( (response) => {

                toast.update(ldToast, {
                    type: toast.TYPE.SUCCESS,
                    render: response.data.msg
                });
                
                setTimeout( () => {
                    //this.props.history.push('/');
                }, 3000);
            })
            .catch( (error) => {
                //console.log(error.response);
                this.setState({  isDisabled: false})

                if(error.response !== undefined && error.response.status === 422){
                    let errorTxt = '';
                    let errorMessage = error.response.data.errors;

                    Object.keys(errorMessage).map((key, i) => {
                        errorTxt = (<span> {errorTxt} {errorMessage[key][0]} <br /></span>);});
                    toast.update(ldToast, {
                        type: toast.TYPE.ERROR,
                        render: errorTxt
                    });
                }
            });

    }
    render() {
 
        //const {} = this.state;
        const lng = this.props.selectedLanguage;
        const sts = this.props.defaultSets;

        return (
    
            <Fragment>
                <Helmet><title>{lng.contactTxt}</title></Helmet>

<BreadCrumb title={lng.contactTxt} />


<section className="contact-page section-b-space">
        <div className="container">
            <div className="row section-b-space">
                <div className="col-lg-7 map">
                    <iframe
                        src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d385044.5216941425!2d28.971338000000003!3d41.065488!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x27354dac035b1cf7!2zTVdGIERFREVLVMOWUiAtIEdvbGQgQW5kIE1ldGFsIERldGVjdG9ycyAtINij2KzZh9iy2Kkg2YPYtNmBINin2YTYsNmH2Kgg2YjYp9mE2YXYudin2K_Zhg!5e0!3m2!1sar!2sus!4v1590426880503!5m2!1sar!2sus"
                  allowFullScreen></iframe>
                </div>
                <div className="col-lg-5">
                    <div className="contact-right">
                        <ul>
                            <li>
                                <div className="contact-icon">
                                    <i className="fa fa-phone" aria-hidden="true" ></i>
                                    <h6>{lng.contactTxt}</h6>
                                </div>
                                <div className="media-body">
                                    <a href={"tel:"+sts.phone1}>{sts.phone1}</a>
                                </div>
                            </li>
                    <li>
                      <div className="contact-icon">
                        <i className="fa fa-mobile-phone" aria-hidden="true" ></i>
                        <h6>{lng.contactTxt}</h6>
                      </div>
                      <div className="media-body">
                        <p>
                          <a href={"tel:"+sts.phone1}>{sts.phone1}</a>
                        </p>
                        <p>
                          <a href={"tel:"+sts.phone2}>{sts.phone2}</a>
                        </p>
                      </div>
                    </li>
                    <li>
                      <div className="contact-icon">
                        <i className="fa fa-map-marker" aria-hidden="true" ></i>
                        <h6>{lng.adrsTxt}</h6>
                      </div>
                      <div className="media-body">
                        <p>
                        {eval("sts.adrs"+lng.langCode)}
                        </p>
                      </div>
                    </li>
                            <li>
                      <div className="contact-icon">
                        <i className="fa fa-envelope-o" aria-hidden="true" ></i>
                        <h6>{lng.emailTxt}</h6>
                      </div>
                      <div className="media-body">
                        <p className="pt-2">
                          <a href={"mailto:"+sts.email1}>
                            {sts.email1}
                          </a>
                        </p>
                      </div>
                    </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-sm-12">
                    
                    <form className="theme-form" onSubmit={this.handleSubmit}>

                        <div className="form-row">
                            <div className="col-md-6">
                                <label htmlFor="name">{lng.nameTxt}</label>
                                
                    <input type="text" 
                                    className="form-control"
                                    name="name"
                                    placeholder={lng.nameTxt}
                                    onChange={this.onChangeValue} />
                            </div>
                            <div className="col-md-6">
                                <label htmlFor="review">{lng.phoneTxt}</label>
                                <input 
                                    type="text" 
                                    className="form-control"
                                    name="phone"
                                    placeholder={lng.phoneTxt}
                                    onChange={this.onChangeValue} />
                            </div>

                            
                            <div className="col-md-6">
                                <label htmlFor="email">{lng.titleTxt}</label>

                                <input type="text" 
                                    className="form-control"
                                    name="title"
                                    placeholder={lng.titleTxt}
                                    onChange={this.onChangeValue} />
                            </div>
                            
                            <div className="col-md-6">
                                <label htmlFor="email">{lng.emailTxt}</label>
                                <input type="text" 
                                className="form-control"
                                placeholder={lng.emailTxt}                                
                                onChange={this.onChangeValue} />
                            </div>
                            <div className="col-md-12">
                                <label htmlFor="review">{lng.msgTxt}</label>
                                <textarea 
                                className="form-control"
                        name="msg"
                        rows="6"
                        placeholder={lng.msgTxt}
                        onChange={this.onChangeValue}
                        ></textarea>

                            </div>
                            <div className="col-md-12">
                                
                                <button 
                                className="btn btn-solid" 
                                disabled={this.state.isDisabled}
                                type="submit">{lng.sendTxt}</button>
                            </div>
                        </div>
                    </form>
                
                </div>
            </div>
        </div>
    </section>
    



          </Fragment>

    )
    }
}



const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        cartItems: state.cartItems.cart,
        //userInfo: state.User.info
        //selectedCurr: state.Curr.slctdCurr,
        defaultSets: state.DfltSts
    }
}
export default connect(mapStateToProps, null)(Contact);