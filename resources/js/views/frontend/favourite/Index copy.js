import React, { Component, Fragment} from 'react'

import { connect } from 'react-redux';
import BreadCrumb from '../partials/BreadCrumb';
import {Helmet} from "react-helmet";
import {Link} from "react-router-dom";
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css
import deleteData from '../../../helpers/deleteData';
import getData from '../../../helpers/getData';
import {toast} from "react-toastify";

 class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            rows: []
        };
    }
  
   
    componentDidMount(){
        this.fetchData();
    }

    
   fetchData(){
    //const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

    getData(this.props.defaultSets.apiUrl+'user-favourites/'+this.props.userInfo.id, this.props.userInfo.accessToken)
        //.then(response => response.json())
        .then(response => {
            this.setState({ rows: response.data.rows });
            //toast.dismiss(ldToast)
            //console.log(response.data)
        })
        .catch(function (error) {
            console.log(error);
        })
   }

    deleteRow(id) {

        confirmAlert({
              
          customUI: ({ onClose }) => {
              return (
                  <div id="react-confirm-alert">
                      <div className="react-confirm-alert-overlay">
                          <div className="react-confirm-alert">
                              <div className="react-confirm-alert-body">
                                  <h1>{this.props.selectedLanguage.rUSureTxt}</h1>
                                  <div 
                                  style={{flexDirection: this.props.selectedLanguage.langCode == "AR" ? 'row-reverse': 'row'}}
                                  className="react-confirm-alert-button-group">
                                  <button
                                  className="yes"
                                  onClick={() => {
                                    this.props.removeFromFavourites(id)

                                      deleteData(this.props.defaultSets.apiUrl+'favourites/'+id, this.props.userInfo.accessToken)
                                      .then(res => {
                                          document.getElementById(id).remove(); 
           
                                      })
                                      .catch(err => {
                                          console.log(err);
                                      });
                                      onClose();
                                  }}
                                  >
                                          {this.props.selectedLanguage.yesTxt}
                                      </button>
  
                                      <button className="no" onClick={onClose}>
                                      {this.props.selectedLanguage.noTxt}
                                      </button>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
  
              );
            }
        });
    
  
    }

   
  
    render() {
        
        
        const {rows} = this.state;
        const lng = this.props.selectedLanguage;

        return (
           
         <Fragment>

<Helmet><title>{lng.myFavsTxt}</title></Helmet>

         <BreadCrumb title={lng.myFavsTxt} />



         <section className="wishlist-section section-b-space">
        <div className="container">
            <div className="row">
                <div className="col-sm-12">
                    <table className="table cart-table table-responsive-xs">
                        <thead>
                            <tr className="table-head">
                                <th>#</th>
                                <th scope="col">{lng.productTitleTxt}</th>
                                <th scope="col">{lng.priceTxt}</th>
                                <th scope="col">{lng.availabilityTxt}</th>
                                <th scope="col">{lng.cancelTxt}</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                        {rows && rows.map((item, i) => {
                                            return(
                                <tr key={'fav'+i} id={item.id}>
                                    <td>{i+1}</td>
                                    <td>
                                        
                                    <img src={"/uploads/files/"+item.product.photo} 
                                    alt={eval("item.product.title"+lng.langCode)} />
                                    </td>
                                    <td>{eval("item.product.title"+lng.langCode)}</td>
                           
                            
                                    <td>{item.product.priceAfterDiscount} YER</td>
                          
                                   
                                    <td>
                                    <button className="btn btn-success"
                                    onClick={(e) => { 
                                        e.preventDefault(); 
                                        this.props.addToCart(item.product, lng.addedToCartTxt);
                                    }}
                                    >{lng.addToCartTxt}</button>
                                        <a href="#"
                                        className="btn btn-outline-danger"
                                        onClick={(e) => {e.preventDefault(); this.deleteRow(item.id)}}>   
                                        <i className="fa fa-trash text-danger"></i>                                     
                                        </a>
                                    </td>
                               
                            </tr>
                               )
                            })}
                        </tbody>
                    </table>
                </div>
            
        </div>
        </div>
    </section>
         
              

         </Fragment>

        )
    }
}



const mapDispatchToProps = (dispatch) => {
    return {
        addToCart: (product, msg) => {
            toast.success(msg);
            dispatch({ type: 'ADD_TO_CART', payload: product })
        },
        removeFromFavourites: (itemId) => {
            dispatch({ type: 'REMOVE_FROM_FAV', payload: itemId });
        }
    }
}

const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        defaultSets: state.DfltSts,
        userInfo: state.User.info,
        favedItems: state.Favourites.rows
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Index);
