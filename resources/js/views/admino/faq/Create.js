import React, { Component, Fragment} from 'react'

import {toast} from 'react-toastify';
import { connect } from 'react-redux';
import ImgToBeUploaded from '../../../components/ImageToUpload';

import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';

import {Helmet} from "react-helmet";
import { Link } from 'react-router-dom';
import postData from '../../../helpers/postData';
 
 class Create extends Component {
    constructor(props) {
        super(props);
        this.state = {
          qAR: '',
          qEN: '',
          aAR: '',
          aEN: '',  
            status: 1,
            errorMessage: '',
            isDisabled: false
        };

        
        this.onChangeValue = this.onChangeValue.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }
  
    onChangeValue(e) {
      //console.log(e.target.value)
        this.setState({
            [e.target.name]: e.target.value, isDisabled: false
        });
    }
    handleSubmit(e){
//console.log('k'+this.state.status)
        e.preventDefault();
        this.setState({isDisabled: true});
        const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

        postData(this.props.defaultSets.apiUrl+'faqs', {

          qAR: this.state.qAR,
          qEN: this.state.qEN,
          aAR: this.state.aAR,
          aEN: this.state.aEN,    
            status: this.state.status,
            langCode: this.props.selectedLanguage.langCode
        })
            .then( (response) => {

                toast.update(ldToast, {
                    type: toast.TYPE.SUCCESS,
                    render: response.data.msg
                });

               /*  setTimeout( () => {
                    this.props.history.push('/faqs');
                }, 3000); */
            })
            .catch( (error) => {
                //console.log(error.response);
                this.setState({  isDisabled: false})

                if(error.response !== undefined && error.response.status === 422){
                    let errorTxt = '';
                    let errorMessage = error.response.data.errors;

                    Object.keys(errorMessage).map((key, i) => {
                        errorTxt = (<span> {errorTxt} {errorMessage[key][0]} <br /></span>);});
                    toast.update(ldToast, {
                        type: toast.TYPE.ERROR,
                        render: errorTxt
                    });
                }
            });

    }


    render() {
        
       
      const { qTxt, faqsTxt, crtNewTxt,
        statusTxt, saveTxt, aTxt, activeTxt, inActiveTxt
      } = this.props.selectedLanguage;

        return (
                           
        
      <div className="row">
            <Helmet><title>{faqsTxt}</title></Helmet>

                  
          <div className="col-md-12">
                  
                  <nav aria-label="breadcrumb">
                    <ol className="breadcrumb">
                      <li className="breadcrumb-item"><Link to="/admin"><i className="fa fa-home"></i></Link></li>
                      <li className="breadcrumb-item"><Link to="/admin/faqs">{faqsTxt}</Link></li>
                      <li className="breadcrumb-item active" aria-current="page">{crtNewTxt}</li>
                    </ol>
                  </nav>
                </div>


            <div className="col-md-8">
              <div className="card">
                <div className="card-header card-header-primary">
                  <h4 className="card-title mb-3">{crtNewTxt}</h4>
                  <p className="card-category"></p>
                </div>
                <div className="card-body">
                  <form>
                    <div className="row">
                      <div className="col-md-6">
                        <div className="form-group">
                          <label className="bmd-label-floating">{qTxt} AR</label>                          
                          <input 
                          className="form-control"
                          type="text"
                          name="qAR"
                          onChange={this.onChangeValue} />
                        </div>
                      </div>

                      <div className="col-md-6">
                        <div className="form-group">
                          <label className="bmd-label-floating">{qTxt} EN</label>                            
                          <input 
                          className="form-control"
                          type="text"
                          name="qEN"
                          onChange={this.onChangeValue} />
                        </div>
                      </div>
                    </div>
                 

                    <div className="row">

                      <div className="col-md-12">
                        <div className="form-group">
                          <label className="bmd-label-floating">{statusTxt}</label>                     
                          <select 
                          name="status"
                          className="form-control"
                          onChange={this.onChangeValue}>
                            <option value="1">{activeTxt}</option>
                            <option value="0">{inActiveTxt}</option>
                          </select>
                        </div>
                      </div>

                    </div>

                    <div className="row">
                      <div className="col-md-12">
                        <div className="form-group">
                          <label></label>
                          <div className="form-group">
                            <label className="bmd-label-floating">{aTxt} AR</label>
                            <CKEditor
                          editor={ ClassicEditor }
                          data=''
                          onChange={ ( event, editor ) => {
                              const data = editor.getData();
                              this.setState({aAR: data})
                              //console.log( { event, editor, data } );
                          }}
                          />
                          </div>
                        </div>
                      </div>
                    </div>
                    
                    <div className="row">
                      <div className="col-md-12">
                        <div className="form-group">
                          <label></label>
                          <div className="form-group">
                            <label className="bmd-label-floating">{aTxt} EN</label>
                            <CKEditor
                          editor={ ClassicEditor }
                          data=''
                          onChange={ ( event, editor ) => {
                              const data = editor.getData();
                              this.setState({aEN: data})
                              //console.log( { event, editor, data } );
                          }}
                          />
                          </div>
                        </div>
                      </div>
                    </div>

                    <button 
                    type="submit" 
                    onClick={this.handleSubmit}
                    disabled={this.state.isDisabled}
                    className="btn btn-primary pull-right">{saveTxt}
                    </button>
                    <div className="clearfix"></div>
                  </form>
                </div>
              </div>
            </div>
            

          </div>
        
        

        )
    }
}


const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        defaultSets: state.DfltSts
    }
}
export default connect(mapStateToProps, null)(Create);
