import React, { Component, Fragment} from 'react'

import {toast} from 'react-toastify';
import { connect } from 'react-redux';

import { Link } from 'react-router-dom';
import {Helmet} from "react-helmet";

import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import getData from '../../../helpers/getData';
import putData from '../../../helpers/putData';

 class Edit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id:0,
            qAR: '',
            qEN: '',
            aAR: '',
            aEN: '',
            status: 1,
            errorMessage: '',
            isDisabled: false,

        };

        
        this.onChangeValue = this.onChangeValue.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }
  
     
  
    componentDidMount(){

      this.fetchData(this.props.match.params.id);    
 }

 fetchData(elmntId) {
  getData(this.props.defaultSets.apiUrl+'faqs/'+elmntId+'/edit')

       //.then(response => response.json())
       .then(response => {

         this.setState({ 
             id: elmntId,
             qAR: response.data.qAR,
             qEN: response.data.qEN,
             aAR: response.data.aAR || '',
             aEN: response.data.aEN || '',             
             status: response.data.status, 
             isDisabled: false,            
           });
           //console.log(response.data)
       })
       .catch(function (error) {
           console.log(error);
       });
 }


    onChangeValue(e) {
      //console.log(e.target.value)
        this.setState({
            [e.target.name]: e.target.value
        });
    }
    
    handleSubmit(e){
//console.log('k'+this.state.status)
        e.preventDefault();
        this.setState({isDisabled: true});
        const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

          putData(this.props.defaultSets.apiUrl+'faqs/'+this.state.id, {
            id: this.state.id,
            qAR: this.state.qAR,
            qEN: this.state.qEN,
            aAR: this.state.aAR,
            aEN: this.state.aEN,            
            status: this.state.status,
            langCode: this.props.selectedLanguage.langCode
        })
            .then( (response) => {
                toast.update(ldToast, {
                    type: toast.TYPE.SUCCESS,
                    render: response.data.msg
                });

               /*  setTimeout( () => {
                    this.props.history.push('/faqs');
                }, 3000); */
            })
            .catch( (error) => {
                //console.log(error.response);
                this.setState({  isDisabled: false})

                if(error.response !== undefined && error.response.status === 422){
                    let errorTxt = '';
                    let errorMessage = error.response.data.errors;

                    Object.keys(errorMessage).map((key, i) => {
                        errorTxt = (<span> {errorTxt} {errorMessage[key][0]} <br /></span>);});
                    toast.update(ldToast, {
                        type: toast.TYPE.ERROR,
                        render: errorTxt
                    });
                }
            });

    }


    render() {
        
        
        const {isDisabled, qAR, qEN, aAR, aEN, status,  
         } = this.state;

        const { qTxt, faqsTxt, updtFaqTxt,
           statusTxt, saveTxt, aTxt, activeTxt, inActiveTxt
         } = this.props.selectedLanguage;

        return (
                   
          
          <div className="row">
            <Helmet><title>{updtFaqTxt}</title></Helmet>

          <div className="col-md-12">
                  
            <nav aria-label="breadcrumb">
              <ol className="breadcrumb">
                <li className="breadcrumb-item"><Link to="/admin"><i className="fa fa-home"></i></Link></li>
                <li className="breadcrumb-item"><Link to="/admin/faqs">{faqsTxt}</Link></li>
                <li className="breadcrumb-item active" aria-current="page">{updtFaqTxt}</li>
              </ol>
            </nav>
          </div>
          
          <div className="col-md-8">
            <div className="card">
              <div className="card-header card-header-primary">
                <h4 className="card-title">{updtFaqTxt}</h4>
                <p className="card-category"></p>
              </div>
              <div className="card-body">
                <form>
                  <div className="row">
                    <div className="col-md-6">
                      <div className="form-group">
                        <label className="bmd-label-floating">{qTxt} AR</label>                          
                        <input 
                        className="form-control"
                        value={qAR}
                        type="text"
                        name="qAR"
                        onChange={this.onChangeValue} />
                      </div>
                    </div>

                    <div className="col-md-6">
                      <div className="form-group">
                        <label className="bmd-label-floating">{qTxt} EN</label>                            
                        <input 
                        value={qEN}
                        className="form-control"
                        type="text"
                        name="qEN"
                        onChange={this.onChangeValue} />
                      </div>
                    </div>
                  </div>
               

                  <div className="row">

                    <div className="col-md-12">
                      <div className="form-group">
                        <label className="bmd-label-floating">{statusTxt}</label>                     
                        <select 
                        value={status}
                        name="status"
                        className="form-control"
                        onChange={this.onChangeValue}>
                          <option value="1">{activeTxt}</option>
                          <option value="0">{inActiveTxt}</option>
                        </select>
                      </div>
                    </div>

                  </div>

                  <div className="row">
                    <div className="col-md-12">
                      <div className="form-group">
                        <label></label>
                        <div className="form-group">
                          <label className="bmd-label-floating">{aTxt} AR</label>
                          <CKEditor
                          editor={ ClassicEditor }
                          data={aAR}
                          onChange={ ( event, editor ) => {
                              const data = editor.getData();
                              this.setState({aAR: data})
                              //console.log( { event, editor, data } );
                          }}
                          />
                        </div>
                      </div>
                    </div>
                  </div>                  
                  
                  <div className="row">
                    <div className="col-md-12">
                      <div className="form-group">
                        <label></label>
                        <div className="form-group">
                          <label className="bmd-label-floating">{aTxt} EN</label>
                          <CKEditor
                          editor={ ClassicEditor }
                          data={aEN}
                          onChange={ ( event, editor ) => {
                              const data = editor.getData();
                              this.setState({aEN: data})
                              //console.log( { event, editor, data } );
                          }}
                          />
                        </div>
                      </div>
                    </div>
                  </div>

                  <button 
                  type="submit" 
                  onClick={this.handleSubmit}
                  disabled={isDisabled}
                  className="btn btn-primary pull-right">{saveTxt}
                  </button>
                  <div className="clearfix"></div>
                </form>
              </div>
            </div>
          </div>
      
        </div>
      
        )
    }
}


const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        defaultSets: state.DfltSts
    }
}
export default connect(mapStateToProps, null)(Edit);
