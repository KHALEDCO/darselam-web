import React, { Component, Fragment} from 'react'

import {toast} from 'react-toastify';
import { connect } from 'react-redux';


import ImgToBeUploaded from '../../../components/ImageToUpload';
import RowThumbnail from '../../../components/RowThumbnail';

import { Link } from 'react-router-dom';
import {Helmet} from "react-helmet";

import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import getData from '../../../helpers/getData';
import putData from '../../../helpers/putData';
import { checkMimeType } from '../../../helpers/checkMimeType';

 class Edit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id:1,
            titleAR: '',
            //titleTR: '',
            titleEN: '',/* 
            descriptionAR: '',
            descriptionTR: '',
            descriptionEN: '', */
            phone1: '',
            phone2: '',
            email1: '',
            email2: '',

            adrsAR: '',
            //adrsTR: '',
            adrsEN: '',

            twitter_url: '',
            fb_url: '',
            google_url: '',
            instagram: '',
            snapchat: '',
            whatsapp: '',

            copyRightsAR: '',
            //copyRightsTR: '',
            copyRightsEN: '',

            welcomeMsgAR: '',
            //welcomeMsgTR: '',
            welcomeMsgEN: '',
            
            errorMessage: '',
            isDisabled: false,
            notificationWay: 0,

            // here i load original images of an element that comes from db.
            photo: '',
            //.
            
            // here the new selected images will be loaded.
            mainImg: null,            
            //.
        };

        
        this.onChangeValue = this.onChangeValue.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.fetchData = this.fetchData.bind(this);

        //the next functions handle images on selected, change and upload.
        this.onMainImageChange = this.onMainImageChange.bind(this);
        this.rmvSlctdMnImg = this.rmvSlctdMnImg.bind(this);        
        this.hndlUpldMnImg = this.hndlUpldMnImg.bind(this);        
        this.rmvMnImg = this.rmvMnImg.bind(this);      
        //.
    }
  
     
  
    componentDidMount(){

      this.fetchData();    
      //console.log('khaled', this.state.titleAR)                
 }

 fetchData() {

  getData(this.props.defaultSets.apiUrl+'settings')

       //.then(response => response.json())
       .then(response => {

         this.setState({ 
             //id: elmntId,
             titleAR: response.data.titleAR,
             //titleTR: response.data.titleTR,
             titleEN: response.data.titleEN,
             
             phone1: response.data.phone1,
             phone2: response.data.phone2,

             email1: response.data.email1,
             email2: response.data.email2,
             
             adrsAR: response.data.adrsAR,
             //adrsTR: response.data.adrsTR,
             adrsEN: response.data.adrsEN,
             
             welcomeMsgAR: response.data.welcomeMsgAR,
             //welcomeMsgTR: response.data.welcomeMsgTR,
             welcomeMsgEN: response.data.welcomeMsgEN,

             fb_url: response.data.fb_url,
             twitter_url: response.data.twitter_url,
             google_url: response.data.google_url,
             whatsapp: response.data.whatsapp,
             instagram: response.data.instagram,
             snapchat: response.data.snapchat,

             copyRightsAR: response.data.copyRightsAR,
             //copyRightsTR: response.data.copyRightsTR,
             copyRightsEN: response.data.copyRightsEN,
             /* 
             descriptionAR: response.data.descriptionAR || '',
             descriptionTR: response.data.descriptionTR || '',
             descriptionEN: response.data.descriptionEN || '', */
             
             notificationWay: 0,
             photo: response.data.photo,
             
           });
           //console.log(response.data)
       })
       .catch(function (error) {
           console.log(error);
       });
 }



 onMainImageChange(event) {
  if (event.target.files && event.target.files[0]) {

    let img = event.target.files[0];

    let mimeTypeCheck = checkMimeType(event, this.props.selectedLanguage.notAllowedTxt);
    if(mimeTypeCheck == true){

      return this.setState({
        mainImg: img 
      }); 
    } 
    toast.error(mimeTypeCheck);
      
  }
};
    // The next function is used inside handleSubmit function not used directrly anywhere 
    // so after row have been inserted it uploads its photo.
    
    hndlUpldMnImg(rowId) {
      if (this.state.mainImg == null ) {
        return false;
      }

      // then prepare a form to send files.
      const formData = new FormData()
      
      // The next two lines go to FilesController to specify the row and tbl where inserted.
      formData.append('rowId', rowId);
      formData.append('type', 'setting');
      formData.append('mainPhoto', '1');
      //.

      
      // append the main photo to formData.
      formData.append(
        'files[]',
        this.state.mainImg,
        this.state.mainImg.name
      )
      //.

      fetch(this.props.defaultSets.apiUrl+'upload-files', {
        method: 'POST',
        body: formData
      })
      .then(response => response.json())
      .then(response => {
        this.setState({
          mainImg: null,
        });
        /* onUploadProgress: progressEvent => {
          console.log(progressEvent.loaded / progressEvent.total)
        } */
      })
    }

 rmvSlctdMnImg() { // handle selected photo not main photo that comes from db.
  this.setState({
    mainImg: null
  })
}

rmvMnImg() { //handle main photo that comes from db not a selected one.
  this.setState({
    photo: null
  });
}

    onChangeValue(e) {
      //console.log(e.target.value)
        this.setState({
            [e.target.name]: e.target.value, isDisabled: false
        });
    }
    
    handleSubmit(e){
//console.log('k'+this.state.status)
        e.preventDefault();
        this.setState({isDisabled: true});
        const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

          putData(this.props.defaultSets.apiUrl+'settings/', {
            //id: this.state.id,
            titleAR: this.state.titleAR,
            //titleTR: this.state.titleTR,
            titleEN: this.state.titleEN,
            
            phone1: this.state.phone1,
            phone2: this.state.phone2,

            email1: this.state.email1,
            email2: this.state.email2,

            adrsAR: this.state.adrsAR,
            //adrsTR: this.state.adrsTR,
            adrsEN: this.state.adrsEN,

            welcomeMsgAR: this.state.welcomeMsgAR,
            //welcomeMsgTR: this.state.welcomeMsgTR,
            welcomeMsgEN: this.state.welcomeMsgEN,

            fb_url: this.state.fb_url,
            twitter_url: this.state.twitter_url,
            google_url: this.state.google_url,
            whatsapp: this.state.whatsapp,
            instagram: this.state.instagram,
            snapchat: this.state.snapchat,

            copyRightsAR: this.state.copyRightsAR,
            //copyRightsTR: this.state.copyRightsTR,
            copyRightsEN: this.state.copyRightsEN,
            notificationWay: this.state.notificationWay,
            photo: this.state.mainImg || this.state.photo,
            langCode: this.props.selectedLanguage.langCode
        })
            .then( (response) => {
              this.hndlUpldMnImg(this.state.id);

                toast.update(ldToast, {
                    type: toast.TYPE.SUCCESS,
                    render: response.data.msg
                });
                //this.setState({  isDisabled: false})

               /*  setTimeout( () => {
                    this.props.history.push('/articles');
                }, 3000); */
            })
            .catch( (error) => {
                //console.log(error.response);
                this.setState({  isDisabled: false})

                if(error.response !== undefined && error.response.status === 422){
                    let errorTxt = '';
                    let errorMessage = error.response.data.errors;

                    Object.keys(errorMessage).map((key, i) => {
                        errorTxt = (<span> {errorTxt} {errorMessage[key][0]} <br /></span>);});
                    toast.update(ldToast, {
                        type: toast.TYPE.ERROR,
                        render: errorTxt
                    });
                }
            });

    }


    render() {
        
        
        const {errorMessage, isDisabled,titleAR, titleEN, notificationWay, /* titleTR, */ 
          phone1, phone2, email1, email2, welcomeMsgAR, welcomeMsgEN, welcomeMsgTR, 
          adrsAR, adrsEN, /* adrsTR, */ copyRightsAR, copyRightsEN, /* copyRightsTR, */ fb_url, 
          google_url, twitter_url, instagram, snapchat, whatsapp , photo,  mainImg } = this.state;

        const { titleTxt, adrsTxt, welcomeMsgTxt, emailTxt, phoneTxt, notificationWayTxt,
          copyRightsTxt, fbTxt, twitterTxt, googleTxt, instagramTxt, snapchatTxt,
          whatsappTxt, mainPhotoTxt, saveTxt, updtSettingsTxt, bySmsEmailTxt, bySmsTxt, byEmailTxt
         } = this.props.selectedLanguage;

        return (
                   
          
          <div className="row">

<Helmet><title>{updtSettingsTxt}</title></Helmet>

<div className="col-md-12">
        
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item"><Link to="/admin"><i className="fa fa-home"></i></Link></li>
            <li className="breadcrumb-item active" aria-current="page">{updtSettingsTxt}</li>
          </ol>
        </nav>
      </div>

          <div className="col-md-8">
            <div className="card">
              <div className="card-header card-header-primary">
                <h4 className="card-title">{updtSettingsTxt}</h4>
                <p className="card-category"></p>
              </div>
              <div className="card-body">
                <form>
                  <div className="row">
                    <div className="col-md-4">
                      <div className="form-group">
                        <label className="bmd-label-floating">{titleTxt} AR</label>                          
                        <input 
                        className="form-control"
                        value={titleAR || ''}
                        type="text"
                        name="titleAR"
                        onChange={this.onChangeValue} />
                      </div>
                    </div>

                   {/*  <div className="col-md-4">
                      <div className="form-group">
                        <label className="bmd-label-floating">{titleTxt} TR</label>                            
                        <input 
                        value={titleTR || ''}
                        className="form-control"
                        type="text"
                        name="titleTR"
                        onChange={this.onChangeValue} />
                      </div>
                    </div> */}

                    <div className="col-md-4">
                      <div className="form-group">
                        <label className="bmd-label-floating">{titleTxt} EN</label>                            
                        <input 
                        value={titleEN || ''}
                        className="form-control"
                        type="text"
                        name="titleEN"
                        onChange={this.onChangeValue} />
                      </div>
                    </div>
                  </div>
               
                  <div className="row">
                    <div className="col-md-6">
                      <div className="form-group">
                        <label className="bmd-label-floating">{phoneTxt} 1</label>                          
                        <input 
                        className="form-control"
                        value={phone1 || ''}
                        type="text"
                        name="phone1"
                        onChange={this.onChangeValue} />
                      </div>
                    </div>

                    <div className="col-md-6">
                      <div className="form-group">
                        <label className="bmd-label-floating">{phoneTxt} 2</label>                            
                        <input 
                        value={phone2}
                        className="form-control"
                        type="text"
                        name="phone2"
                        onChange={this.onChangeValue} />
                      </div>
                    </div>

                  </div>

                  <div className="row">
                    <div className="col-md-6">
                      <div className="form-group">
                        <label className="bmd-label-floating">{emailTxt} 1</label>                          
                        <input 
                        className="form-control"
                        value={email1 || ''}
                        type="text"
                        name="email1"
                        onChange={this.onChangeValue} />
                      </div>
                    </div>

                    <div className="col-md-6">
                      <div className="form-group">
                        <label className="bmd-label-floating">{emailTxt} 2</label>                            
                        <input 
                        value={email2 || ''}
                        className="form-control"
                        type="text"
                        name="email2"
                        onChange={this.onChangeValue} />
                      </div>
                    </div>

                  </div>
               
               
                  <div className="row">
                    <div className="col-md-6">
                      <div className="form-group">
                        <label className="bmd-label-floating">{copyRightsTxt} AR</label>                          
                        <input 
                        className="form-control"
                        value={copyRightsAR || ''}
                        type="text"
                        name="copyRightsAR"
                        onChange={this.onChangeValue} />
                      </div>
                    </div>

                    
                    
                    <div className="col-md-6">
                      <div className="form-group">
                        <label className="bmd-label-floating">{adrsTxt} AR</label>                            
                        <input 
                        value={adrsAR || ''}
                        className="form-control"
                        type="text"
                        name="adrsAR"
                        onChange={this.onChangeValue} />
                      </div>
                    </div>
               

                  </div>
                  
                  <div className="row">

                    <div className="col-md-6">
                      <div className="form-group">
                        <label className="bmd-label-floating">{copyRightsTxt} EN</label>                            
                        <input 
                        value={copyRightsEN || ''}
                        className="form-control"
                        type="text"
                        name="copyRightsEN"
                        onChange={this.onChangeValue} />
                      </div>
                    </div>
                    
                  
                    <div className="col-md-6">
                      <div className="form-group">
                        <label className="bmd-label-floating">{adrsTxt} EN</label>                            
                        <input 
                        value={adrsEN || ''}
                        className="form-control"
                        type="text"
                        name="adrsEN"
                        onChange={this.onChangeValue} />
                      </div>
                    </div>
                  </div>

                  <div className="row">

               {/*  <div className="col-md-6">
                      <div className="form-group">
                        <label className="bmd-label-floating">{copyRightsTxt} TR</label>                            
                        <input 
                        value={copyRightsTR || ''}
                        className="form-control"
                        type="text"
                        name="copyRightsTR"
                        onChange={this.onChangeValue} />
                      </div>
                    </div> */}
 
 
                   {/*  <div className="col-md-6">
                      <div className="form-group">
                        <label className="bmd-label-floating">{adrsTxt} TR</label>                            
                        <input 
                        value={adrsTR || ''}
                        className="form-control"
                        type="text"
                        name="adrsTR"
                        onChange={this.onChangeValue} />
                      </div>
                    </div>
*/}
                  </div> 

                
                  <div className="row">
                    <div className="col-md-4">
                      <div className="form-group">
                        <label className="bmd-label-floating">{fbTxt}</label>                          
                        <input 
                        className="form-control"
                        value={fb_url || ''}
                        type="text"
                        name="fb_url"
                        onChange={this.onChangeValue} />
                      </div>
                    </div>

                    <div className="col-md-4">
                      <div className="form-group">
                        <label className="bmd-label-floating">{twitterTxt}</label>                            
                        <input 
                        value={twitter_url || ''}
                        className="form-control"
                        type="text"
                        name="twitter_url"
                        onChange={this.onChangeValue} />
                      </div>
                    </div>

                    <div className="col-md-4">
                      <div className="form-group">
                        <label className="bmd-label-floating">{whatsappTxt}</label>                            
                        <input 
                        value={whatsapp || ''}
                        className="form-control"
                        type="text"
                        name="whatsapp"
                        onChange={this.onChangeValue} />
                      </div>
                    </div>
                  </div>
               
                  <div className="row">
                    <div className="col-md-4">
                      <div className="form-group">
                        <label className="bmd-label-floating">{instagramTxt}</label>                          
                        <input 
                        className="form-control"
                        value={instagram || ''}
                        type="text"
                        name="instagram"
                        onChange={this.onChangeValue} />
                      </div>
                    </div>

                    <div className="col-md-4">
                      <div className="form-group">
                        <label className="bmd-label-floating">{snapchatTxt}</label>                            
                        <input 
                        value={snapchat || ''}
                        className="form-control"
                        type="text"
                        name="snapchat"
                        onChange={this.onChangeValue} />
                      </div>
                    </div>

                    <div className="col-md-4">
                      <div className="form-group">
                        <label className="bmd-label-floating">{googleTxt}</label>                            
                        <input 
                        value={google_url || ''}
                        className="form-control"
                        type="text"
                        name="google_url"
                        onChange={this.onChangeValue} />
                      </div>
                    </div>
                  </div>
               

                  <div className="row">
                    <div className="col-md-12">
                    <label className="bmd-label-floating">{notificationWayTxt}</label>                            

                      <select 
                      className="form-control"
                      name="notificationWay"
                      value={notificationWay}
                      onChange={this.onChangeValue}>
                        <option value="1">{bySmsTxt}</option>
                        <option value="2">{byEmailTxt}</option>
                        <option value="3">{bySmsEmailTxt}</option>
                      </select>
                    </div>
                  </div>

                  <div className="row">
                    <div className="col-md-12">
                      <div className="form-group">
                        <label></label>
                        <div className="form-group">
                          <label className="bmd-label-floating">{welcomeMsgTxt} AR</label>
                          <textarea 
                          name="welcomeMsgAR" 
                          className="form-control"
                          value={welcomeMsgAR || ''}
                          onChange={this.onChangeValue}>

                          </textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                  
                  {/* <div className="row">
                    <div className="col-md-12">
                      <div className="form-group">
                        <label></label>
                        <div className="form-group">
                          <label className="bmd-label-floating">{welcomeMsgTxt} TR</label>
                          <CKEditor
                          editor={ ClassicEditor }
                          data={welcomeMsgTR || ''}
                          onChange={ ( event, editor ) => {
                              const data = editor.getData();
                              this.setState({welcomeMsgTR: data})
                              //console.log( { event, editor, data } );
                          }}
                          />
                        </div>
                      </div>
                    </div>
                  </div> */}
                  
                  <div className="row">
                    <div className="col-md-12">
                      <div className="form-group">
                        <label></label>
                        <div className="form-group">
                          <label className="bmd-label-floating">{welcomeMsgTxt} EN</label>
                          {/* <CKEditor
                          editor={ ClassicEditor }
                          data={welcomeMsgEN || ''}
                          onChange={ ( event, editor ) => {
                              const data = editor.getData();
                              this.setState({welcomeMsgEN: data, isDisabled: false});
                              //console.log( { event, editor, data } );
                          }}
                          /> */}
                          <textarea 
                          name="welcomeMsgEN" 
                          className="form-control"
                          value={welcomeMsgEN || ''}
                          onChange={this.onChangeValue}>
                          </textarea>
                        </div>
                      </div>
                    </div>
                  </div>

                  <button 
                  type="submit" 
                  onClick={this.handleSubmit}
                  disabled={isDisabled}
                  className="btn btn-primary pull-right">{saveTxt}
                  </button>
                  <div className="clearfix"></div>
                </form>
              </div>
            </div>
          </div>
          

              
        <div className="col-md-4">
         

         <div className="col-md-12">
            
         <div className="card card-profile">
               <div className="card-avatar">.</div>
               <div className="card-body">
                 <h6 className="card-category text-gray"></h6>
                 { <h4 className="card-title">{mainPhotoTxt}</h4>}
                 
                 <div className="card-description">
                   <RowThumbnail imgName={photo} rmvMnImg={this.rmvMnImg} />
                    
                   <ImgToBeUploaded 
                   image={mainImg} 
                   rmvSlctdMnImg={this.rmvSlctdMnImg} />
                   
                   <input type="file" 
                   accept=".gif,.jpg,.jpeg,.png"
                   onChange={this.onMainImageChange} />
                 </div>
                 
                 {/* <button 
                 onClick={this.handleUpload}
                 className="btn btn-primary btn-round">
                   Upload
                 </button> */}
               </div>
             </div>
          
         </div>
       
      
       </div>


        </div>
      
        )
    }
}


const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        defaultSets: state.DfltSts
    }
}
export default connect(mapStateToProps, null)(Edit);
