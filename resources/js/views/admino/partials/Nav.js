import React, { Component} from 'react'

import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { ar } from '../../../lang/ar';
import { en } from '../../../lang/en';

 class Nav extends Component {
    constructor(props) {
        super(props);
        this.state = {
            
        };
    }
 
    render() {
        
        const { cpanelTxt} = this.props.selectedLanguage;
        //const {mainUrl} = this.props.defaultSets;

        return (
            
  
            <nav className="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
            <div className="container-fluid">
              <div className="navbar-wrapper">
                
                <Link to="/admin" className="navbar-brand">{cpanelTxt}</Link>
              </div>
              <button className="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                <span className="sr-only">Toggle navigation</span>
                <span className="navbar-toggler-icon icon-bar"></span>
                <span className="navbar-toggler-icon icon-bar"></span>
                <span className="navbar-toggler-icon icon-bar"></span>
              </button>
              <div className="collapse navbar-collapse justify-content-end">
                <form className="navbar-form">
                  <div className="input-group no-border">
                   
                  </div>
                </form>
                <ul className="navbar-nav">
                        
              
                  <li className="nav-item">
                    <a className="nav-link" onClick={()=> this.props.selectLanguage(ar)}>
                    <img src={'/assets/custom-img/ye-24.png'} alt="Arabic" />
                      <p className="d-lg-none d-md-block">
                        
                      </p>
                    </a>
                  </li>
        
                  <li className="nav-item">
                    <a className="nav-link" onClick={()=> this.props.selectLanguage(en)}>
                    <img src={'/assets/custom-img/uk-24.png'} alt="English" />
                      <p className="d-lg-none d-md-block">
                        
                      </p>
                    </a>
                  </li>
        
                  <li className="nav-item">
                    <Link className="nav-link" to="/admin">
                      <i className="fa fa-dashboard"></i>
                      
                    </Link>
                  </li>
        
        
                  <li className="nav-item dropdown">
                    <a className="nav-link" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i className="fa fa-bell"></i>
                      <span className="notification">5</span>
                      <p className="d-lg-none d-md-block">
                        Some Actions
                      </p>
                    </a>
                    <div className="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                      <a className="dropdown-item" href="#">Mike John responded to your email</a>
                      <a className="dropdown-item" href="#">You have 5 new tasks</a>
                      <a className="dropdown-item" href="#">You're now friend with Andrew</a>
                      <a className="dropdown-item" href="#">Another Notification</a>
                      <a className="dropdown-item" href="#">Another One</a>
                    </div>
                  </li>
                  <li className="nav-item dropdown">
                    <a className="nav-link" href="#" 
                    onClick={() => this.props.logout()}
                    id="navbarDropdownProfile" 
                    data-toggle="dropdown" 
                    aria-haspopup="true" aria-expanded="false">
                      <i className="fa fa-sign-out"></i>
                      <p className="d-lg-none d-md-block">
                        Account
                      </p>
                    </a>
                    <div className="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                      <a className="dropdown-item" href="#">Profile</a>
                      <a className="dropdown-item" href="#">Settings</a>
                      <div className="dropdown-divider"></div>
                      <a className="dropdown-item" href="#">Log out</a>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </nav>
        

        )
    }
}

const mapDispatchToProps = (dispatch) => {
  return {
      selectLanguage: (val) => dispatch({ type: 'language_data', payload: val }),
      //changeCurrency: (curr) => dispatch({ type: 'change_currency', payload: curr }),
  }
};

export default connect(null, mapDispatchToProps)(Nav);
