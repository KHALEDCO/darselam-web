import React, { Component} from 'react'

import { Link } from 'react-router-dom';

 class Sidebar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            
        };
    }
 
    render() { 
        
      const {userInfo}= this.props;
        const { homeTxt, usersTxt, productsTxt, categoriesTxt, settingsTxt,
          articlesTxt, cpagesTxt, sizesTxt, servicesTxt, derasaTxt, inboxTxt, ordersTxt
        , couponsTxt, rolesTxt, faqsTxt, custommsgsTxt} = this.props.selectedLanguage;

        return (
            
                           
    <div className="sidebar" data-color="purple" data-background-color="white" 
    data-image="/assets/admin/img/sidebar-1.jpg">
      
      
      <div className="logo"><Link to="/" className="simple-text logo-normal">
          {derasaTxt}
        </Link></div>
      <div className="sidebar-wrapper">
        <ul className="nav">
          <li className="nav-item active  ">
            <Link to="/admin" className="nav-link">                                
              <i className="fa fa-laptop"></i>
              <p className="text-white">{homeTxt}</p>                            
            </Link>            
          </li>

          {(userInfo.userPermissions.length > 0 && userInfo.userPermissions.includes("AllOrders")) || (userInfo.super == 1) ?
          <li className="nav-item ">
            <Link to="/admin/orders" className="nav-link">                                
              <i className="fa fa-shopping-cart"></i>
              <p>{ordersTxt}</p>                            
            </Link>
          </li>
          : null}

          {(userInfo.userPermissions.length > 0 && userInfo.userPermissions.includes("AllUsers")) || (userInfo.super == 1) ?
          <li className="nav-item ">
            <Link to="/admin/users" className="nav-link">                                
              <i className="fa fa-user"></i>
              <p>{usersTxt}</p>                            
            </Link>
          </li>
          : null}

          {(userInfo.userPermissions.length > 0 && userInfo.userPermissions.includes("AllProducts")) || (userInfo.super == 1) ?
          <li className="nav-item ">
            <Link to="/admin/products" className="nav-link">                                
              <i className="fa fa-window-restore"></i>
              <p>{productsTxt}</p>                            
            </Link>
          </li>
          : null}

{(userInfo.userPermissions.length > 0 && userInfo.userPermissions.includes("AllTabs")) || (userInfo.super == 1) ?
          <li className="nav-item ">
            <Link to="/admin/tabs" className="nav-link">                                
              <i className="fa fa-window-restore"></i>
              <p>{lng.tabsTxt}</p>                            
            </Link>
          </li>
          : null}
          
          {(userInfo.userPermissions.length > 0 && userInfo.userPermissions.includes("AllCategories")) || (userInfo.super == 1) ?
          <li className="nav-item ">
            <Link to="/admin/categories" className="nav-link">                                
              <i className="fa fa-book"></i>
              <p>{categoriesTxt}</p>                            
            </Link>
          </li>
          : null}

          {(userInfo.userPermissions.length > 0 && userInfo.userPermissions.includes("AllRoles")) || (userInfo.super == 1) ?
          <li className="nav-item ">
            <Link to="/admin/roles" className="nav-link">                                
              <i className="fa fa-group"></i>
              <p>{rolesTxt}</p>                            
            </Link>
          </li>
          : null}


          {/* {(userInfo.userPermissions.length > 0 && userInfo.userPermissions.includes("AllColours")) || (userInfo.super == 1) ?
          

          <li className="nav-item ">
            <Link to="/admin/colours" className="nav-link">                                
              <i className="fa fa-book"></i>
              <p>{coloursTxt}</p>                            
            </Link>
          </li>
          : null} */}


          {/* {(userInfo.userPermissions.length > 0 && userInfo.userPermissions.includes("AllSizes")) || (userInfo.super == 1) ?

          <li className="nav-item ">
            <Link to="/admin/sizes" className="nav-link">                                
              <i className="fa fa-book"></i>
              <p>{sizesTxt}</p>                            
            </Link>
          </li>
          : null} */}

          
          {(userInfo.userPermissions.length > 0 && userInfo.userPermissions.includes("AllCoupons")) || (userInfo.super == 1) ?

          <li className="nav-item ">
            <Link to="/admin/coupons" className="nav-link">                                
              <i className="fa fa-file"></i>
              <p>{couponsTxt}</p>                            
            </Link>
          </li>
          : null}

          
          {(userInfo.userPermissions.length > 0 && userInfo.userPermissions.includes("AllArticles")) || (userInfo.super == 1) ?

          <li className="nav-item ">
            <Link to="/admin/articles" className="nav-link">                                
              <i className="fa fa-file"></i>
              <p>{articlesTxt}</p>                            
            </Link>
          </li>
          : null}
          <li className="nav-item ">
            <Link to="/admin/services" className="nav-link">                                
              <i className="fa fa-briefcase"></i>
              <p>{servicesTxt}</p>                            
            </Link>
          </li>
          
          {(userInfo.userPermissions.length > 0 && userInfo.userPermissions.includes("AllCpages")) || (userInfo.super == 1) ?

          <li className="nav-item ">
            <Link to="/admin/cpages" className="nav-link">                                
              <i className="fa fa-file"></i>
              <p>{cpagesTxt}</p>                            
            </Link>
          </li>
          : null}
          
          {(userInfo.userPermissions.length > 0 && userInfo.userPermissions.includes("AllFaqs")) || (userInfo.super == 1) ?

          <li className="nav-item ">
          <Link to="/admin/faqs" className="nav-link">                                
              <i className="fa fa-envelope-o"></i>
              <p>{faqsTxt}</p>                            
            </Link>
          </li>
          : null}

          
          {(userInfo.userPermissions.length > 0 && userInfo.userPermissions.includes("AllCustommsgs")) || (userInfo.super == 1) ?

          <li className="nav-item ">
            <Link to="/admin/custommsgs" className="nav-link">                                
              <i className="fa fa-user"></i>
              <p>{custommsgsTxt}</p>                            
            </Link>
          </li>
          : null}

          
          {(userInfo.userPermissions.length > 0 && userInfo.userPermissions.includes("EditSettings")) || (userInfo.super == 1) ?

          <li className="nav-item ">
          <Link to="/admin/settings" className="nav-link">                                
              <i className="fa fa-cog"></i>
              <p>{settingsTxt}</p>                            
            </Link>
          </li>
          : null}
          
          
          {(userInfo.userPermissions.length > 0 && userInfo.userPermissions.includes("AllCargos")) || (userInfo.super == 1) ?
          
          <li className="nav-item ">
            <Link to="/admin/cargos" className="nav-link">                                
              <i className="fa fa-book"></i>
              <p>{lng.cargosTxt}</p>                            
            </Link>
          </li>
          : null}

     
          
{(userInfo.userPermissions.length > 0 && userInfo.userPermissions.includes("Newsletters")) || (userInfo.super == 1) ?

<li className="nav-item ">
<Link to="/admin/newsletters" className="nav-link">                                
    <i className="fa fa-envelope"></i>
    <p>{lng.inboxTxt}</p>                            
  </Link>
</li>
: null}

          {(userInfo.userPermissions.length > 0 && userInfo.userPermissions.includes("AllContacts")) || (userInfo.super == 1) ?

          <li className="nav-item ">
          <Link to="/admin/contact" className="nav-link">                                
              <i className="fa fa-envelope-o"></i>
              <p>{inboxTxt}</p>                            
            </Link>
          </li>
          : null}
        </ul>
      </div>
    
    
    </div>


        )
    }
}



export default Sidebar;
