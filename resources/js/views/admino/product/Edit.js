import React, { Component, Fragment} from 'react'

import {toast} from 'react-toastify';
import { connect } from 'react-redux';

import {Helmet} from "react-helmet";
import { Link } from 'react-router-dom';
 
import ImgToBeUploaded from '../../../components/ImageToUpload';
import RowThumbnail from '../../../components/RowThumbnail';
import RmvFileFrmDB from '../../../helpers/RmvFileFrmDB';
import CategoriesSelect from '../../../components/CategoriesSelect';
import ColoursSelect from '../../../components/ColoursSelect';
import SizesSelect from '../../../components/SizesSelect';

import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import getData from '../../../helpers/getData';
import putData from '../../../helpers/putData';
import { checkMimeType } from '../../../helpers/checkMimeType';

 class Edit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id:0,
            titleAR: '',
            //titleTR: '',
            titleEN: '',
            descriptionAR: null,
            //descriptionTR: null,
            descriptionEN: null,
            priceBeforeDiscount: 0,
            priceAfterDiscount: 0,
            sku: '',
            status: 1,
            theCount: 1,
            isOffer: 0,
            isGallery: 0,
            errorMessage: '',
            isDisabled: false,
            // here i load original images of an element that comes from db.
            otherImages: [],
            photo: '',
            //.
            // here the new selected images will be loaded.
            mainImg: null,            
            images: [],
            //.
            categoryId: 0,
            selectedColours: [],
            selectedSizes: [],
            prdctPrps: [],
            propValueId: 0,
            propId: 0,
            isSvPrpsDisabled: false,
            isSvVlsDisabled: false
        };
        
        this.onChangeValue = this.onChangeValue.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.fetchData = this.fetchData.bind(this);

        //the next functions handle images on selected, change and upload.
        this.onMainImageChange = this.onMainImageChange.bind(this);
        this.onOtherImagesChange = this.onOtherImagesChange.bind(this);
        this.rmvSlctdOthrImgs = this.rmvSlctdOthrImgs.bind(this);
        this.rmvSlctdMnImg = this.rmvSlctdMnImg.bind(this);        
        this.hndlUpldOthrImgs = this.hndlUpldOthrImgs.bind(this);
        this.hndlUpldMnImg = this.hndlUpldMnImg.bind(this);        
        this.rmvOthrImgs = this.rmvOthrImgs.bind(this);
        this.rmvMnImg = this.rmvMnImg.bind(this);      
        //.
        
        this.onColoursSelectChange = this.onColoursSelectChange.bind(this);
        this.onSizesSelectChange = this.onSizesSelectChange.bind(this);
        this.handleSvPrps = this.handleSvPrps.bind(this);
        this.handleSvValues = this.handleSvValues.bind(this);
    }
  
    onColoursSelectChange(selectedColours) {
      this.setState({selectedColours})
    }
  
    onSizesSelectChange(selectedSizes) {
      this.setState({selectedSizes})
    }
    componentDidMount(){

         this.fetchData(this.props.match.params.id);     

    }

    fetchData(elmntId) {

      getData(this.props.defaultSets.apiUrl+'products/'+elmntId+'/edit')

          //.then(response => response.json())
          .then(response => {

            let colours = [];
        {response.data.product_props && response.data.product_props.map((item, i) => {
          colours.push({
            "label": eval("item.title"+this.props.selectedLanguage.langCode), 
            "value": item.id
          })
        })};
        

            this.setState({ 
                id: elmntId,
                categoryId: response.data.categoryId,
                colourId: response.data.colourId,
                sizeId: response.data.sizeId,
                titleAR: response.data.titleAR,
                //titleTR: response.data.titleTR,
                titleEN: response.data.titleEN,
                descriptionAR: response.data.descriptionAR,
                //descriptionTR: response.data.descriptionTR,
                descriptionEN: response.data.descriptionEN,
                
                priceBeforeDiscount: response.data.priceBeforeDiscount,
                priceAfterDiscount: response.data.priceAfterDiscount,
                theCount: response.data.theCount,
                sku: response.data.sku,
                status: response.data.status,
                isOffer: response.data.isOffer,
                isGallery: response.data.isGallery,
                photo: response.data.photo,
                otherImages: response.data.files,
                
                selectedColours: colours,
                prdctPrps: response.data.product_props
              });
              //console.log(response.data.files)
          })
          .catch(function (error) {
              console.log(error);
          });
    }


    onMainImageChange(event) {
      if (event.target.files && event.target.files[0]) {
    
        let img = event.target.files[0];
    
        let mimeTypeCheck = checkMimeType(event, this.props.selectedLanguage.notAllowedTxt);
        if(mimeTypeCheck == true){
    
          return this.setState({
            mainImg: img 
          }); 
        } 
        toast.error(mimeTypeCheck);
          
      }
    };
    onOtherImagesChange(event) {
      if (event.target.files && event.target.files[0]) {

        let mimeTypeCheck = checkMimeType(event, this.props.selectedLanguage.notAllowedTxt);
        if(mimeTypeCheck == true){
    
          return this.setState({ images: [...this.state.images, ...event.target.files] })

        } 
        return toast.error(mimeTypeCheck);


      }
    };


    // The next function is used inside handleSubmit function not used directrly anywhere 
    // so after row have been inserted it uploads its photo.
    
    hndlUpldMnImg(rowId) {
      if (this.state.mainImg == null ) {
        return false;
      }

      // then prepare a form to send files.
      const formData = new FormData()
      
      // The next two lines go to FilesController to specify the row and tbl where inserted.
      formData.append('rowId', rowId);
      formData.append('type', 'product');
      formData.append('mainPhoto', '1');
      //.

      
      // append the main photo to formData.
      formData.append(
        'files[]',
        this.state.mainImg,
        this.state.mainImg.name
      )
      //.

      fetch(this.props.defaultSets.apiUrl+'upload-files', {
        method: 'POST',
        body: formData
      })
      .then(response => response.json())
      .then(response => {
        this.setState({
          mainImg: null,
        });
        /* onUploadProgress: progressEvent => {
          console.log(progressEvent.loaded / progressEvent.total)
        } */
      })
    }


    hndlUpldOthrImgs(rowId) { // Id of the row that created in DB to be inserted in files db table.
      
      if (this.state.images.length == 0) {
        return false;
      }
      // then prepare a form to send files.
      const formData = new FormData()
      
      // The next two lines go to FilesController to specify the row and tbl where inserted.
      formData.append('rowId', rowId);
      formData.append('type', 'product');
      //.

      // iterate over multiple additional images and append them to formData.
      this.state.images.forEach((image, i) => { 
        formData.append(
          'files[]',
          image,
          image.name
        )
      })
  
      fetch(this.props.defaultSets.apiUrl+'upload-files', {
        method: 'POST',
        body: formData
      })
      .then(response => response.json())
      .then(response => {
        this.setState({          
          images: []
        });
        /* onUploadProgress: progressEvent => {
          console.log(progressEvent.loaded / progressEvent.total)
        } */
      })
    }
    
    
    rmvSlctdOthrImgs(name) { // handle selected photos not that comes from db.
      this.setState({
        images: this.state.images.filter(image => image.name !== name)
      })
    }

    rmvSlctdMnImg() { // handle selected photo not main photo that comes from db.
      this.setState({
        mainImg: null
      })
    }
    
    rmvMnImg() { //handle main photo that comes from db not a selected one.
      this.setState({
        photo: null
      });
    }

    rmvOthrImgs(id) { // handle other photos that comes from db not a selected ones.
      
      this.setState({
        otherImages: this.state.otherImages.filter(image => image.id !== id)        
      })
      RmvFileFrmDB(this.props.defaultSets.apiUrl, id, 'product', this.props.selectedLanguage.loadingTxt);
    }

  

    onChangeValue(e) {
      
        this.setState({
            [e.target.name]: e.target.value, isDisabled: false
        });
    }
    
    handleSubmit(e){

        e.preventDefault();
        this.setState({isDisabled: true});
        const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

        
          putData(this.props.defaultSets.apiUrl+'products/'+this.state.id, {
            id: this.state.id,
            categoryId: this.state.categoryId,
            sizeId: this.state.sizeId,
            colourId: this.state.colourId,
            titleAR: this.state.titleAR,
            //titleTR: this.state.titleTR,
            titleEN: this.state.titleEN,
            descriptionAR: this.state.descriptionAR,
            descriptionEN: this.state.descriptionEN,
            //descriptionTR: this.state.descriptionTR,
            priceBeforeDiscount: this.state.priceBeforeDiscount,
            priceAfterDiscount: this.state.priceAfterDiscount,
            sku: this.state.sku,
            status: this.state.status,
            isGallery: this.state.isGallery,
            isOffer: this.state.isOffer,
            theCount: this.state.theCount,
            photo: this.state.mainImg || this.state.photo,
            langCode: this.props.selectedLanguage.langCode
        })
            .then( (response) => {

              this.hndlUpldMnImg(this.state.id);
              this.hndlUpldOthrImgs(this.state.id);

                toast.update(ldToast, {
                    type: toast.TYPE.SUCCESS,
                    render: response.data.msg
                });

               /*  setTimeout( () => {
                    this.props.history.push('/products');
                }, 3000); */
            })
            .catch( (error) => {
                
                this.setState({  isDisabled: false})

                if(error.response !== undefined && error.response.status === 422){
                    let errorTxt = '';
                    let errorMessage = error.response.data.errors;

                    Object.keys(errorMessage).map((key, i) => {
                        errorTxt = (<span> {errorTxt} {errorMessage[key][0]} <br /></span>);});
                    toast.update(ldToast, {
                        type: toast.TYPE.ERROR,
                        render: errorTxt
                    });
                }
            });

    }


    handleSvValues(e){

      
      e.preventDefault();
      this.setState({isSvVlsDisabled: true});
      const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

      // prepare selected roles to be sent.
      let sizes = [];
      {this.state.selectedSizes && this.state.selectedSizes.map((item, i) => {
        sizes.push(item.value);
      })}
      //.
 
      axios.post(this.props.defaultSets.apiUrl+'sv-prdct-prps-vls', {
        propId: this.state.propId,
        productId: this.state.id,
        propValues: sizes,
        sku: this.state.sku,
        theCount: this.state.theCount,
        langCode: this.props.selectedLanguage.langCode
      })
          .then( (response) => {
            if(response.data.success == true) {
              toast.update(ldToast, {
                type: toast.TYPE.SUCCESS,
                render: response.data.msg
            });
            } else {
              toast.update(ldToast, {
                type: toast.TYPE.ERROR,
                render: response.data.msg
            });
            }
              
              this.setState({ isSvVlsDisabled: false})

          })
          .catch( (error) => {
              console.log(error);
          });

  }

    handleSvPrps(e){

      e.preventDefault();
      this.setState({isSvPrpsDisabled: true});
      const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

      // prepare selected roles to be sent.
      let colours = [];
      {this.state.selectedColours && this.state.selectedColours.map((item, i) => {
        colours.push(item.value);
      })}
      //.

      axios.post(this.props.defaultSets.apiUrl+'sv-prdct-prps', {
        productId: this.state.id,
        props: colours,
        langCode: this.props.selectedLanguage.langCode
      })
          .then( (response) => {
            
              if(response.data.success == true) {
                toast.update(ldToast, {
                  type: toast.TYPE.SUCCESS,
                  render: response.data.msg
              });
              } else {
                toast.update(ldToast, {
                  type: toast.TYPE.ERROR,
                  render: response.data.msg
              });
              }
              this.setState({ isSvVlsDisabled: false, isSvPrpsDisabled: false, prdctPrps: response.data.prps})

          })
          .catch( (error) => {
              console.log(error);
          });

  }

    render() {
        
        
        const {errorMessage, isDisabled,titleAR, titleEN, descriptionAR, descriptionEN
          , priceBeforeDiscount, priceAfterDiscount, sku, photo, status, prdctPrps, prdctData,
          theCount, images, otherImages, mainImg, categoryId, colourId, sizeId, isOffer, isGallery
          , selectedColours, selectedSizes, isSvPrpsDisabled, isSvVlsDisabled} = this.state;

        const { titleTxt, prcBfrDscnt, prcAftrDscnt, updtPrdctTxt, productsTxt, theCountTxt,
          skuTxt, statusTxt, mainPhotoTxt, saveTxt, descriptionTxt, activeTxt, inActiveTxt
         , otherPhotosTxt, isOfferTxt, isGalleryTxt
         , chooseTxt, savePropsTxt, svPrdctFrstTxt, svPrpsFrstTxt, sizesTxt, coloursTxt,
         colourTxt, sizeTxt,yesTxt, noTxt, langCode} = this.props.selectedLanguage;

        return (
                   
          
          <div className="row">

                        
<Helmet><title>{updtPrdctTxt}</title></Helmet>

                  
<div className="col-md-12">
        
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item"><Link to="/admin"><i className="fa fa-home"></i></Link></li>
            <li className="breadcrumb-item"><Link to="/admin/products">{productsTxt}</Link></li>
            <li className="breadcrumb-item active" aria-current="page">{updtPrdctTxt}</li>
          </ol>
        </nav>
      </div>


          <div className="col-md-8">
            <div className="card">
              <div className="card-header card-header-primary">
                <h4 className="card-title">{updtPrdctTxt}</h4>
                <p className="card-category"></p>
              </div>
              <div className="card-body">
                <form>

                  
                  <div className="row mb-3">
                      <div className="col-md-12">
                          <CategoriesSelect
                          onChangeValue={this.onChangeValue} 
                          dfltId={categoryId} 
                          defaultSets={this.props.defaultSets}
                          selectedLanguage={this.props.selectedLanguage} />
                      </div>
                  </div>

                  <div className="row">
                    <div className="col-md-6">
                      <div className="form-group">
                        <label className="bmd-label-floating">{titleTxt} AR</label>                          
                        <input 
                        className="form-control"
                        value={titleAR}
                        type="text"
                        name="titleAR"
                        onChange={this.onChangeValue} />
                      </div>
                    </div>

                    <div className="col-md-6">
                      <div className="form-group">
                        <label className="bmd-label-floating">{titleTxt} EN</label>                            
                        <input 
                        value={titleEN}
                        className="form-control"
                        type="text"
                        name="titleEN"
                        onChange={this.onChangeValue} />
                      </div>
                    </div>

                    </div>

                    <div className="row">

                    <div className="col-md-6">
                      <div className="form-group">
                        <label className="bmd-label-floating">{prcBfrDscnt}</label>                            
                        <input 
                        value={priceBeforeDiscount}
                        className="form-control"
                        type="text"
                        name="priceBeforeDiscount"
                        onChange={this.onChangeValue} />
                      </div>
                    </div>


                    <div className="col-md-6">
                      <div className="form-group">
                        <label className="bmd-label-floating">{prcAftrDscnt}</label>                            
                        <input 
                        value={priceAfterDiscount}
                        className="form-control"
                        type="text"
                        name="priceAfterDiscount"
                        onChange={this.onChangeValue} />
                      </div>
                    </div>

                    
                  </div>
               

                  <div className="row">

                    <div className="col-md-12">
                      <div className="form-group">
                        <label className="bmd-label-floating">{statusTxt}</label>                     
                        <select 
                        value={status}
                        name="status"
                        className="form-control"
                        onChange={this.onChangeValue}>
                          <option value="1">{activeTxt}</option>
                          <option value="0">{inActiveTxt}</option>
                        </select>
                      </div>
                    </div>

                  </div>


                  <div className="row">

                    <div className="col-md-6">
                      <div className="form-group">
                        <label className="bmd-label-floating">{isGalleryTxt}</label>                     
                        <select 
                        value={isGallery}
                        name="isGallery"
                        className="form-control"
                        onChange={this.onChangeValue}>
                          <option value="1">{yesTxt}</option>
                          <option value="0">{noTxt}</option>
                        </select>
                      </div>
                    </div>


                    <div className="col-md-6">
                      <div className="form-group">
                        <label className="bmd-label-floating">{isOfferTxt}</label>                     
                        <select 
                        value={isOffer}
                        name="isOffer"
                        className="form-control"
                        onChange={this.onChangeValue}>
                          <option value="1">{yesTxt}</option>
                          <option value="0">{noTxt}</option>
                        </select>
                      </div>
                    </div>


                  </div>

                  <div className="row">
                    <div className="col-md-12">
                      <div className="form-group">
                        <label></label>
                        <div className="form-group">
                          <label className="bmd-label-floating">{descriptionTxt} AR</label>

                          <CKEditor
                          editor={ ClassicEditor }
                          data={descriptionAR}
                          onChange={ ( event, editor ) => {
                              const data = editor.getData();
                              this.setState({descriptionAR: data})
                              //console.log( { event, editor, data } );
                          }}
                          />
                
                        </div>
                      </div>
                    </div>
                  </div>
                  
                  <div className="row">
                    <div className="col-md-12">
                      <div className="form-group">
                        <label></label>
                        <div className="form-group">
                          <label className="bmd-label-floating">{descriptionTxt} EN</label>
                                   
                          <CKEditor
                          editor={ ClassicEditor }
                          data={descriptionEN}
                          onChange={ ( event, editor ) => {
                              const data = editor.getData();
                              this.setState({descriptionEN: data})
                              //console.log( { event, editor, data } );
                          }}
                          />
                        </div>
                      </div>
                    </div>
                  </div>

                  <button 
                  type="submit" 
                  onClick={this.handleSubmit}
                  disabled={isDisabled}
                  className="btn btn-primary pull-right">{saveTxt}
                  </button>
                  <div className="clearfix"></div>
                </form>
              </div>
            </div>
          
          
          
          
          </div>
          

          
        <div className="col-md-4">
         

         <div className="col-md-12">
            
         <div className="card card-profile">
               <div className="card-avatar">.</div>
               <div className="card-body">
                 <h6 className="card-category text-gray"></h6>
                 <h5 className="card-title mb-3">{mainPhotoTxt}</h5>

                 <div className="card-description">
                   <RowThumbnail imgName={photo} rmvMnImg={this.rmvMnImg} />
                    
                   <ImgToBeUploaded 
                   image={mainImg} 
                   rmvSlctdMnImg={this.rmvSlctdMnImg} />
                   
                   <input type="file" 
                   accept=".gif,.jpg,.jpeg,.png"
                   onChange={this.onMainImageChange} />
                 </div>
                 
                 {/* <button 
                 onClick={this.handleUpload}
                 className="btn btn-primary btn-round">
                   Upload
                 </button> */}
               </div>
             </div>
          
         </div>
       
       
         <div className="col-md-12">
             
         <div className="card card-profile">
               <div className="card-avatar">.</div>
               <div className="card-body">
                 <h6 className="card-category text-gray"></h6>
                 <h5 className="card-title mb-3">{otherPhotosTxt}</h5>
                 
                 <div className="card-description">
                 {otherImages ? otherImages.map((img, index) => {
                                 return(
                                  <RowThumbnail imgName={img.fileName} imgId={img.id} rmvOthrImgs={this.rmvOthrImgs} key={'otherImg'+index} />
                                 )
                     }) : null
                   }

                 
                   {images? images.map((img, index) => {
                                 return(
                                   <ImgToBeUploaded image={img} key={'img'+index} rmvSlctdOthrImgs={this.rmvSlctdOthrImgs} />
                                 )
                     }) : null
                   }
                 
                   <input type="file" 
                   accept=".gif,.jpg,.jpeg,.png"
                   multiple onChange={this.onOtherImagesChange} />
                 </div>
                 
                 {/* <button 
                 onClick={this.handleUpload}
                 className="btn btn-primary btn-round">
                   Upload
                 </button> */}
               </div>
             </div>
          
         </div>            
       
       </div>

        </div>
      
        )
    }
}


const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        defaultSets: state.DfltSts
    }
}
export default connect(mapStateToProps, null)(Edit);
