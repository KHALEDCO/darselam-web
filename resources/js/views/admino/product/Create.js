import React, { Component, Fragment} from 'react'

import {toast} from 'react-toastify';
import { connect } from 'react-redux';

import ImgToBeUploaded from '../../../components/ImageToUpload';
import CategoriesSelect from '../../../components/CategoriesSelect';
import ColoursSelect from '../../../components/ColoursSelect';
import SizesSelect from '../../../components/SizesSelect';

import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';

import {Helmet} from "react-helmet";
import { Link } from 'react-router-dom';
import { checkMimeType } from '../../../helpers/checkMimeType';
import postData from '../../../helpers/postData';

 class Create extends Component {
    constructor(props) {
        super(props);
        this.state = {
          productId:0,
            titleAR: '',
            //titleTR: '',
            titleEN: '',
            descriptionAR: '',
            //descriptionTR: '',
            descriptionEN: '',
            priceBeforeDiscount: 0,
            priceAfterDiscount: 0,
            sku: '',
            photo: '',
            status: 1,
            isOffer: 0,
            isGallery: 0,
            theCount: 1,
            errorMessage: '',
            isDisabled: false,
            mainImg: null,            
            images: [],
            categoryId: 0,
            selectedColours: [],
            selectedSizes: [],
            prdctPrps: [],
            propValueId: 0,
            propId: 0,
            isSvPrpsDisabled: true,
            isSvVlsDisabled: true
        };

        
        this.onChangeValue = this.onChangeValue.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        
        //the next functions handle images on selected, change and upload.
        this.onMainImageChange = this.onMainImageChange.bind(this);
        this.onOtherImagesChange = this.onOtherImagesChange.bind(this);
        this.rmvSlctdOthrImgs = this.rmvSlctdOthrImgs.bind(this);
        this.rmvSlctdImg = this.rmvSlctdImg.bind(this);
        this.handleUploadOtherPhoto = this.handleUploadOtherPhoto.bind(this);
        this.handleUploadMainPhoto = this.handleUploadMainPhoto.bind(this);
        //.
        this.onColoursSelectChange = this.onColoursSelectChange.bind(this);
        this.onSizesSelectChange = this.onSizesSelectChange.bind(this);
        this.handleSvPrps = this.handleSvPrps.bind(this);
        this.handleSvValues = this.handleSvValues.bind(this);
         
    }
    onColoursSelectChange(selectedColours) {
      this.setState({selectedColours})
    }
  
    onSizesSelectChange(selectedSizes) {
      this.setState({selectedSizes})
    }
    componentDidMount() {  
    }

    
    onMainImageChange(event) {
      if (event.target.files && event.target.files[0]) {

        let img = event.target.files[0];

        let mimeTypeCheck = checkMimeType(event, this.props.selectedLanguage.notAllowedTxt);
        if(mimeTypeCheck == true){

          return this.setState({
            mainImg: img 
          }); 
        } 
        toast.error(mimeTypeCheck);
          
      }
    };

    onOtherImagesChange(event) {
      if (event.target.files && event.target.files[0]) {

        this.setState({ images: [...this.state.images, ...event.target.files] })

      }
    };

    // The next function is used inside handleSubmit function not used directrly anywhere 
    // so after row have been inserted it uploads its photo.
    handleUploadMainPhoto(rowId) {
      if (this.state.mainImg == null ) {
        return false;
      }

      // then prepare a form to send files.
      const formData = new FormData()
      
      // The next two lines go to FilesController to specify the row and tbl where inserted.
      formData.append('rowId', rowId);
      formData.append('type', 'product');
      formData.append('mainPhoto', '1');
      //.

      
      // append the main photo to formData.
      formData.append(
        'files[]',
        this.state.mainImg,
        this.state.mainImg.name
      )
      //.

      fetch(this.props.defaultSets.apiUrl+'upload-files', {
        method: 'POST',
        body: formData
      })
      .then(response => response.json())
      .then(response => {
        this.setState({
          mainImg: null,
        });
        /* onUploadProgress: progressEvent => {
          console.log(progressEvent.loaded / progressEvent.total)
        } */
      })
    }

    handleUploadOtherPhoto(rowId) { // Id of the row that created in DB to be inserted in files db table.
      
      if (this.state.images.length == 0) {
        return false;
      }
      // then prepare a form to send files.
      const formData = new FormData()
      
      // The next two lines go to FilesController to specify the row and tbl where inserted.
      formData.append('rowId', rowId);
      formData.append('type', 'product');
      //.

      // iterate over multiple additional images and append them to formData.
      this.state.images.forEach((image, i) => { 
        formData.append(
          'files[]',
          image,
          image.name
        )
      })
  
      fetch(this.props.defaultSets.apiUrl+'upload-files', {
        method: 'POST',
        body: formData
      })
      .then(response => response.json())
      .then(response => {
        this.setState({          
          images: []
        });
        /* onUploadProgress: progressEvent => {
          console.log(progressEvent.loaded / progressEvent.total)
        } */
      })
    }
    
    
    rmvSlctdOthrImgs(name) {
      this.setState({
        images: this.state.images.filter(image => image.name !== name)
      })
    }

    rmvSlctdImg() {
      this.setState({
        mainImg: null
      })
    }
    
    onChangeValue(e) {
      console.log(e.target.value)
        this.setState({
            [e.target.name]: e.target.value, isDisabled: false
        });
    }
    
    

    handleSvValues(e){

      console.log('halid', this.state.propId)
      e.preventDefault();
      this.setState({isSvVlsDisabled: true});
      const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

      // prepare selected roles to be sent.
      let sizes = [];
      {this.state.selectedSizes && this.state.selectedSizes.map((item, i) => {
        sizes.push(item.value);
      })}
      //.
 
      postData(this.props.defaultSets.apiUrl+'sv-prdct-prps-vls', {
        propId: this.state.propId,
        productId: this.state.productId,
        propValues: sizes,
        sku: this.state.sku,
        theCount: this.state.theCount,
        langCode: this.props.selectedLanguage.langCode
      })
          .then( (response) => {
            if(response.data.success == true) {
              toast.update(ldToast, {
                type: toast.TYPE.SUCCESS,
                render: response.data.msg
            });
            } else {
              toast.update(ldToast, {
                type: toast.TYPE.ERROR,
                render: response.data.msg
            });
            }
              
              this.setState({ isSvVlsDisabled: false})

          })
          .catch( (error) => {
              console.log(error);
          });

  }

    handleSvPrps(e){

      e.preventDefault();
      this.setState({isSvPrpsDisabled: true});
      const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

      // prepare selected roles to be sent.
      let colours = [];
      {this.state.selectedColours && this.state.selectedColours.map((item, i) => {
        colours.push(item.value);
      })}
      //.

      postData(this.props.defaultSets.apiUrl+'sv-prdct-prps', {
        productId: this.state.productId,
        props: colours,
        langCode: this.props.selectedLanguage.langCode
      })
          .then( (response) => {
            
              if(response.data.success == true) {
                toast.update(ldToast, {
                  type: toast.TYPE.SUCCESS,
                  render: response.data.msg
              });
              } else {
                toast.update(ldToast, {
                  type: toast.TYPE.ERROR,
                  render: response.data.msg
              });
              }
              this.setState({ isSvVlsDisabled: false, isSvPrpsDisabled: false, prdctPrps: response.data.prps})

          })
          .catch( (error) => {
              console.log(error);
          });

  }


    handleSubmit(e){

        e.preventDefault();
        this.setState({isDisabled: true});
        const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

        postData(this.props.defaultSets.apiUrl+'products', {
            categoryId: this.state.categoryId,
            titleAR: this.state.titleAR,
            titleEN: this.state.titleEN,
            descriptionAR: this.state.descriptionAR,
            descriptionEN: this.state.descriptionEN,
            priceBeforeDiscount: this.state.priceBeforeDiscount,
            priceAfterDiscount: this.state.priceAfterDiscount,            
            status: this.state.status,
            isGallery: this.state.isGallery,
            isOffer: this.state.isOffer,            
            photo: this.state.mainImg ? this.state.mainImg.name :  null,
            langCode: this.props.selectedLanguage.langCode
        })
            .then( (response) => {
              
              this.handleUploadMainPhoto(response.data.rowId);
              this.handleUploadOtherPhoto(response.data.rowId);

                toast.update(ldToast, {
                    type: toast.TYPE.SUCCESS,
                    render: response.data.msg
                });
                this.setState({ isSvPrpsDisabled: false, productId: response.data.rowId})

                /* setTimeout( () => {
                    this.props.history.push('/products');
                }, 3000); */
            })
            .catch( (error) => {
                //console.log(error.response);
                this.setState({  isDisabled: false})

                if(error.response !== undefined && error.response.status === 422){
                    let errorTxt = '';
                    let errorMessage = error.response.data.errors;

                    Object.keys(errorMessage).map((key, i) => {
                        errorTxt = (<span> {errorTxt} {errorMessage[key][0]} <br /></span>);});
                    toast.update(ldToast, {
                        type: toast.TYPE.ERROR,
                        render: errorTxt
                    });
                }
            });

    }

    render() {
       
        const {isSvPrpsDisabled, isDisabled, images, selectedColours, 
          prdctPrps, isSvVlsDisabled, selectedSizes} = this.state;

          
        const { langCode, titleTxt, prcBfrDscnt, prcAftrDscnt, crtPrdctTxt, productsTxt, theCountTxt,
          skuTxt, statusTxt, mainPhotoTxt, saveTxt, descriptionTxt, activeTxt, inActiveTxt, addProductTxt
         , photoTxt, otherPhotosTxt, isGalleryTxt, isOfferTxt, yesTxt, noTxt, coloursTxt, sizesTxt,
         colourTxt, sizeTxt, categoryTxt, chooseTxt, savePropsTxt, svPrdctFrstTxt, svPrpsFrstTxt
          
        } = this.props.selectedLanguage;

        return (
                           
        
      <div className="row">
          <Helmet><title>{crtPrdctTxt}</title></Helmet>

                  
      <div className="col-md-12">
        
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item"><Link to="/admin"><i className="fa fa-home"></i></Link></li>
            <li className="breadcrumb-item"><Link to="/admin/products">{productsTxt}</Link></li>
            <li className="breadcrumb-item active" aria-current="page">{crtPrdctTxt}</li>
          </ol>
        </nav>
      </div>


        <div className="col-md-8">
            
                     
            <div className="card">
                <div className="card-header card-header-primary">
                  <h4 className="card-title">{crtPrdctTxt}</h4>
                  <p className="card-category"></p>
                </div>
                <div className="card-body">
                  <form>

                    <div className="row">
                      <div className="col-md-12">
                      <label className="bmd-label-floating">{categoryTxt}</label>                     

                          <CategoriesSelect 
                          chosenOne={this.state.categoryId} 
                          onChangeValue={this.onChangeValue}
                          defaultSets={this.props.defaultSets}
                          selectedLanguage={this.props.selectedLanguage} />
                      </div>
                    </div>  


                    <div className="row">
                      <div className="col-md-6">
                        <div className="form-group">
                          <label className="bmd-label-floating">{titleTxt} AR</label>                          
                          <input 
                          className="form-control"
                          type="text"
                          name="titleAR"
                          onChange={this.onChangeValue} />
                        </div>
                      </div>

                      <div className="col-md-6">
                        <div className="form-group">
                          <label className="bmd-label-floating">{titleTxt} EN</label>                            
                          <input 
                          className="form-control"
                          type="text"
                          name="titleEN"
                          onChange={this.onChangeValue} />
                        </div>
                      </div>

                      </div>

                      <div className="row">

                      <div className="col-md-6">
                        <div className="form-group">
                          <label className="bmd-label-floating">{prcBfrDscnt}</label>                            
                          <input 
                          className="form-control"
                          type="text"
                          name="priceBeforeDiscount"
                          onChange={this.onChangeValue} />
                        </div>
                      </div>


                      <div className="col-md-6">
                        <div className="form-group">
                          <label className="bmd-label-floating">{prcAftrDscnt}</label>                            
                          <input 
                          className="form-control"
                          type="text"
                          name="priceAfterDiscount"
                          onChange={this.onChangeValue} />
                        </div>
                      </div>

                    </div>
                 

                    <div className="row">

                      <div className="col-md-12">
                        <div className="form-group">
                          <label className="bmd-label-floating">{statusTxt}</label>                     
                          <select 
                          name="status"
                          className="form-control"
                          onChange={this.onChangeValue}>
                            <option value="1">{activeTxt}</option>
                            <option value="0">{inActiveTxt}</option>
                          </select>
                        </div>
                      </div>

                    </div>


                    <div className="row">
                      <div className="col-md-6">
                        <div className="form-group">
                          <label className="bmd-label-floating">{isGalleryTxt}</label>                     
                          <select 
                          name="isGallery"
                          className="form-control"
                          onChange={this.onChangeValue}>
                            <option value="0">{noTxt}</option>
                            <option value="1">{yesTxt}</option>
                          </select>
                        </div>
                      </div>

                      <div className="col-md-6">
                        <div className="form-group">
                          <label className="bmd-label-floating">{isOfferTxt}</label>                     
                          <select 
                          name="isOffer"
                          className="form-control"
                          onChange={this.onChangeValue}>
                            <option value="0">{noTxt}</option>
                            <option value="1">{yesTxt}</option>
                          </select>
                        </div>
                      </div>

                    </div>

                    <div className="row">
                      <div className="col-md-12">
                        <div className="form-group">
                          <label></label>
                          <div className="form-group">
                            <label className="bmd-label-floating">{descriptionTxt} AR</label>
                                             
                          <CKEditor
                          editor={ ClassicEditor }
                          data=''
                          onChange={ ( event, editor ) => {
                              const data = editor.getData();
                              this.setState({descriptionAR: data})
                              //console.log( { event, editor, data } );
                          }}
                          />
                          </div>
                        </div>
                      </div>
                    </div>
                    
                    <div className="row">
                      <div className="col-md-12">
                        <div className="form-group">
                          <label></label>
                          <div className="form-group">
                            <label className="bmd-label-floating">{descriptionTxt} EN</label>
                                             
                          <CKEditor
                          editor={ ClassicEditor }
                          data=''
                          onChange={ ( event, editor ) => {
                              const data = editor.getData();
                              this.setState({descriptionEN: data})
                              //console.log( { event, editor, data } );
                          }}
                          />
                          </div>
                        </div>
                      </div>
                    </div>

                    <button 
                    type="submit" 
                    onClick={this.handleSubmit}
                    disabled={isDisabled}
                    className="btn btn-primary pull-right">{saveTxt}
                    </button>
                    <div className="clearfix"></div>
                  </form>
                </div>
              </div>
        
        </div>



        <div className="col-md-4">
         

          <div className="col-md-12">
             
          <div className="card card-profile">
                <div className="card-avatar">.</div>
                <div className="card-body">
                  <h6 className="card-category text-gray"></h6>
                  <h5 className="card-title mb-3">{mainPhotoTxt}</h5>
                  
                  <div className="card-description">
                    {this.state.mainImg != null && 
                    <ImgToBeUploaded 
                    image={this.state.mainImg} 
                    rmvSlctdImg={this.rmvSlctdImg} />

                    }
                    <input type="file"
                   accept=".gif,.jpg,.jpeg,.png" 
                   onChange={this.onMainImageChange} />
                  </div>
                  
                  
                </div>
              </div>
           
          </div>
        
        
          <div className="col-md-12">
              
          <div className="card card-profile">
                <div className="card-avatar">.</div>
                <div className="card-body">
                  <h6 className="card-category text-gray"></h6>
                  <h5 className="card-title mb-3">{otherPhotosTxt}</h5>
                  
                  <div className="card-description">

                    {images? images.map((img, index) => {
                                  return(
                                    <ImgToBeUploaded image={img} key={'img'+index} rmvSlctdOthrImgs={this.rmvSlctdOthrImgs} />
                                  )
                      }) : null
                    }
                  
                    <input type="file" multiple onChange={this.onOtherImagesChange} />
                  </div>
                  
                </div>
              </div>
           
          </div>            
        
        </div>


      </div>
        
        

        )
    }
}


const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        defaultSets: state.DfltSts
    }
}
export default connect(mapStateToProps, null)(Create);
