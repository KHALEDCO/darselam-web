import React, { Component, Fragment} from 'react'

import {toast} from 'react-toastify';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import {Helmet} from "react-helmet";
import RowThumbnail from '../../../components/RowThumbnail';
import getData from '../../../helpers/getData';

 class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            rows: ''
        };
    }
  
   
    componentDidMount(){
 
      const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

      getData(this.props.defaultSets.apiUrl+'custommsgs')

          //.then(response => response.json())
          .then(response => {
              this.setState({ rows: response.data.rows });
              toast.dismiss(ldToast)
              //console.log(response.data)
          })
          .catch(function (error) {
              console.log(error);
          })
    }

    
    deleteRow(id) {

      axios.delete(this.props.defaultSets.apiUrl+'custommsgs/'+id)
      .then(res => {
          document.getElementById(id).remove();          
        })
        .catch(err => {
          console.log(err);
        });

  }

    render() {
        
        //const {isLoading} = this.state;
        const { titleTxt, custommsgsTxt, updateTxt, deleteTxt,
          descriptionTxt, mainPhotoTxt, actionTxt} = this.props.selectedLanguage;

        return (
           
         <Fragment>
                
          <div className="row">
          <Helmet><title>{custommsgsTxt}</title></Helmet>

          <div className="col-md-12">
                  
                  <nav aria-label="breadcrumb">
                    <ol className="breadcrumb">
                      <li className="breadcrumb-item"><Link to="/admin"><i className="fa fa-home"></i></Link></li>
                      <li className="breadcrumb-item active" aria-current="page">{custommsgsTxt}</li>
                    </ol>
                  </nav>
                </div>
                
            <div className="col-md-12">
            </div>
          </div>

          <div className="row">
          <div className="col-md-12">
            <div className="card">
              
              <div className="card-header card-header-primary">
                <h4 className="card-title ">{custommsgsTxt}</h4>
                <p className="card-category"></p>
              </div>

              <div className="card-body">
                <div className="table-responsive">
                  <table className="table">
                    <thead className=" text-primary">                    
                      <tr>
                      <th>{'#'}</th>
                      <th>{titleTxt}</th>
                      <th>{descriptionTxt}</th>
                      <th>{mainPhotoTxt}</th>
                      <th>{actionTxt}</th>
                      </tr>                      
                    </thead>
                    <tbody>
                            {this.state.rows ? 
                            this.state.rows.map((item, index) => {
                              return(
                              
            <tr id={item.id} key={index}>
            <td>{index + 1}</td>
            <td>{eval("item.title"+this.props.selectedLanguage.langCode)}</td> 
            <td>{eval("item.content"+this.props.selectedLanguage.langCode)}</td> 
            <td>{<RowThumbnail imgName={item.photo} />}</td>
            <td>
 
                    <Link 
                    to={{
                        pathname: "/admin/edit-custommsg/"+item.id,
                        //data: this.props.obj // your data array of objects
                      }}
                    className="btn btn-primary">{updateTxt}</Link>
                    <input type="button" 
                    value={deleteTxt} 
                    onClick={() => this.deleteRow(item.id)}
                    className="btn btn-danger"/>
                
            </td>
        </tr>
                                )
                            }) : null
                            }
                            
                    </tbody>
                  </table>
                </div>
              </div>
            
            </div>
          </div>

          </div>
      
         </Fragment>

        )
    }
}


const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        defaultSets: state.DfltSts
    }
}
export default connect(mapStateToProps, null)(Index);
