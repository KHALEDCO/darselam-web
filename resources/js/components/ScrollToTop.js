import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';


class ScrollToTop extends Component {
    constructor(props) {
        super(props);
        //this.deleteRow = this.deleteRow.bind(this);
    }

    componentDidUpdate(prevProps) {
		if (this.props.location !== prevProps.location) {
			window.scrollTo(0, 0);
		}
    }
    
    render() {
        
        return <React.Fragment />

    }
}


export default withRouter(ScrollToTop)
