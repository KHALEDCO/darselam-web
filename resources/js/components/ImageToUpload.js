import React from 'react'
//import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
//import { faTimesCircle } from '@fortawesome/free-solid-svg-icons'

export default (props) => {
  
    return(<div className='fadein'>
    { <div 
      className='delete'
      onClick={
          () => {
            props.rmvSlctdMnImg ?
              props.rmvSlctdMnImg() : props.rmvSlctdOthrImgs(props.image.name)
            }
    } 
      
      
    > 
    {(props.rmvSlctdMnImg || props.rmvSlctdOthrImgs) &&  props.image != null ? 'X' : ''}
{/*       <FontAwesomeIcon icon={faTimesCircle} size='2x' />
 */}    </div>}

{props.image != null &&
    <img src={ props.image != null && URL.createObjectURL(props.image)}     
    style={{maxWidth:'100%', maxHeight: '100%'}} />
}
  </div>)
  }