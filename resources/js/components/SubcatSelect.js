import React, { Component } from 'react';

class SubcatSelect extends Component {
    constructor(props) {
        super(props);

        this.state = {
          rows: []
        }
        //this.deleteRow = this.deleteRow.bind(this);
    }

    componentDidMount() {
      
      this.fetchSubcat(this.props.categoryId);

    }

    fetchSubcat(categoryId) {
      axios.get(this.props.defaultSets.apiUrl+'subcat-by-cat/'+categoryId)
      .then( (response) => {
          this.setState({rows: response.data.rows});
          //console.log(this.state.rows)
      })
      .catch( (error) => {
          //console.log(error.response);               
      });
    }

    componentDidUpdate(prevProps, prevState) {

      if (prevProps.categoryId != this.props.categoryId ) {
          this.fetchSubcat(this.props.categoryId);

      }
    }

    render() {
        const {rows} = this.state;
        const { langCode, subcatTxt} = this.props.selectedLanguage;
        const {subcatId, onChangeValue} = this.props;
        
        return (
 
          <select 
          name="subcatId"
          value={subcatId}
          className="form-control"
          onChange={onChangeValue}>
            <option value={''}>{subcatTxt}</option>
            {rows && rows.map((item, i) => {
              return(<option key={i} value={item.id}>{eval("item.title"+langCode)}</option>)

            })}
            
          </select>
        );
    }
}


export default SubcatSelect;
