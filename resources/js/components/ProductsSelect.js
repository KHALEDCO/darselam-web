import React, { Component } from 'react';
//import AsyncSelect from 'react-select/async';

class ProductsSelect extends Component {
    constructor(props) {
        super(props);

        this.state = {
          rows: [],
          title: ''
        }
        //this.deleteRow = this.deleteRow.bind(this);
        //this.filterRows = this.filterRows.bind(this);
        //this.loadRows = this.loadRows.bind(this);
    }

    componentDidMount() {

      axios.get(this.props.defaultSets.apiUrl+'products/status/1')
      .then( (response) => {
        this.setState({rows: response.data.rows});

        /* let rows = [];
        //

        {response.data.rows && response.data.rows.map((item, i) => {  
          let prdctInfo = item.elment_trans.find(trns => trns.languageCode == this.props.selectedLanguage.langCode);
                  
          rows.push({ value: item.id, label: prdctInfo.title})
      })}
      this.setState({rows}); */
      })
      .catch( (error) => {
          //console.log(error.response);               
      });

    }

    /* filterRows(title){
        
      return this.state.rows.filter(i =>
        i.label.toLowerCase().includes(title.toLowerCase())
      );
    }; 

   loadRows(title, callback){

      setTimeout(() => {
        callback(this.filterRows(title));
      }, 1000);
    }; */

    render() {
        const {rows} = this.state;
        const lng = this.props.selectedLanguage;
        const {productId, onChangeValue} = this.props;
        
        return (
 
          <select 
          name="productId"
          value={productId}
          className="form-control"
          onChange={onChangeValue}>
            <option value={''}>{lng.chooseTxt}</option>

            {rows && rows.map((item, i) => {
            let prdctInfo = item.elment_trans.find(trns => trns.languageCode == lng.langCode);

              return(<option key={item.id} value={item.id}>{(prdctInfo && prdctInfo.title)}</option>)

            })}
            
          </select>
         /*  <AsyncSelect
        noOptionsMessage={() => lng.writePrdctLtrsTxt}
        placeholder={lng.writePrdctLtrsTxt}
        defaultInputValue={productTitle}
        defaultValue={productId}
        isDisabled={false}
        isLoading={false}
        isRtl={lng.langCode == 'AR'? true: false}
        isSearchable={true}               
        isClearable={true}
        name="productId"
        onChange={(elmnt) => {this.props.hndlSlctVlu(elmnt)}}
        loadOptions={this.loadRows}
        onInputChange={(title) => {this.setState({ title })}}

        /> */
        );
    }
}


export default ProductsSelect;
