import React, { Component } from 'react';

import MultiSelect from "react-multi-select-component";
import getData from '../helpers/getData';

class FeaturesSelect extends Component {
    constructor(props) {
        super(props);

        this.state = {
          features: []
        }
        //this.deleteRow = this.deleteRow.bind(this);
    }
 
    componentDidMount() {

      getData(this.props.defaultSets.apiUrl+'features/status/1')
      .then( (response) => {
          this.setState({rows: response.data.rows});
          let features = [];
        {response.data.rows && response.data.rows.map((item, i) => {
          let featureInfo = item.elment_trans.find(trns => trns.languageCode == this.props.selectedLanguage.langCode);

          features.push({
            "label": featureInfo && featureInfo.title,
            "value": item.id
          })
        })};
          this.setState({features});
          //console.log(this.state.rows)
      })
      .catch( (error) => {
          //console.log(error.response);               
      });

    }

    render() {
        const {features} = this.state;
        const lng = this.props.selectedLanguage;
        const { onFeaturesSelectChange, selected} = this.props;

        return (
 <div>
          <MultiSelect
options={features}
value={selected}
onChange={(selected) => onFeaturesSelectChange(selected)}
labelledBy={lng.chooseTxt}

overrideStrings={{
  selectSomeItems: lng.chooseTxt,
  allItemsAreSelected: lng.AllSelectedTxt,
  selectAll: lng.selectAllTxt,
  search: lng.searchTxt,
}}

filterOptions={(features, filter) => {
  if (!filter) {
    return features;
  }
  
  const re = new RegExp(filter, "i");
  return features.filter(({ label }) => label && label.match(re));
}}
/>

          {/* <select 
          value={dfltId? dfltId : chosenOne}
          name="featureId"
          className="form-control"
          onChange={onChangeValue}>
            <option value="0">{lng.chooseTxt}</option>
            {rows && rows.map((item, i) => {
              return(<option key={i} value={item.id}>{eval("item.title"+langCode)}</option>)

            })}
            
          </select> */}
          </div>
        );
    }
}


export default FeaturesSelect;
