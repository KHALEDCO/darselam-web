import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import deleteData from '../helpers/deleteData';
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css
import RowThumbnail from './RowThumbnail';

class UserRow extends Component {
    constructor(props) {
        super(props);
        //this.deleteRow = this.deleteRow.bind(this);
    }

    
    deleteRow(id) {

        confirmAlert({
              
          customUI: ({ onClose }) => {
              return (
                  <div id="react-confirm-alert">
                      <div className="react-confirm-alert-overlay">
                          <div className="react-confirm-alert">
                              <div className="react-confirm-alert-body">
                                  <h1>{this.props.selectedLanguage.rUSureTxt}</h1>
                                  <div 
                                  className="react-confirm-alert-button-group">
                                  <button
                                  className="yes"
                                  onClick={() => {
                                      deleteData(this.props.defaultSets.apiUrl+'users/'+id, this.props.userInfo.accessToken)
                                      .then(res => {
                                          document.getElementById(id).remove();            
                                      })
                                      .catch(err => {
                                          console.log(err);
                                      });
                                      onClose();
                                  }}
                                  >
                                          {this.props.selectedLanguage.yesTxt}
                                      </button>
  
                                      <button className="no" onClick={onClose}>
                                      {this.props.selectedLanguage.noTxt}
                                      </button>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
  
              );
            }
        });
    
  
      }

      rtrnUsrGrp(groupId) {
                  switch(groupId){
                      case 1: return this.props.selectedLanguage.adminTxt;
                      case 2: return this.props.selectedLanguage.clientTxt;
                      case 3: return this.props.selectedLanguage.agentTxt;
                      default: return '';
                  }
              }

    render() {
        const {full_name, phone, email, adrs, id, photo, groupId, companyOwner} = this.props.obj;
        const {updateTxt, deleteTxt} = this.props.selectedLanguage;
        
        return (
            <tr id={id}>
                <td>{this.props.num}</td>
                <td>{full_name != null? full_name : companyOwner}</td>
                <td>{phone}</td>
                <td>{email}</td>    
                <td>{this.rtrnUsrGrp(groupId)}</td>
                <td>{<RowThumbnail imgName={photo} />}</td>            
                <td>{adrs}</td>
                <td>

                        <Link to={"/admin/edit-user/"+id} className="btn btn-primary">{updateTxt}</Link>
                        <input type="button" 
                        value={deleteTxt} 
                        onClick={() => this.deleteRow(id)}
                        className="btn btn-danger"/>
                    
                </td>
            </tr>
        );
    }
}


export default UserRow
