import React, { Component } from 'react';

import MultiSelect from "react-multi-select-component";
import getData from '../helpers/getData';

class CatsSelect extends Component {
    constructor(props) {
        super(props);

        this.state = {
          cats: []
        }
        //this.deleteRow = this.deleteRow.bind(this);
    }
 
    componentDidMount() {

      getData(this.props.defaultSets.apiUrl+'categories/status/1')
      .then( (response) => {
          this.setState({rows: response.data.rows});
          let cats = [];
        {response.data.rows && response.data.rows.map((item, i) => {
          let elmntTrns = item.elment_trans && item.elment_trans.find(row => row.languageCode == this.props.selectedLanguage.langCode);

          cats.push({
            "label": elmntTrns && elmntTrns.title, 
            "value": item.id
          })
        })};
          this.setState({cats});
          //console.log(this.state.rows)
      })
      .catch( (error) => {
          //console.log(error.response);               
      });

    }

    render() {
        const {cats} = this.state;
        const lng = this.props.selectedLanguage;
        const { onCatsSelectChange, selected} = this.props;

        return (
 <div>
          <MultiSelect
options={cats}
value={selected}
onChange={(selected) => onCatsSelectChange(selected)}
labelledBy={lng.chooseTxt}

overrideStrings={{
  selectSomeItems: lng.chooseTxt,
  allItemsAreSelected: lng.AllSelectedTxt,
  selectAll: lng.selectAllTxt,
  search: lng.searchTxt,
}}

filterOptions={(cats, filter) => {
  if (!filter) {
    return cats;
  }
  
  const re = new RegExp(filter, "i");
  return cats.filter(({ label }) => label && label.match(re));
}}
/>

          {/* <select 
          value={dfltId? dfltId : chosenOne}
          name="catId"
          className="form-control"
          onChange={onChangeValue}>
            <option value="0">{lng.chooseTxt}</option>
            {rows && rows.map((item, i) => {
              return(<option key={i} value={item.id}>{eval("item.title"+langCode)}</option>)

            })}
            
          </select> */}
          </div>
        );
    }
}


export default CatsSelect;
