import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Moment from 'moment';

class ArticleBox extends Component {
    constructor(props) {
        super(props);
        //this.deleteRow = this.deleteRow.bind(this);
    }

    render() {

        const item = this.props.obj;
        const lng = this.props.selectedLanguage;
        let elmntTrns = item.elment_trans && item.elment_trans.find(row => row.languageCode == this.props.selectedLanguage.langCode);

        return (
            
            <div className="col-md-12">
            <Link to={"/articles/"+item.id}>
              <div className="classic-effect">
                <div>
                  <img
                    src={"/uploads/files/"+item.photo}
                    className="img-fluid blur-up lazyload bg-img"
                    alt={elmntTrns && elmntTrns.title}
                  />
                  <span></span>
                </div>
              </div>
              </Link>
            
            <div className="blog-details">
              <h4>{Moment(item.created_at).format("YYYY-MM-DD")}</h4>
              <Link to={"/articles/"+item.id}>
                <p>{elmntTrns && elmntTrns.title}</p>
              </Link>
              <hr className="style1" />
        <h6 className="pt-2">{lng.writtenByAdminTxt}</h6>
            </div>
          </div>
         

        );
    }
}



export default ArticleBox;
