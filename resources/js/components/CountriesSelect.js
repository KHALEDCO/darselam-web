import React, { Component } from 'react';

class CountriesSelect extends Component {
    constructor(props) {
        super(props);

        this.state = {
          rows: []
        }
        //this.deleteRow = this.deleteRow.bind(this);
    }

    componentDidMount() {

      axios.get(this.props.defaultSets.apiUrl+'countries/status/1')
      .then( (response) => {
          this.setState({rows: response.data.rows});
          //console.log(this.state.rows)
      })
      .catch( (error) => {
          //console.log(error.response);               
      });

    }

    render() {
        const {rows} = this.state;
        const { langCode, countryTxt} = this.props.selectedLanguage;
        const {dfltValue, onChangeValue, chosenOne} = this.props;
        
        return (
 
          <select 
          value={dfltValue? dfltValue : chosenOne}
          name="countryId"
          className="form-control"
          onChange={onChangeValue}>
            <option value="0">{countryTxt}</option>
            {rows && rows.map((item, i) => {
              return(<option key={i} value={item.id}>{eval("item.title"+langCode)}</option>)

            })}
            
          </select>
        );
    }
}


export default CountriesSelect;
