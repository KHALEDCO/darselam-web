import React, { Component } from 'react';

import MultiSelect from "react-multi-select-component";
import getData from '../helpers/getData';

class ColoursSelect extends Component {
    constructor(props) {
        super(props);

        this.state = {
          colours: []
        }
        //this.deleteRow = this.deleteRow.bind(this);
    }

    componentDidMount() {

      getData(this.props.defaultSets.apiUrl+'colours')
      .then( (response) => {
          this.setState({rows: response.data.rows});
          let colours = [];
        {response.data.rows && response.data.rows.map((item, i) => {
          colours.push({
            "label": eval("item.title"+this.props.selectedLanguage.langCode), 
            "value": item.id
          })
        })};
          this.setState({colours});
          //console.log(this.state.rows)
      })
      .catch( (error) => {
          //console.log(error.response);               
      });

    }

    render() {
        const {colours} = this.state;
        const { langCode, chooseTxt, AllSelectedTxt, selectAllTxt, searchTxt } = this.props.selectedLanguage;
        const {dfltId, onColoursSelectChange, selected} = this.props;

        return (
 <div>
          <MultiSelect
options={colours}
value={selected}
onChange={(selected) => onColoursSelectChange(selected)}
labelledBy={chooseTxt}

overrideStrings={{
  selectSomeItems: chooseTxt,
  allItemsAreSelected: AllSelectedTxt,
  selectAll: selectAllTxt,
  search: searchTxt,
}}

filterOptions={(colours, filter) => {
  if (!filter) {
    return colours;
  }
  
  const re = new RegExp(filter, "i");
  return colours.filter(({ label }) => label && label.match(re));
}}
/>

          {/* <select 
          value={dfltId? dfltId : chosenOne}
          name="colourId"
          className="form-control"
          onChange={onChangeValue}>
            <option value="0">{chooseTxt}</option>
            {rows && rows.map((item, i) => {
              return(<option key={i} value={item.id}>{eval("item.title"+langCode)}</option>)

            })}
            
          </select> */}
          </div>
        );
    }
}


export default ColoursSelect;
