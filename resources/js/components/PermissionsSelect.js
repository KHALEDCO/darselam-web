import React, { Component } from 'react';
import MultiSelect from "react-multi-select-component";
import getData from '../helpers/getData';


class PermissionsSelect extends Component {
    constructor(props) {
        super(props);

        this.state = {   
          permissions: []
        }
        //this.deleteRow = this.deleteRow.bind(this);
    }

    componentDidMount() {

      getData(this.props.defaultSets.apiUrl+'permissions')
      .then( (response) => {
        let permissions = [];
        {response.data.rows && response.data.rows.map((item, i) => {
          permissions.push({
            "label": eval("item.title"+this.props.selectedLanguage.langCode), 
            "value": item.id
          })
        })};
          this.setState({permissions});

          //console.log(this.state.rows)
      })
      .catch( (error) => {
          //console.log(error.response);               
      });

    }

    render() {
        const {permissions} = this.state;
        const { langCode, chooseTxt, AllSelectedTxt, permissionsTxt, searchTxt, selectAllTxt} = this.props.selectedLanguage;
        const {dfltId, onPermissionsSelectChange, selected} = this.props;
        
        return (

          <div>
            {/* <pre>{JSON.stringify(selected)}</pre> */}

<MultiSelect
options={permissions}
value={selected}
onChange={(selected) => onPermissionsSelectChange(selected)}
labelledBy={chooseTxt}

overrideStrings={{
  selectSomeItems: chooseTxt,
  allItemsAreSelected: AllSelectedTxt,
  selectAll: selectAllTxt,
  search: searchTxt,
}}

filterOptions={(permissions, filter) => {
  if (!filter) {
    return permissions;
  }
  
  const re = new RegExp(filter, "i");
  return permissions.filter(({ label }) => label && label.match(re));
}}
/>
          </div>
        
        );
    }
}


export default PermissionsSelect;
