import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

class ListItem extends Component {
    constructor(props) {
        super(props);
        //this.deleteRow = this.deleteRow.bind(this);
    }

    render() {

        const item = this.props.obj;
        const lng = this.props.selectedLanguage;
        let elmntTrns = item.elment_trans && item.elment_trans.find(prdct => prdct.languageCode == lng.langCode);

        return (
            <div className="col mega-box">
                                        <div className="link-section">
                                          <div className="menu-title">
                                            <h5>{elmntTrns && elmntTrns.title}</h5>
                                          </div>
                                          <div className="menu-content">
                                            <ul>
                                              <li>
                                                <Link to={"/categories/"+item.id} className="mega-menu-banner">
                                                {elmntTrns && elmntTrns.files[0] && 
                                                <img 
                                                  src={"/uploads/files/"+elmntTrns.files[0].fileName}
                                                  alt={elmntTrns && elmntTrns.title}
                                                  className="img-fluid blur-up lazyload"
                                                />}
                                                </Link>
                                              </li> 
                                            </ul>
                                          </div>
                                        </div>
                                      </div>
                                                            
        );
    }
}


const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        //defaultSets: state.DfltSts
    }
}
export default connect(mapStateToProps, null)(ListItem);
