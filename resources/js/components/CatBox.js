import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';


class CatBox extends Component {
    constructor(props) {
        super(props);
        //this.deleteRow = this.deleteRow.bind(this);
    }

    render() {
                
        const item = this.props.obj;
        const lng = this.props.selectedLanguage;
        let elmntTrns = item.elment_trans && item.elment_trans.find(row => row.languageCode == this.props.selectedLanguage.langCode);


        return (

            <div className="catBoxContainer">
            {elmntTrns && elmntTrns.files[0] && 
              <img 
                src={"/uploads/files/"+elmntTrns.files[0].fileName}
                alt={elmntTrns && elmntTrns.title}
                className="img-fluid blur-up lazyload"
              />}
              
             <Link to={"/categories/"+id} className="imgButton">
             {elmntTrns && elmntTrns.title}
            </Link>
            
           </div>
            );
    }
}


export default CatBox;
