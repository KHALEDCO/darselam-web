import React, { Component } from 'react';
import MultiSelect from "react-multi-select-component";
import getData from '../helpers/getData';


class RolesSelect extends Component {
    constructor(props) {
        super(props);

        this.state = {   
          roles: []
        }
        //this.deleteRow = this.deleteRow.bind(this);
    }

    componentDidMount() {

      getData(this.props.defaultSets.apiUrl+'roles/status/1', this.props.userInfo.accessToken)
      .then( (response) => {
        let roles = [];
        {response.data.rows && response.data.rows.map((item, i) => {
          roles.push({
            "label": eval("item.title"+this.props.selectedLanguage.langCode), 
            "value": item.id
          })
        })};
          this.setState({roles});

          //console.log(this.state.rows)
      })
      .catch( (error) => {
          //console.log(error.response);               
      });

    }

    render() {
        const {roles} = this.state;
        const { langCode, chooseTxt, AllSelectedTxt, rolesTxt, searchTxt, selectAllTxt} = this.props.selectedLanguage;
        const {dfltId, onRolesSelectChange, selected} = this.props;
        
        return (

          <div>
            {/* <pre>{JSON.stringify(selected)}</pre> */}
 
<MultiSelect
options={roles}
value={selected}
onChange={(selected) => onRolesSelectChange(selected)}
labelledBy={chooseTxt}

overrideStrings={{
  selectSomeItems: chooseTxt,
  allItemsAreSelected: AllSelectedTxt,
  selectAll: selectAllTxt,
  search: searchTxt,
}}

filterOptions={(roles, filter) => {
  if (!filter) {
    return roles;
  }
  
  const re = new RegExp(filter, "i");
  return roles.filter(({ label }) => label && label.match(re));
}}
/>
          </div>
        
        );
    }
}


export default RolesSelect;
