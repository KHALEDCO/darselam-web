<?php

namespace App\Http\Controllers;

use App\Http\Models\ProductRegister;
use Illuminate\Http\Request;
use App\Http\Requests\ProductRegisterRequest;

class ProductRegisterController extends Controller
{
    public function __construct()
	{
        $this->middleware('auth:api', ['except' => ['store']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
         
        $rows = ProductRegister::orderBy('id', 'desc')->get();
        return response()->json(['rows' => $rows]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRegisterRequest $request)
    {
        $check = ProductRegister::where('serialNum', $request->serialNum)
        ->where('guaranteeNum', $request->guaranteeNum)->count();

        if($check > 0){
            return response()->json([
                'msg' => trans('general.prdctRgstrdBfr'),
                'success' => false
                ]);
        }

        $row = ProductRegister::create($request->all());
        return response()->json([
            'msg' => trans('general.savedSuccessfully'),
            'success' => true,
            'rowId' => $row->id
            ]);

    }

  
    public function edit($id)
    {
        $row = ProductRegister::find($id);
        return response()->json($row);

    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductRegister  $productRegister
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRegisterRequest $request)
    {
        $row = ProductRegister::findOrFail($request->id);
        $row->update($request->all());

        return response()->json(['msg' => trans('general.updatedSuccessfully')]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductRegister  $productRegister
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ProductRegister::find($id)->delete();
    }
}
