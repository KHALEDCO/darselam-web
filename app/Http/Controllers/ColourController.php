<?php

namespace App\Http\Controllers;

use App\Http\Models\Colour;
use Illuminate\Http\Request;
use App\Http\Requests\ColourRequest;

class ColourController extends Controller
{
    public function __construct()
	{
        $this->middleware('auth:api', ['except' => ['show', 'index']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
         
        $rows = Colour::where('isDeleted', 0)->orderBy('id', 'desc')->get();
        return response()->json(['rows' => $rows]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ColourRequest $request)
    {
        $row = Colour::create($request->all());
        return response()->json([
            'msg' => trans('general.savedSuccessfully'),
            'rowId' => $row->id
            ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Colour  $colour
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $row = Colour::where('status', 1)->find($id);
        return response()->json($row);

    }
    public function edit($id)
    {
        $row = Colour::find($id);
        return response()->json($row);

    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Colour  $colour
     * @return \Illuminate\Http\Response
     */
    public function update(ColourRequest $request)
    {
        $row = Colour::findOrFail($request->id);
        $row->update($request->all());

        return response()->json(['msg' => trans('general.updatedSuccessfully')]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Colour  $colour
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Colour::where('id', $id)->update(['isDeleted' => 1]);
    }
}
