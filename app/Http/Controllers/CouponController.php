<?php

namespace App\Http\Controllers;

use App\Http\Models\Coupon;
use App\Http\Models\CouponUsed;
use Illuminate\Http\Request;
use App\Http\Requests\CouponRequest;

class CouponController extends Controller
{
    public function __construct()
	{
        $this->middleware('auth:api', ['except' => ['couponInfo']]);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
       
        $rows = Coupon::where('isDeleted', 0)
        ->orderBy('id', 'desc')
        ->get();
        return response()->json(['rows' => $rows]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CouponRequest $request)
    {
        $row = Coupon::create($request->all());
        return response()->json([
            'msg' => trans('general.savedSuccessfully'),
            'rowId' => $row->id
            ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Coupon  $colour
     * @return \Illuminate\Http\Response
     */
    

    public function couponInfo(Request $request, $couponCode = '')
    {
        if( trim($request->couponCode) == ''){ // if there is NO coupon.
            return response()->json([    
                'success' => false,
                'msg' => trans('general.invalidCoupon')
            ]);    
        }

        $todayDate = date('Y-m-d');

        $row = Coupon::where('couponCode', $request->couponCode)
            ->where('validFrom', '<=', $todayDate)
            ->where('validTo', '>=', $todayDate)
            ->where('status', '1')
            ->where('isDeleted', '0')
            ->first();

            if( $row == null){ // if there is NO coupon.
                return response()->json([    
                    'success' => false,
                    'msg' => trans('general.couponExpired')
                ]);    
            }

            if($row['usedQuantity'] == $row['originalQuantity'] ){
                return response()->json([
                    'success' => false,
                    'msg' => trans('general.couponsRanOut')
                ]);
            }

            $checkUserWithCoupon = CouponUsed::where('couponId', $row['id'])
            ->where('userId', $request->userId)
            ->first();

        if( $checkUserWithCoupon == null) { // then it's the first time to use this coupon.

            return response()->json([
                'success' => true,
                'couponInfo' => $row
            ]);
        }

        return response()->json([
            'success' => false,
            'msg' => trans('general.couponUsedBefore')
        ]);

    }

    public function edit($id)
    {
        $row = Coupon::find($id);
        return response()->json($row);

    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Coupon  $colour
     * @return \Illuminate\Http\Response
     */
    public function update(CouponRequest $request)
    {
        $row = Coupon::findOrFail($request->id);
        $row->update($request->all());

        return response()->json(['msg' => trans('general.updatedSuccessfully')]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Coupon  $colour
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Coupon::where('id', $id)->update(['isDeleted' => 1]);
    }
}
