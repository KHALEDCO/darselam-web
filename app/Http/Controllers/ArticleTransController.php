<?php

namespace App\Http\Controllers;

//use App\Article;
use App\Http\Models\Article;
use App\Http\Models\ArticleTrans;
use Illuminate\Http\Request;
use App\Http\Requests\ArticleRequest;

class ArticleTransController extends Controller
{
    public function __construct()
	{
        $this->middleware('auth:api', ['except' => ['show', 'index']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /* public function index($status = '')
    {        

        if( $status != '') {
            $rows = ArticleTrans::with('elment_trans')->where('status', $status)->get();
        } else {
            $rows = ArticleTrans::with('elment_trans')->orderBy('id', 'desc')->get();
        } 
        
        return response()->json(['rows' => $rows]);
    } */

    /** 
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArticleRequest $request)
    {
        $row = ArticleTrans::create($request->all());       

        return response()->json([
            'msg' => trans('general.savedSuccessfully'),
            'rowId' => $row->id
            ]);

    }

    public function edit($id)
    {
        $row = ArticleTrans::find($id);
        return response()->json($row);

    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $row = ArticleTrans::findOrFail($request->id);
        $row->update($request->all());

        return response()->json(['msg' => trans('general.updatedSuccessfully')]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ArticleTrans::find($id)->delete();
    }
}
