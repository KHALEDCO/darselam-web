<?php

namespace App\Http\Controllers;

//use App\Feature;
use App\Http\Models\Feature;
use Illuminate\Http\Request;
use App\Http\Requests\FeatureRequest;

class FeatureController extends Controller
{
    public function __construct()
	{
        $this->middleware('auth:api', ['except' => ['show', 'index']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($status = '')
    {        

        if( $status != '') {
            $rows = Feature::with('elment_trans')->where('status', $status)->get();
        } else {
            $rows = Feature::with('elment_trans')->orderBy('id', 'desc')->get();
        } 
        
        return response()->json(['rows' => $rows]);
    }

    /** 
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FeatureRequest $request)
    {
        $row = Feature::create($request->all());
        // save trans.
        $row->languages()->attach($request->get('languageCode', []), 
        [
            'title' => $request->title,
            'description1' => $request->description1,
            'description2' => $request->description2,
        ]);
 
        return response()->json([
            'msg' => trans('general.savedSuccessfully'),
            'rowId' => $row->id
            ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Feature  $feature
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $row = Feature::where('status', 1)->find($id);
        return response()->json($row);

    }
    public function edit($id)
    {
        $row = Feature::find($id);
        return response()->json($row);

    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Feature  $feature
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $row = Feature::findOrFail($request->id);
        $row->update($request->all());

        return response()->json(['msg' => trans('general.updatedSuccessfully')]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Feature  $feature
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Feature::find($id)->delete();
    }
}
