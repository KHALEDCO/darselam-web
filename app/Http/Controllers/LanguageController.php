<?php

namespace App\Http\Controllers;

use App\Http\Models\Language;
use Illuminate\Http\Request;
use App\Http\Requests\LanguageRequest;

class LanguageController extends Controller
{
    public function __construct()
	{
        $this->middleware('auth:api', ['except' => ['index']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($status = '')
    {        
        

        if( $status != '') {
            $rows = Language::where('status', $status)->get();
        } else {
            $rows = Language::orderBy('id', 'desc')->get();
        } 
        
        return response()->json(['rows' => $rows]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LanguageRequest $request)
    {
        $row = Language::create($request->all());
        return response()->json([
            'msg' => trans('general.savedSuccessfully'),
            'rowId' => $row->id
            ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Language  $Language
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $row = Language::find($id);
        return response()->json($row);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Language  $Language
     * @return \Illuminate\Http\Response
     */
    public function update(LanguageRequest $request)
    {
        $row = Language::findOrFail($request->id);
        $row->update($request->all());

        return response()->json(['msg' => trans('general.updatedSuccessfully')]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Language  $Language
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Language::findOrFail($id)->delete();
    }
}
