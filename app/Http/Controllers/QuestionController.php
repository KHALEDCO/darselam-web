<?php

namespace App\Http\Controllers;

use App\Http\Models\Question;
use App\Http\Requests\QuestionRequest;
use App\Http\Models\Answer;
use Illuminate\Http\Request;
use App\Http\Models\StudentAnswer;
use App\Http\Models\StudentScore;
use App\Http\Models\User;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index']]);
    }

    public function index(Request $request, $branchId = 0, $examId = 0)
    {

        $rows = Question::where('examId', $examId)
            ->with('answers')
            ->where('branchId', $branchId) 
            ->orderBy('id', 'desc');

            if (isset($request->offset) and isset($request->limit)) {
                $rows =  $rows->offset($request->offset)->limit($request->limit);
            }
    
            // Get by sort.
            if (isset($request->sortBy) and isset($request->sortSign)) {
                $rows =  $rows->orderBy($request->sortBy, $request->sortSign);
            }
            $rows =  $rows->get(); 

        return response()->json(['rows' => $rows]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(QuestionRequest $request)
    {
        // save a new question.
        $row = Question::create($request->all());

        // save its temporary empty answers.
        $answers = [];
        for ($k = 1; $k <= 4; $k++) {

            $answers[] = [
                'examId' => $request->examId,
                'questionId' => $row->id,
                'answer' => '',
                'correct' => 0
            ];
        }
        Answer::insert($answers);

        return response()->json([
            'msg' => trans('general.savedSuccessfully'),
            'success' => true
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Question  $Question
     * @return \Illuminate\Http\Response
     */
    /* public function show($id)
    {
        $row = Question::with('questions')->where('status', 1)->find($id);
        return response()->json($row);
    } */

    public function edit($id)
    {
        $row = Question::with('answers')->find($id);
        return response()->json($row);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Question  $Question
     * @return \Illuminate\Http\Response
     */
    public function update(QuestionRequest $request)
    {
        $row = Question::findOrFail($request->id);
        $row->update($request->all());

        return response()->json([
            'msg' => trans('general.updatedSuccessfully'),
            'success' => true
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Question  $Question
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Question::findOrFail($id)->delete();
        Answer::where('questionId', $id)->delete();
    }

    public function updateMultipleQuestions(Request $request)
    {
        /* return response()->json([
            'ggggg' => $request->data[0]['answers'][0]['answer']
            ]); */

        foreach ($request->data as $row) {

            Question::updateOrCreate(
                ['id' => $row['id'], 'examId' => $row['examId']],
                ['question' => $row['question']]
            );

            foreach ($row['answers'] as $answer) {
                Answer::updateOrCreate(
                    ['id' => $answer['id'], 'questionId' => $answer['questionId']],
                    ['answer' => $answer['answer'], 'correct' => $answer['correct']]
                );
            }
        }

        return response()->json([
            'msg' => trans('general.updatedSuccessfully'),
            'success' => true
        ]);
    }

    public function strStdntAnsrs(Request $request)
    {
        /* return response()->json([
            'ggggg' => $request->data //[0]['answers'][0]['answer']
            ]);  */
        $userId = auth('api')->user()->id;
        
        // Check if logged in.
        if (!isset($userId) or $userId == null) {
            return response()->json([
                'msg' => trans('general.plsLogin'),
                'success' => false,
            ]);
        }

        // Check if answered before.
        $chk = StudentAnswer::where('userId', $userId)
            ->where('examId', $request->examId)
            ->count();

        if ($chk > 0) {
            return response()->json([
                'msg' => trans('general.answeredBefore'),
                'success' => false,
            ]);
        }

        $questionCount = 0;
        $correctAnswersCount = 0;

        foreach ($request->data as $row) {
            $questionCount += 1;

            foreach ($row['answers'] as $answer) {

                $isCorrect = isset($answer['isCorrect']) ? $answer['isCorrect'] : 0;
                $correctAnswersCount += $isCorrect;

                StudentAnswer::create([
                    'userId' => $userId,
                    'examId' => $request->examId,
                    'questionId' => $answer['questionId'],
                    'answerId' => $answer['id'],
                    'isCorrect' => $isCorrect
                ]);
            }
        }

        //update user credit.
        if ($correctAnswersCount != 0) {
            $user = User::find($userId);
            $user->credit = $user->credit + $correctAnswersCount;
            $user->save();
        }

        // save user score
        StudentScore::create([
            'userId' => $userId,
            'examId' => $request->examId,
            'questionsCount' => $questionCount,
            'correctAnswersCount' => $correctAnswersCount
        ]);

        // return
        return response()->json([
            'msg' => trans('general.examAnswersSent'),
            'success' => true,
            'questionCount' => $questionCount,
            'correctAnswersCount' => $correctAnswersCount,
        ]);
    }
}
