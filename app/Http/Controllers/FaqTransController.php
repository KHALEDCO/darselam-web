<?php

namespace App\Http\Controllers;

//use App\Faq;
use App\Http\Models\Faq;
use App\Http\Models\FaqTrans;
use Illuminate\Http\Request;
use App\Http\Requests\FaqRequest;

class FaqTransController extends Controller
{
    public function __construct()
	{
        $this->middleware('auth:api', ['except' => ['show', 'index']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /* public function index($status = '')
    {        

        if( $status != '') {
            $rows = FaqTrans::with('elment_trans')->where('status', $status)->get();
        } else {
            $rows = FaqTrans::with('elment_trans')->orderBy('id', 'desc')->get();
        } 
        
        return response()->json(['rows' => $rows]);
    } */

    /** 
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FaqRequest $request)
    {
        $row = FaqTrans::create($request->all());       

        return response()->json([
            'msg' => trans('general.savedSuccessfully'),
            'rowId' => $row->id
            ]);

    }

    public function edit($id)
    {
        $row = FaqTrans::find($id);
        return response()->json($row);

    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $row = FaqTrans::findOrFail($request->id);
        $row->update($request->all());

        return response()->json(['msg' => trans('general.updatedSuccessfully')]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        FaqTrans::find($id)->delete();
    }
}
