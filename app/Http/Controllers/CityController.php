<?php

namespace App\Http\Controllers;

use App\Http\Models\City;
use App\Http\Models\Country;
use Illuminate\Http\Request;
//use App\Http\Requests\CityRequest;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   

    public function index($status = '')
    {        
        
        $rows = City::where('isDeleted', 0);

        if( $status != '') {
            $rows = $rows->where('status', $status);
        }
        
        $rows = $rows->get();
        return response()->json(['rows' => $rows]);
    }

    public function citiesByCountry($countryCode = 'YE')
    {        
        $countryId = Country::where('countryCode', $countryCode)->first()->id;
        $rows = City::where('isDeleted', 0)
        ->where('countryId', $countryId)
        ->get();
        return response()->json(['rows' => $rows]);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $row = City::create($request->all());
        return response()->json([
            'msg' => trans('general.savedSuccessfully'),
            'rowId' => $row->id
            ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $row = City::where('status', 1)->find($id);
        return response()->json($row);

    }

    public function edit($id)
    {
        $row = City::find($id);
        return response()->json($row);

    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $row = City::findOrFail($request->id);
        $row->update($request->all());

        return response()->json(['msg' => trans('general.updatedSuccessfully')]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        City::where('id', $id)->update(['isDeleted' => 1]);
    }
}
