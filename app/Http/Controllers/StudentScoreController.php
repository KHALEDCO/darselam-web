<?php

namespace App\Http\Controllers;

use App\Http\Models\StudentAnswer;
use App\Http\Models\StudentScore;


class StudentScoreController extends Controller 
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
	{
        $this->middleware('auth:api', ['except' => []]);
    }
    
  


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Exam  $Exam
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $scoreInfo = StudentScore::findOrFail($id);   
             
        StudentAnswer::where('examId', $scoreInfo->examId)
        ->where('userId', $scoreInfo->userId)
        ->delete();
        
        $scoreInfo->delete();
    }

    public function scoresByExam($examId = 0){

        $rows = StudentScore::orderBy('id', 'desc')
        ->where('examId', $examId) 
        ->with('user')
        ->get();

        return response()->json(['rows' => $rows]);      

}
    
}
