<?php

namespace App\Http\Controllers;

use App\Http\Models\Shipmentadrs;
use Illuminate\Http\Request;
use App\Http\Requests\ShipmentadrsRequest;
use App\Http\Models\Country;

class ShipmentadrsController extends Controller
{
    public function __construct()
	{
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        
        $rows = Shipmentadrs::where('isDeleted', 0)->orderBy('id', 'desc')->get();
        return response()->json(['rows' => $rows]);
    }

    public function shpmntAdrsByUserId($userId = 0)
    {        
        $rows = Shipmentadrs::where('isDeleted', 0)
        ->where('userId', $userId)
        ->with('country')
        ->with('city')
        ->get();

        return response()->json(['rows' => $rows]);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ShipmentadrsRequest $request)
    {
        $countryId = Country::where('countryCode', $request->countryCode)->first()->id;

        $request->merge(['countryId' => $countryId]);

        $row = Shipmentadrs::create($request->all());
        return response()->json([
            'msg' => trans('general.savedSuccessfully'),
            'rowId' => $row->id
            ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Shipmentadrs  $size
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $row = Shipmentadrs::where('status', 1)->find($id);
        return response()->json($row);
    }

    public function edit($id)
    {
        $row = Shipmentadrs::with('country')->with('city')->find($id);
        return response()->json($row);

    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Shipmentadrs  $size
     * @return \Illuminate\Http\Response
     */
    public function update(ShipmentadrsRequest $request)
    {
        $countryId = Country::where('countryCode', $request->countryCode)->first()->id;

        $request->merge(['countryId' => $countryId]);

        $row = Shipmentadrs::findOrFail($request->id);
        $row->update($request->all());

        return response()->json(['msg' => trans('general.updatedSuccessfully')]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Shipmentadrs  $size
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Shipmentadrs::where('id', $id)->update(['isDeleted' => 1]);
    }
}
