<?php

namespace App\Http\Controllers;

use App\Http\Models\Guarantee;
use Illuminate\Http\Request;
use App\Http\Requests\GuaranteeRequest;

class GuaranteeController extends Controller 
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
	{
        $this->middleware('auth:api', ['except' => ['index']]);
    }
    
    public function index()
    { 
        $rows = Guarantee::where('isDeleted', 0)->with('product')->get();
        return response()->json(['rows' => $rows]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GuaranteeRequest $request)
    {
        $chk = Guarantee::where('serialNum', $request->serialNum)
        ->orWhere('guaranteeNum', $request->guaranteeNum)
        ->count();

        if($chk > 0) {
            return response()->json([
                'success' => false,
                'msg' => trans('general.serialOrGuaranteeNrExists'),
                ]); 
        }

        $row = Guarantee::create($request->all());

        return response()->json([            
            'success' => true,
            'msg' => trans('general.savedSuccessfully'),
            'rowId' => $row->id
            ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Guarantee  $Guarantee
     * @return \Illuminate\Http\Response
     */
   /*  public function show($id)
    {
        $row = Guarantee::where('status', 1)->find($id);
        return response()->json($row);
    } */

    public function edit($id)
    {
        $row = Guarantee::with('product')->find($id);
        return response()->json($row);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Guarantee  $Guarantee
     * @return \Illuminate\Http\Response
     */
    public function update(GuaranteeRequest $request)
    {
        $srlNr = $request->serialNum;
        $grntNr = $request->guaranteeNum;

        $chk = Guarantee::where('id', '!=', $request->id)    

        ->where(function ($query) use ($srlNr, $grntNr) {
            $query->where('serialNum', '=', $srlNr);
            $query->orWhere('guaranteeNum', '=', $grntNr);
        })
        ->count();

        if($chk > 0) {
            return response()->json([
                'success' => false,
                'msg' => trans('general.serialOrGuaranteeNrExists'),
                ]); 
        }

        $row = Guarantee::findOrFail($request->id);
        $row->update($request->all());

        return response()->json([
            'success' => true,
            'msg' => trans('general.updatedSuccessfully')]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Guarantee  $Guarantee
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Guarantee::where('id', $id)->update(['isDeleted' => 1]);
    }
}
