<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Mail;

use App\Http\Models\Setting;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public function sendEmail( 
        $email = 'sun.stronghold@gmail.com', //sun.stronghold@gmail.com
        $title = 'notification',
        $msg = '',
        $fileName = '',
        $senderEmail = 'support@shoppingworld-yem.com',
        $senderName = 'Shopping Yemen') {

            $chkSets = Setting::find(1);
            if($chkSets->notificationWay == 1) { // 1:sms only.
                return false;
            }

    if($email == ''){ return false; }
    if($title == ''){ $title = trans('general.website_name'); }
    $data  = ['emailMsg' => $msg];

    $attachmentUrl = ($fileName != '')? url().'/uploads/files/'.$fileName : '';
    $attachmentPath = ($fileName != '')? public_path().'/uploads/files/'.$fileName : '';

    //echo $email.' '.$senderEmail. ' '.$senderName. ' '. $msg;die;
    $emailRegex = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/';

    if ( preg_match($emailRegex, trim($email)) == true and $msg != '' ) {

    Mail::send('emails.msg', $data, function ($message) use ($title, $email, $fileName, $attachmentUrl, $attachmentPath, $senderEmail, $senderName ){

        $message->from( $senderEmail, $senderName );
        $message->to($email)->subject($title);

        if( file_exists($attachmentPath) == true and $fileName != '' ) {
            $message->attach($attachmentUrl, ['as' => $title]);
        }

    });

}

return '1';

}



public function sendSmsMsg($msg, $mobile_number, $countryCode = 'SA')
{

    $chkSets = Setting::find(1);

    if($chkSets->notificationWay == 2) { // 2:email only.
        return false;
    }
    
    $msg = urlencode($msg);
    $sid = 'shoppingyem';

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'http://api.smscountry.com/SMSCwebservice_bulk.aspx?User=shopworld&passwd=74103309&sid='.$sid.'&mobilenumber='.$mobile_number.'&message='.$msg.'&mtype=LNG&DR=Y');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    
    //$sendResult = curl_exec($ch);
    curl_close($ch);
    //$sendResult = explode(':', $sendResult);
    $sendResult[0] = 'OK';
    if ( $sendResult[0] == 'OK' ) return '1'; else return '0';

}



}


