<?php

namespace App\Http\Controllers;

use App\Http\Models\Album;
use App\Http\Models\User;
use App\Http\Models\UserCourse;
use App\Http\Requests\AlbumRequest;
use Illuminate\Http\Request;

class AlbumController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index', 'show', 'userAlbums']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function userAlbums(Request $request)
    {
        $rows = Album::where('userId', $request->userId)
            ->where('type', 1);

        if (isset($request->offset)) {
            $rows =  $rows->offset($request->offset)->limit(10);
        }

        $rows =  $rows->get();
        return response()->json(['rows' => $rows]);
    }

    public function index(Request $request)
    {
        $fullName = '';

        $rows = Album::where('branchId', $request->branchId);
        /*   ->with(['user' => function ($q) {
            $q->select('id', 'firstName', 'lastName');
        }]); */

        if (isset($request->userId)) {
            $rows =  $rows->where('userId', $request->userId);
            $fullName = User::find($request->userId)->full_name;
        }

        if (isset($request->slug)) {
            $user = User::where('slug', $request->slug)->first();
            if ($user) {
                $userId = $user->id;
                $rows =  $rows->where('userId', $userId);
                $fullName = $user->full_name;
            } else {
                $rows =  $rows->where('userId', 'notFound');
            }
        }

        if (isset($request->offset)) {
            $rows =  $rows->offset($request->offset)->limit(10);
        }
        // Get by sort.
        if (isset($request->sortBy) and isset($request->sortSign)) {
            $rows =  $rows->orderBy($request->sortBy, $request->sortSign);
        }

        $rows =  $rows->inRandomOrder()->get();

        return response()->json([
            'rows' => $rows,
            'fullName' => $fullName
        ]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rowId = 0;
        if (!isset($request->imgPlace) or $request->imgPlace != 1) { // Then it's main photo, don't store it in album.
            $row = Album::create($request->all());
            $rowId = $row->id;
        }

        return response()->json([
            'msg' => trans('general.savedSuccessfully'),
            'success' => true,
            'rowId' => $rowId
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $row = Album::find($id);
        return response()->json($row);
    }

    public function edit($id)
    {
        $row = Album::find($id);
        return response()->json($row);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function update(AlbumRequest $request)
    {
        $row = Album::findOrFail($request->id);
        $row->update($request->all());

        return response()->json([
            'msg' => trans('general.updatedSuccessfully'),
            'success' => true,
            //'rowId' => $row->id
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Album::find($id)->delete();
    }
}
