<?php

namespace App\Http\Controllers;

use App\Http\Models\Answer;
use App\Http\Models\Exam;
use App\Http\Models\Question;
use App\Http\Models\StudentScore;
use App\Http\Models\User;
use App\Http\Models\UserCourse;
use App\Http\Requests\ExamRequest;
use Illuminate\Http\Request;


class ExamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index', 'show', 'lastExams', 'examsByLevel']]);
    }

    public function index(Request $request)
    {
        $rows = Exam::orderBy('id', 'desc')
            ->where('branchId', $request->branchId);

        if ($request->status != 'all') { //with('questions')-> 
            $rows = $rows->where('status', $request->status);
        }

        if ($request->userId != 0) {
            $rows = $rows->where('userId', $request->userId);
        }
        if ($request->courseId != 0) {
            $rows = $rows->where('courseId', $request->courseId);
        }

        if (isset($request->offset) ) { //and isset($request->limit)
            $rows =  $rows->offset($request->offset)->limit(10); //$request->limit
        }

        // Get by sort.
        if (isset($request->sortBy) and isset($request->sortSign)) {
            $rows =  $rows->orderBy($request->sortBy, $request->sortSign);
        }
        
        $rows = $rows->get();
        return response()->json(['rows' => $rows]);
    }

    public function lastExams(Request $request)
    {
        $rows = Exam::where('status', 1)
            ->where('branchId', $request->branchId)
            ->orderBy('id', 'desc')
            ->take(3)->get();
        return response()->json(['rows' => $rows]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ExamRequest $request)
    {
        $row = Exam::create($request->all());

        return response()->json([
            'msg' => trans('general.savedSuccessfully'),
            'success' => true,
            'rowId' => $row->id
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Exam  $Exam
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $row = Exam::with('questions')->where('status', 1)->find($id);
        return response()->json($row);
    }

    public function edit($id)
    {
        $row = Exam::find($id);
        return response()->json($row);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Exam  $Exam
     * @return \Illuminate\Http\Response
     */
    public function update(ExamRequest $request)
    {
        $row = Exam::findOrFail($request->id);
        $row->update($request->all());

        return response()->json([
            'msg' => trans('general.updatedSuccessfully'),
            'success' => true
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Exam  $Exam
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Exam::findOrFail($id)->delete();
        Question::where('examId', $id)->delete();
        Answer::where('examId', $id)->delete();
        StudentScore::where('examId', $id)->delete();
    }

    public function examsByLevel($branchId = 0, $userId = 0)
    {
        $user = User::find($userId);

        if (!$user or $user->groupId != 2) { // not student.
            $rows = Exam::orderBy('id', 'desc')
                ->where('branchId', $branchId)
                ->where('status', 1)
                ->get();

            return response()->json(['rows' => $rows]);
        }

        $userLevels = UserCourse::where('userId', $userId)->get(['level'])->toArray();

        $examsByLevel = Exam::whereIn('level', $userLevels)
            ->where('status', 1)
            ->where('branchId', $branchId)
            ->get();

        return response()->json(['rows' => $examsByLevel]);
    }

    public function examsHistoryByUser($userId = 0)
    {

        $rows = StudentScore::orderBy('id', 'desc')
            ->where('userId', $userId)
            ->with(['exam' => function ($q) {
                $q->select('id', 'title');
            }])
            ->get(['examId', 'questionsCount', 'correctAnswersCount']);

        return response()->json(['rows' => $rows]);
    }
}
