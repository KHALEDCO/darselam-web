<?php

namespace App\Http\Controllers;

use App\Http\Models\Product;
use App\Http\Models\ProductRate;
use App\Http\Models\ProductFile;
use Illuminate\Http\Request;
use App\Http\Requests\ProductRequest;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['show', 'list', 'productImages']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $rows = Product::with(['category' => function ($q) {
            $q->select('id', 'title');
        }])
            ->with('featured_img')
            ->with(['seller' => function ($q) {
                $q->select('id', 'firstName', 'lastName');
            }])
            ->where('branchId', $request->branchId)
            ->where('isDeleted', 0)
            ->orderBy('id', 'desc');

        if (auth('api')->user()->groupId != 1) {
            $rows = $rows->where('sellerId', auth('api')->user()->id);
        }
        $rows = $rows->get(['id', 'title', 'regularPrice', 'salePrice', 'isNew', 'categoryId']);

        return response()->json(['rows' => $rows]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function rateProduct(Request $request)
    {
        $check = ProductRate::where('userId', $request->userId)
            ->where('elementId', $request->elementId)->count();

        if ($check != 0) {
            return response()->json([
                'msg' => trans('general.ratedBefore'),
                'success' => false
            ]);
        }

        $row = ProductRate::create($request->all());
        return response()->json([
            'msg' => trans('general.ratedSuccessfully'),
            'success' => true
        ]);
    }

    public function store(ProductRequest $request)
    {

        $request->merge(['sellerId' => auth('api')->user()->id]);

        $row = Product::create($request->all());

        return response()->json([
            'msg' => trans('general.savedSuccessfully'),
            'success' => true,
            'rowId' => $row->id,
        ]);
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {
        $row = Product::with(['category' => function ($q) {
            $q->select('id', 'title');
        }])
            ->where('isDeleted', 0)
            ->find($id);

        return response()->json($row);
    }

    public function show($id)
    {
        $row = Product::where('isDeleted', 0)
            ->with(['category' => function ($q) {
                $q->select('id', 'title');
            }])
            ->with('files')
            ->with(['seller' => function ($q) {
                $q->select('id', 'firstName', 'lastName');
            }])
            ->where('status', 1)
            ->find($id);

        return response()->json(['row' => $row]);
    }

    public function rndmPrdcts()
    {
        $rows = Product::with(['category' => function ($q) {
            $q->select('id', 'title');
        }])
            ->where('isDeleted', 0)
            ->where('status', 1)
            ->inRandomOrder()
            ->limit(10)
            ->get();
        return response()->json($rows);
    }


    public function prdctsByCatId($id, $subcatId = 0)
    {
        $rows = Product::where('isDeleted', 0)
            ->with(['category' => function ($q) {
                $q->select('id', 'title');
            }])
            ->where('status', 1);

        $rows = $rows->get();
        return response()->json(['rows' => $rows]);
    }

    /** 
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request)
    {
        $row = Product::findOrFail($request->id);

        $row->update($request->all());
        return response()->json([
            'msg' => trans('general.updatedSuccessfully'),
            'success' => true,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::where('id', $id)->update(['isDeleted' => 1]);
    }

    public function productImages($id = 0)
    {
        $rows = ProductFile::where('rowId', $id)->get();

        return response()->json(['rows' => $rows]);
    }

    public function list(Request $request)
    {
        $rows = Product::with(['category' => function ($q) {
            $q->select('id', 'title');
        }])
            ->with(['seller' => function ($q) {
                $q->select('id', 'firstName', 'lastName');
            }])
            ->with('featured_img')
            ->where('isDeleted', 0) 
            ->where('branchId', $request->branchId)
            ->where('status', 1)
            ->orderBy('id', 'desc');
            
            if (isset($request->offset) and isset($request->limit)) {
                $rows =  $rows->offset($request->offset)->limit($request->limit);
            }
    
            // Get by sort.
            if (isset($request->sortBy) and isset($request->sortSign)) {
                $rows =  $rows->orderBy($request->sortBy, $request->sortSign);
            }

            $rows =  $rows->get(['id', 'sellerId', 'title', 'regularPrice', 'salePrice', 'isNew', 'categoryId']);

        return response()->json(['rows' => $rows]);
    }
}
