<?php

namespace App\Http\Controllers;

//use App\Course;
use App\Http\Models\Course;
use App\Http\Models\CourseTrans;
use Illuminate\Http\Request;
use App\Http\Requests\CourseRequest;

class CourseTransController extends Controller
{
    public function __construct()
	{
        $this->middleware('auth:api', ['except' => ['show', 'index']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /* public function index($status = '')
    {        

        if( $status != '') {
            $rows = CourseTrans::with('elment_trans')->where('status', $status)->get();
        } else {
            $rows = CourseTrans::with('elment_trans')->orderBy('id', 'desc')->get();
        } 
        
        return response()->json(['rows' => $rows]);
    } */

    /** 
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CourseRequest $request)
    {
        $row = CourseTrans::create($request->all());       

        return response()->json([
            'msg' => trans('general.savedSuccessfully'),
            'rowId' => $row->id,
            'success' => true
            ]);

    }

    public function edit($id)
    {
        $row = CourseTrans::find($id);
        return response()->json($row);

    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $row = CourseTrans::findOrFail($request->id);
        $row->update($request->all());

        return response()->json([
            'msg' => trans('general.updatedSuccessfully'),
            
            'success' => true
            ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        CourseTrans::find($id)->delete();
    }
}
