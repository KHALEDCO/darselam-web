<?php

namespace App\Http\Controllers;

use App\Http\Models\Country;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   

    public function index($status = '')
    {        
        
        $rows = Country::where('isDeleted', 0)->orderBy('id', 'asc'); 

        if( $status != '') {
            $rows = $rows->where('status', $status);
        }
        
        $rows = $rows->get();
        return response()->json(['rows' => $rows]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $row = Country::create($request->all());
        return response()->json([
            'msg' => trans('general.savedSuccessfully'),
            'rowId' => $row->id
            ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $row = Country::where('status', 1)->find($id);
        return response()->json($row);

    }
    public function countryByCode($code)
    {
        $row = Country::where('countryCode', $code)->get()->first();
        return response()->json($row);

    }
    
    public function edit($id)
    {
        $row = Country::find($id);
        return response()->json($row);

    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $row = Country::findOrFail($request->id);
        $row->update($request->all());

        return response()->json(['msg' => trans('general.updatedSuccessfully')]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Country::where('id', $id)->update(['isDeleted' => 1]);
    }
}
