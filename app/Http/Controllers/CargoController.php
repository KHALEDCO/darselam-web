<?php

namespace App\Http\Controllers;

use App\Http\Models\Cargo;
use Illuminate\Http\Request;
use App\Http\Requests\CargoRequest;

class CargoController extends Controller
{
    public function __construct()
	{
        $this->middleware('auth:api', ['except' => ['show', 'index']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
         
        $rows = Cargo::where('isDeleted', 0)->orderBy('id', 'desc')->get();
        return response()->json(['rows' => $rows]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CargoRequest $request)
    {
        $row = Cargo::create($request->all());
        return response()->json([
            'msg' => trans('general.savedSuccessfully'),
            'rowId' => $row->id
            ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cargo  $cargo
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $row = Cargo::where('status', 1)->find($id);
        return response()->json($row);

    }
    public function edit($id)
    {
        $row = Cargo::find($id);
        return response()->json($row);

    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cargo  $cargo
     * @return \Illuminate\Http\Response
     */
    public function update(CargoRequest $request)
    {
        $row = Cargo::findOrFail($request->id);
        $row->update($request->all());

        return response()->json(['msg' => trans('general.updatedSuccessfully')]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cargo  $cargo
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cargo::where('id', $id)->update(['isDeleted' => 1]);
    }
}
