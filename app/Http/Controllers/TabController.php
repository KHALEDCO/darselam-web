<?php

namespace App\Http\Controllers;

use App\Http\Models\Tab;
use Illuminate\Http\Request;
use App\Http\Requests\TabRequest;
use App\Http\Models\TabTrans;


class TabController extends Controller 
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
	{
        $this->middleware('auth:api', ['except' => ['index']]);
    }
    
    public function index()
    { 
        $rows = Tab::with('product')
        ->with('elment_trans')
        ->get();
        return response()->json(['rows' => $rows]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TabRequest $request)
    {
        $row = Tab::create($request->all());

        // save trans.
        TabTrans::where('rowId', $row->id)
        ->where('languageCode', $request->languageCode)->delete();

        $trans = TabTrans::create([
            'rowId' => $row->id,
            'languageCode' => $request->languageCode,
            'title' => $request->title,
            'description' => $request->description,
            
        ]); 

        return response()->json([
            'msg' => trans('general.savedSuccessfully'),
            'rowId' => $row->id
            ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tab  $Tab
     * @return \Illuminate\Http\Response
     */
   /*  public function show($id)
    {
        $row = Tab::where('status', 1)->find($id);
        return response()->json($row);
    } */

    public function edit($id)
    {
        $row = Tab::with('product')->find($id);
        return response()->json($row);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tab  $Tab
     * @return \Illuminate\Http\Response
     */
    public function update(TabRequest $request)
    {
        $row = Tab::findOrFail($request->id);
        $row->update($request->all());

        return response()->json(['msg' => trans('general.updatedSuccessfully')]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tab  $Tab
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Tab::findOrFail($id)->delete();
    }
}
