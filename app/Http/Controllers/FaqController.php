<?php

namespace App\Http\Controllers;

//use App\Faq;
use App\Http\Models\Faq;
use Illuminate\Http\Request;
use App\Http\Requests\FaqRequest;

class FaqController extends Controller
{
    public function __construct()
	{
        $this->middleware('auth:api', ['except' => ['index']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request)
    {        

            $rows = Faq::with('elment_trans')->where('branchId', $request->branchId);

            if (auth('api')->user() != null and auth('api')->user()->groupId == 1) {
                if (isset($request->status)) {    
                    $rows = $rows->where('status', $request->status);
                }
            } else {
                $rows = $rows->where('status', 1);
            }
    
            if (isset($request->offset) and isset($request->limit)) {
                $rows =  $rows->offset($request->offset)->limit($request->limit);
            }
    
            // Get by sort.
            if (isset($request->sortBy) and isset($request->sortSign)) {
                $rows =  $rows->orderBy($request->sortBy, $request->sortSign);
            }
            $rows =  $rows->get();
        
        
        return response()->json(['rows' => $rows]);
    }

    /** 
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FaqRequest $request)
    {
        $row = Faq::create($request->all());
        // save trans.
        $row->languages()->attach($request->get('languageCode', []), 
        [
            'description1' => $request->description1,
            'description2' => $request->description2,
        ]);
 
        return response()->json([
            'msg' => trans('general.savedSuccessfully'),
            'rowId' => $row->id
            ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $row = Faq::where('status', 1)->find($id);
        return response()->json($row);

    }
    public function edit($id)
    {
        $row = Faq::find($id);
        return response()->json($row);

    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $row = Faq::findOrFail($request->id);
        $row->update($request->all());

        return response()->json(['msg' => trans('general.updatedSuccessfully')]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Faq::find($id)->delete();
    }
}
