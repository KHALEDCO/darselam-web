<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class ProductRate extends Model
{
    protected $table = "product_rate";

    protected $guarded = ['id', 'langCode'];
    public $timestamps = true;

    
}
