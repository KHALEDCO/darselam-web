<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable = ['price', 'status'];

    public function elment_trans()
    {
        return $this->hasMany('App\Http\Models\CourseTrans', 'rowId');
    } 
    public function one_trans()
    {
        return $this->hasOne('App\Http\Models\CourseTrans', 'rowId');
    } 
    public function files() 
    {
        return $this->hasMany('App\Http\Models\CourseFile', 'rowId');
    }

    
}
