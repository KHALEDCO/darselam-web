<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Guarantee extends Model
{
    protected $guarded = ['id', 'langCode'];
    public $timestamps = false;

    public function product()
    {
        return $this->belongsTo('App\Http\Models\Product', 'productId')->with('elment_trans');
    }
    
}
