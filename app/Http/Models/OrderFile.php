<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class OrderFile extends Model {

    protected $table = 'order_file';

    protected $guarded = [
        'id',
    ];

    /**
     * Set timestamps off
     */
    public $timestamps = true;


}
