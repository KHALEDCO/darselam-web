<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class RolePermission extends Model {

    protected $table = 'role_permission';

    protected $guarded = [
        'id',
    ];

    /**
     * Set timestamps off
     */
    public $timestamps = false;



}
