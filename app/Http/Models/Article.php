<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = ['status', 'photo'];

    public function elment_trans()
    {
        return $this->hasMany('App\Http\Models\ArticleTrans', 'rowId');
    }
}
