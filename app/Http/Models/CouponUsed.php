<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class CouponUsed extends Model {

    protected $table = 'coupon_used';

    protected $guarded = [
        'id',
    ];

    /*public static function getAll()
    {
        return Coupon::where('status', '1')->get();
    }*/
}
