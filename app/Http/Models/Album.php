<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    protected $guarded = ['id', 'langCode', 'imgPlace'];
    public $timestamps = false;


    protected $appends = ['small', 'medium'];

    protected $visible = [
        'id', 'title','small', 'medium'
     ];

    public function getSmallAttribute() {
        //return url('/uploads/files/'.$this->fileName);
        return url('/uploads/thumbnails/300-'.$this->photo);
    }
 
    public function getMediumAttribute() {
        //return url('/uploads/files/'.$this->fileName);
        return url('/uploads/thumbnails/512-'.$this->photo);
    }

 
    public function user()
    {
        return $this->belongsTo('App\Http\Models\User', 'userId');
    }
} 
