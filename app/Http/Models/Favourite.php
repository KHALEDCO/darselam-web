<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Favourite extends Model
{
    protected $guarded = ['id', 'langCode'];
    public $timestamps = true;

    public function product()
    {
        return $this->belongsTo('App\Http\Models\Product', 'itemId')->with('elment_trans');
    }
}
