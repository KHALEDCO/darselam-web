<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Subcat extends Model
{
    protected $guarded = ['id', 'langCode'];

    public $timestamps = false;
    protected $table = 'sub_cat';


    public function category()
    {
        return $this->belongsTo('App\Http\Models\Category', 'categoryId');
    }
}
