<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Tab extends Model
{
    protected $fillable = ['status', 'photo', 'productId'];
    public $timestamps = false;

    public function product()
    {
        return $this->belongsTo('App\Http\Models\Product', 'productId')->with('elment_trans');
    }
    public function languages()
    {
        return $this->belongsToMany('App\Http\Models\Language', 'tabs_trans', 'rowId', 'languageCode');
    }

    public function elment_trans()
    {
        return $this->hasMany('App\Http\Models\TabTrans', 'rowId');
    }
}
