<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class UserCourse extends Model { 

    protected $table = 'users_courses';

    protected $guarded = [
        'id',
    ];

    /**
     * Set timestamps off
     */
    public $timestamps = false;



}
