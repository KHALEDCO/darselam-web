<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Gift extends Model
{
    protected $guarded = ['id', 'langCode'];
    public $timestamps = false;
    protected $hidden = [
        'photo', 'branchId',
     ];

    protected $appends = ['imgUrl'];

    public function getImgUrlAttribute() {
        return url('/uploads/thumbnails/512-'.$this->photo);
    }
} 
