<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    
    protected $guarded = ['id', 'langCode'];

    public function files() 
    {
        return $this->hasMany('App\Http\Models\ProductFile', 'rowId');
    }

    public function featured_img() 
    {
        return $this->hasOne('App\Http\Models\ProductFile', 'rowId')
        ->where('mainImg', 1);
    }
    public function seller()
    {
        return $this->belongsTo('App\Http\Models\User', 'sellerId');
    }
    public function category()
    {
        return $this->belongsTo('App\Http\Models\ProductCategory', 'categoryId');
    }
 
    public function product_rate() 
    {
        return $this->hasMany('App\Http\Models\ProductRate', 'elementId');
    }
  
}
