<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Shipmentadrs extends Model
{
    protected $guarded = ['id', 'langCode', 'countryCode'];

    public function country()
    {
        return $this->belongsTo('App\Http\Models\Country', 'countryId');
    }
    public function city()
    {
        return $this->belongsTo('App\Http\Models\City', 'cityId');
    }
}
