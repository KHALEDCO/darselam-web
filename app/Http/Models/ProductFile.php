<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class ProductFile extends Model {

    protected $table = 'products_files';

    protected $guarded = [
        'id',
    ];

    /**
     * Set timestamps off
     */
    public $timestamps = false;

    protected $appends = ['small', 'medium']; 

    protected $visible = [
        'id', 'small', 'medium'
     ];

    public function getSmallAttribute() {
        //return url('/uploads/files/'.$this->fileName);
        return url('/uploads/thumbnails/300-'.$this->fileName);
    }
 
    public function getMediumAttribute() {
        //return url('/uploads/files/'.$this->fileName);
        return url('/uploads/thumbnails/512-'.$this->fileName);
    }
}
