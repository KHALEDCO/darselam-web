<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    protected $guarded = ['id'];
 
    protected $table = 'products_categories';
    public $timestamps = false;

  
}
