<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class OrderItems extends Model
{
    protected $guarded = ['id'];
    public $timestamps = false;

   /*  protected $hidden = [
        'created_at',
        'updated_at'
    ]; */
    public function product()
    {
        return $this->belongsTo('App\Http\Models\Product', 'productId');
    }

    public function seller()
    {
        return $this->belongsTo('App\Http\Models\User', 'sellerId');
    }
}
