<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CouponRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'couponValue' => 'required|numeric', 
            'couponCode'  => 'required',
            'originalQuantity'    => 'required',
            //'type'        => 'required',
            'validFrom'   => 'required', 
            'validTo'     => 'required', 
            'minimumValueToBeApplied' => 'required|numeric', 
        ];
    }

   public function messages()
   {
       return [
           'couponValue.required' => __('general.couponValueRequired'),
           'couponValue.numeric' => __('general.couponValueNumeric'),
           'couponCode.required' => __('general.couponCodeRequired'),
           'originalQuantity.required' => __('general.quantityRequired'),
           //'type.required' => __('general.typeRequired'),
           'validFrom.required' => __('general.ExpiryDateRequired'), 
           'validTo.required' => __('general.ExpiryDateRequired'), 
           'minimumValueToBeApplied.numeric' => __('general.minimumValueToBeAppliedNumeric'),
           'minimumValueToBeApplied.required' => __('general.minimumValueToBeAppliedRequired'), 

       ];
   }
}
