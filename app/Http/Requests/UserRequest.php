<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //'firstName' => 'required',
            //'lastName' => 'required',
            //'phone' => 'required',
            //'password' => 'required',
            //'adrs' => 'required',
        ];
    }

   public function messages()
   {
       return [
           //'firstName.required' => __('general.firstNameRequired'),
           //'lastName.required' => __('general.lastNameRequired'),
           //'phone.required' => __('general.phoneRequired'),
           //'password.required' => __('general.passwordRequired'),
           //'adrs.required' => __('general.adrsRequired'),

       ];
   }
}
