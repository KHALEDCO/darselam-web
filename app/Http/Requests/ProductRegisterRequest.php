<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstName' => 'required',
            'lastName' => 'required',
            'phone' => 'required',
            'email' => 'required|email',
            'serialNum' => 'required',
            'guaranteeNum' => 'required',
            'adrs' => 'required',
        ];
    }

   public function messages()
   {
       return [
           'firstName.required' => __('general.firstNameRequired'),
           'lastName.required' => __('general.lastNameRequired'),
           'phone.required' => __('general.phoneRequired'),
           'email.required' => __('general.emailRequired'),
           'email.email' => __('general.emailEmail'),
           'serialNum.required' => __('general.serialNumRequired'),
           'guaranteeNum.required' => __('general.guaranteeNumRequired'),
           'adrs.required' => __('general.adrsRequired'),

       ];
   }
}
