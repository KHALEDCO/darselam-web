<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ShipmentadrsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstName' => 'required',
            'lastName' => 'required',
            'receiverMobile' => 'required',
            'receiverMobile2' => 'required',
            'countryId' => 'required',
            'cityId' => 'required',
            'adrs' => 'required',
            //'receiverMobile' => 'required',
        ];
    }

   public function messages()
   {
       
       return [
        'firstName.required' => __('general.firstNameRequired'),
        'lastName.required' => __('general.lastNameRequired'),
        'receiverMobile.required' => __('general.receiverMobileRequired'),
        'receiverMobile2.required' => __('general.receiverMobile2Required'),
        'countryId.required' => __('general.countryRequired'),
        'cityId.required' => __('general.cityRequired'),
        'adrs.required' => __('general.adrsRequired'),
           //'receiverMobile.required' => __('general.phoneRequired'),

       ];
   }
}
