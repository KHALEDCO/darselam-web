<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        if (Request::get('groupId') == '2' || Request::get('groupId') == '3') {
            
            return [
                'groupId' => 'required',
                'firstName' => 'required',
                'lastName' => 'required',
                'tcNr' => 'required|numeric|digits_between:9,12', 
                'phone' => 'required|numeric|digits_between:9,14', 
                'birthDate' => 'required',
                'orphan' => 'required',
                /* 'municipality' => 'required',
                'neighborhood' => 'required',
                'street' => 'required',
                'building' => 'required',
                'apartment' => 'required', */
            ];
        } else {
            return [
                'groupId' => 'required',
                'firstName' => 'required',
                'lastName' => 'required',
                'phone' => 'required|numeric|digits_between:9,14', 
            ];
        }
        
    }

   public function messages() 
   {
       
       return [ 
           'firstName.required' => __('general.firstNameRequired'),
           'lastName.required' => __('general.lastNameRequired'),
           'groupId.required' => __('general.chooseDepTxt'),/* 
           'email.required' => __('general.chooseDepTxt'),
           'email.required' => __('general.emailEmail'), */
           'phone.required' => __('general.phoneRequired'),
           'phone.numeric' => __('general.phoneNumeric'),
           'phone.digits_between' => __('general.phoneDigits_between'),
           'tcNr.required' => __('general.tcNrRequired'),
           'tcNr.numeric' => __('general.tcNrNumeric'),
           'tcNr.digits_between' => __('general.tcNrDigits_between'),
           'birthDate.required' => __('general.birthDateRequired'),
           'orphan.required' => __('general.orphanRequired'),
           'municipality.required' => __('general.municipalityRequired'),
           'neighborhood.required' => __('general.neighborhoodRequired'),
           'street.required' => __('general.streetRequired'),
           'building.required' => __('general.buildingRequired'),
           'apartment.required' => __('general.apartmentRequired'),

       ];
   }
   
}
