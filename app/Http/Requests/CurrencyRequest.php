<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CurrencyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'currencyCode' => 'required', 
            'exchangeRate' => 'required', 
        ];
    }

   public function messages()
   {
       return [
           'title.required' => __('general.titleRequired'),
           'currencyCode.required' => __('general.currencyCodeRequired'),
           'exchangeRate.required' => __('general.exchangeRateRequired'),

       ];
   }
}
